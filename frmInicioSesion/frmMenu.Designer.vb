﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ProcesosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProducciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EnfundeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.R11ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformeDeEmbarqueToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReporteComidaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MantenimientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InternoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalendarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ColorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FincaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LotesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalidadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TipoPrecioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BananoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CultivoVariedadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DefectosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OtrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PaisToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProvinciaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CiudadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProcesosToolStripMenuItem, Me.MantenimientoToolStripMenuItem, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(894, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ProcesosToolStripMenuItem
        '
        Me.ProcesosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProducciónToolStripMenuItem, Me.ReportesToolStripMenuItem})
        Me.ProcesosToolStripMenuItem.Name = "ProcesosToolStripMenuItem"
        Me.ProcesosToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.ProcesosToolStripMenuItem.Text = "Procesos"
        '
        'ProducciónToolStripMenuItem
        '
        Me.ProducciónToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EnfundeToolStripMenuItem, Me.R11ToolStripMenuItem})
        Me.ProducciónToolStripMenuItem.Name = "ProducciónToolStripMenuItem"
        Me.ProducciónToolStripMenuItem.Size = New System.Drawing.Size(135, 22)
        Me.ProducciónToolStripMenuItem.Text = "Producción"
        '
        'EnfundeToolStripMenuItem
        '
        Me.EnfundeToolStripMenuItem.Name = "EnfundeToolStripMenuItem"
        Me.EnfundeToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.EnfundeToolStripMenuItem.Text = "Enfunde"
        '
        'R11ToolStripMenuItem
        '
        Me.R11ToolStripMenuItem.Name = "R11ToolStripMenuItem"
        Me.R11ToolStripMenuItem.Size = New System.Drawing.Size(148, 22)
        Me.R11ToolStripMenuItem.Text = "R11 - Recobro"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InformeDeEmbarqueToolStripMenuItem, Me.ReporteComidaToolStripMenuItem})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(135, 22)
        Me.ReportesToolStripMenuItem.Text = "Reportes"
        '
        'InformeDeEmbarqueToolStripMenuItem
        '
        Me.InformeDeEmbarqueToolStripMenuItem.Name = "InformeDeEmbarqueToolStripMenuItem"
        Me.InformeDeEmbarqueToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.InformeDeEmbarqueToolStripMenuItem.Text = "Informe de embarque"
        '
        'ReporteComidaToolStripMenuItem
        '
        Me.ReporteComidaToolStripMenuItem.Name = "ReporteComidaToolStripMenuItem"
        Me.ReporteComidaToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ReporteComidaToolStripMenuItem.Text = "Listado comida"
        '
        'MantenimientoToolStripMenuItem
        '
        Me.MantenimientoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClientesToolStripMenuItem, Me.UsuarioToolStripMenuItem, Me.CalendarioToolStripMenuItem, Me.FincaToolStripMenuItem, Me.CajasToolStripMenuItem, Me.BananoToolStripMenuItem, Me.OtrosToolStripMenuItem})
        Me.MantenimientoToolStripMenuItem.Name = "MantenimientoToolStripMenuItem"
        Me.MantenimientoToolStripMenuItem.Size = New System.Drawing.Size(101, 20)
        Me.MantenimientoToolStripMenuItem.Text = "Mantenimiento"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegistroToolStripMenuItem3})
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ClientesToolStripMenuItem.Text = "Clientes"
        '
        'RegistroToolStripMenuItem3
        '
        Me.RegistroToolStripMenuItem3.Name = "RegistroToolStripMenuItem3"
        Me.RegistroToolStripMenuItem3.Size = New System.Drawing.Size(117, 22)
        Me.RegistroToolStripMenuItem3.Text = "Registro"
        '
        'UsuarioToolStripMenuItem
        '
        Me.UsuarioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InternoToolStripMenuItem})
        Me.UsuarioToolStripMenuItem.Name = "UsuarioToolStripMenuItem"
        Me.UsuarioToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.UsuarioToolStripMenuItem.Text = "Usuario"
        '
        'InternoToolStripMenuItem
        '
        Me.InternoToolStripMenuItem.Name = "InternoToolStripMenuItem"
        Me.InternoToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.InternoToolStripMenuItem.Text = "Registro"
        '
        'CalendarioToolStripMenuItem
        '
        Me.CalendarioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegistroToolStripMenuItem2, Me.ColorToolStripMenuItem})
        Me.CalendarioToolStripMenuItem.Name = "CalendarioToolStripMenuItem"
        Me.CalendarioToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.CalendarioToolStripMenuItem.Text = "Calendario"
        '
        'RegistroToolStripMenuItem2
        '
        Me.RegistroToolStripMenuItem2.Name = "RegistroToolStripMenuItem2"
        Me.RegistroToolStripMenuItem2.Size = New System.Drawing.Size(117, 22)
        Me.RegistroToolStripMenuItem2.Text = "Registro"
        '
        'ColorToolStripMenuItem
        '
        Me.ColorToolStripMenuItem.Name = "ColorToolStripMenuItem"
        Me.ColorToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.ColorToolStripMenuItem.Text = "Colores"
        '
        'FincaToolStripMenuItem
        '
        Me.FincaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegistroToolStripMenuItem, Me.LotesToolStripMenuItem})
        Me.FincaToolStripMenuItem.Name = "FincaToolStripMenuItem"
        Me.FincaToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.FincaToolStripMenuItem.Text = "Finca"
        '
        'RegistroToolStripMenuItem
        '
        Me.RegistroToolStripMenuItem.Name = "RegistroToolStripMenuItem"
        Me.RegistroToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.RegistroToolStripMenuItem.Text = "Registro"
        '
        'LotesToolStripMenuItem
        '
        Me.LotesToolStripMenuItem.Name = "LotesToolStripMenuItem"
        Me.LotesToolStripMenuItem.Size = New System.Drawing.Size(117, 22)
        Me.LotesToolStripMenuItem.Text = "Lotes"
        '
        'CajasToolStripMenuItem
        '
        Me.CajasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegistroToolStripMenuItem1, Me.CalidadToolStripMenuItem, Me.TipoPrecioToolStripMenuItem})
        Me.CajasToolStripMenuItem.Name = "CajasToolStripMenuItem"
        Me.CajasToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.CajasToolStripMenuItem.Text = "Cajas"
        '
        'RegistroToolStripMenuItem1
        '
        Me.RegistroToolStripMenuItem1.Name = "RegistroToolStripMenuItem1"
        Me.RegistroToolStripMenuItem1.Size = New System.Drawing.Size(152, 22)
        Me.RegistroToolStripMenuItem1.Text = "Registro"
        '
        'CalidadToolStripMenuItem
        '
        Me.CalidadToolStripMenuItem.Name = "CalidadToolStripMenuItem"
        Me.CalidadToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.CalidadToolStripMenuItem.Text = "Calidad"
        '
        'TipoPrecioToolStripMenuItem
        '
        Me.TipoPrecioToolStripMenuItem.Name = "TipoPrecioToolStripMenuItem"
        Me.TipoPrecioToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.TipoPrecioToolStripMenuItem.Text = "Tipo Precio"
        '
        'BananoToolStripMenuItem
        '
        Me.BananoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ManosToolStripMenuItem, Me.CultivoVariedadToolStripMenuItem, Me.DefectosToolStripMenuItem})
        Me.BananoToolStripMenuItem.Name = "BananoToolStripMenuItem"
        Me.BananoToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.BananoToolStripMenuItem.Text = "Banano"
        '
        'ManosToolStripMenuItem
        '
        Me.ManosToolStripMenuItem.Name = "ManosToolStripMenuItem"
        Me.ManosToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.ManosToolStripMenuItem.Text = "Manos"
        '
        'CultivoVariedadToolStripMenuItem
        '
        Me.CultivoVariedadToolStripMenuItem.Name = "CultivoVariedadToolStripMenuItem"
        Me.CultivoVariedadToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.CultivoVariedadToolStripMenuItem.Text = "Cultivo/Variedad"
        '
        'DefectosToolStripMenuItem
        '
        Me.DefectosToolStripMenuItem.Name = "DefectosToolStripMenuItem"
        Me.DefectosToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.DefectosToolStripMenuItem.Text = "Defectos"
        '
        'OtrosToolStripMenuItem
        '
        Me.OtrosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PaisToolStripMenuItem, Me.ProvinciaToolStripMenuItem, Me.CiudadToolStripMenuItem})
        Me.OtrosToolStripMenuItem.Name = "OtrosToolStripMenuItem"
        Me.OtrosToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.OtrosToolStripMenuItem.Text = "Otros"
        '
        'PaisToolStripMenuItem
        '
        Me.PaisToolStripMenuItem.Name = "PaisToolStripMenuItem"
        Me.PaisToolStripMenuItem.Size = New System.Drawing.Size(123, 22)
        Me.PaisToolStripMenuItem.Text = "Pais"
        '
        'ProvinciaToolStripMenuItem
        '
        Me.ProvinciaToolStripMenuItem.Name = "ProvinciaToolStripMenuItem"
        Me.ProvinciaToolStripMenuItem.Size = New System.Drawing.Size(123, 22)
        Me.ProvinciaToolStripMenuItem.Text = "Provincia"
        '
        'CiudadToolStripMenuItem
        '
        Me.CiudadToolStripMenuItem.Name = "CiudadToolStripMenuItem"
        Me.CiudadToolStripMenuItem.Size = New System.Drawing.Size(123, 22)
        Me.CiudadToolStripMenuItem.Text = "Ciudad"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'frmMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(894, 423)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BANPROD-MENU"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ProcesosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProducciónToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents R11ToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MantenimientoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FincaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegistroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LotesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalendarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CajasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegistroToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CalidadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TipoPrecioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BananoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ManosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CultivoVariedadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DefectosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OtrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PaisToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProvinciaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CiudadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformeDeEmbarqueToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InternoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegistroToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ColorToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EnfundeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RegistroToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReporteComidaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
