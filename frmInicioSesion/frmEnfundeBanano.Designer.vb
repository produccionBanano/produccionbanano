﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEnfundeBanano
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtColorSemanaR = New System.Windows.Forms.TextBox()
        Me.dgvEnfunde = New System.Windows.Forms.DataGridView()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.cboLote = New System.Windows.Forms.ComboBox()
        Me.cboSemana = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnEliminarRecobre = New System.Windows.Forms.Button()
        Me.btnEnfunde = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboHacienda = New System.Windows.Forms.ComboBox()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.cboAnio = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        CType(Me.dgvEnfunde, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtColorSemanaR
        '
        Me.txtColorSemanaR.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtColorSemanaR.Location = New System.Drawing.Point(208, 42)
        Me.txtColorSemanaR.MaxLength = 4
        Me.txtColorSemanaR.Multiline = True
        Me.txtColorSemanaR.Name = "txtColorSemanaR"
        Me.txtColorSemanaR.Size = New System.Drawing.Size(59, 6)
        Me.txtColorSemanaR.TabIndex = 23
        '
        'dgvEnfunde
        '
        Me.dgvEnfunde.AllowUserToAddRows = False
        Me.dgvEnfunde.AllowUserToDeleteRows = False
        Me.dgvEnfunde.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvEnfunde.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEnfunde.Location = New System.Drawing.Point(12, 96)
        Me.dgvEnfunde.Name = "dgvEnfunde"
        Me.dgvEnfunde.ReadOnly = True
        Me.dgvEnfunde.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEnfunde.Size = New System.Drawing.Size(407, 162)
        Me.dgvEnfunde.TabIndex = 20
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(358, 50)
        Me.txtCantidad.MaxLength = 4
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(61, 20)
        Me.txtCantidad.TabIndex = 18
        '
        'cboLote
        '
        Me.cboLote.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLote.FormattingEnabled = True
        Me.cboLote.Location = New System.Drawing.Point(336, 18)
        Me.cboLote.Name = "cboLote"
        Me.cboLote.Size = New System.Drawing.Size(83, 21)
        Me.cboLote.TabIndex = 17
        '
        'cboSemana
        '
        Me.cboSemana.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSemana.FormattingEnabled = True
        Me.cboSemana.Location = New System.Drawing.Point(209, 50)
        Me.cboSemana.Name = "cboSemana"
        Me.cboSemana.Size = New System.Drawing.Size(58, 21)
        Me.cboSemana.TabIndex = 16
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(303, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Cantidad"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(302, 21)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(28, 13)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Lote"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(157, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(46, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Semana"
        '
        'btnEliminarRecobre
        '
        Me.btnEliminarRecobre.Image = Global.produccionBanano.My.Resources.Resources.cancel
        Me.btnEliminarRecobre.Location = New System.Drawing.Point(425, 123)
        Me.btnEliminarRecobre.Name = "btnEliminarRecobre"
        Me.btnEliminarRecobre.Size = New System.Drawing.Size(55, 42)
        Me.btnEliminarRecobre.TabIndex = 22
        Me.btnEliminarRecobre.UseVisualStyleBackColor = True
        '
        'btnEnfunde
        '
        Me.btnEnfunde.Image = Global.produccionBanano.My.Resources.Resources.save
        Me.btnEnfunde.Location = New System.Drawing.Point(425, 75)
        Me.btnEnfunde.Name = "btnEnfunde"
        Me.btnEnfunde.Size = New System.Drawing.Size(55, 41)
        Me.btnEnfunde.TabIndex = 21
        Me.btnEnfunde.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Finca:"
        '
        'cboHacienda
        '
        Me.cboHacienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHacienda.Enabled = False
        Me.cboHacienda.FormattingEnabled = True
        Me.cboHacienda.Location = New System.Drawing.Point(63, 15)
        Me.cboHacienda.Name = "cboHacienda"
        Me.cboHacienda.Size = New System.Drawing.Size(217, 21)
        Me.cboHacienda.TabIndex = 27
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(422, 244)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(16, 13)
        Me.lblTotal.TabIndex = 29
        Me.lblTotal.Text = "..."
        '
        'cboAnio
        '
        Me.cboAnio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnio.FormattingEnabled = True
        Me.cboAnio.Items.AddRange(New Object() {"2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"})
        Me.cboAnio.Location = New System.Drawing.Point(63, 50)
        Me.cboAnio.Name = "cboAnio"
        Me.cboAnio.Size = New System.Drawing.Size(67, 21)
        Me.cboAnio.TabIndex = 31
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 13)
        Me.Label2.TabIndex = 30
        Me.Label2.Text = "Año:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 80)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(283, 13)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Atención: La información de enfunde se filtra por semanas."
        '
        'frmEnfundeBanano
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(489, 266)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboAnio)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboHacienda)
        Me.Controls.Add(Me.txtColorSemanaR)
        Me.Controls.Add(Me.btnEliminarRecobre)
        Me.Controls.Add(Me.btnEnfunde)
        Me.Controls.Add(Me.dgvEnfunde)
        Me.Controls.Add(Me.txtCantidad)
        Me.Controls.Add(Me.cboLote)
        Me.Controls.Add(Me.cboSemana)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmEnfundeBanano"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BANPROD-ENFUNDE"
        CType(Me.dgvEnfunde, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtColorSemanaR As System.Windows.Forms.TextBox
    Friend WithEvents btnEliminarRecobre As System.Windows.Forms.Button
    Friend WithEvents btnEnfunde As System.Windows.Forms.Button
    Friend WithEvents dgvEnfunde As System.Windows.Forms.DataGridView
    Friend WithEvents txtCantidad As System.Windows.Forms.TextBox
    Friend WithEvents cboLote As System.Windows.Forms.ComboBox
    Friend WithEvents cboSemana As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboHacienda As System.Windows.Forms.ComboBox
    Friend WithEvents lblTotal As System.Windows.Forms.Label
    Friend WithEvents cboAnio As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
