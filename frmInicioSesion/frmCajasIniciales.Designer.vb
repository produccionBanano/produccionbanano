﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCajasIniciales
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboCliente = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.cboMarca = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.dgvCajasIniciales = New System.Windows.Forms.DataGridView()
        Me.btnEliminaCaja = New System.Windows.Forms.Button()
        Me.btnGuardarCajas = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.dgvCajasIniciales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboCliente
        '
        Me.cboCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCliente.FormattingEnabled = True
        Me.cboCliente.Location = New System.Drawing.Point(59, 33)
        Me.cboCliente.Name = "cboCliente"
        Me.cboCliente.Size = New System.Drawing.Size(247, 21)
        Me.cboCliente.TabIndex = 35
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(13, 36)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(42, 13)
        Me.Label25.TabIndex = 34
        Me.Label25.Text = "Cliente:"
        '
        'cboMarca
        '
        Me.cboMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMarca.FormattingEnabled = True
        Me.cboMarca.Location = New System.Drawing.Point(59, 60)
        Me.cboMarca.Name = "cboMarca"
        Me.cboMarca.Size = New System.Drawing.Size(192, 21)
        Me.cboMarca.TabIndex = 33
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(13, 63)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(40, 13)
        Me.Label22.TabIndex = 32
        Me.Label22.Text = "Marca:"
        '
        'dgvCajasIniciales
        '
        Me.dgvCajasIniciales.AllowUserToAddRows = False
        Me.dgvCajasIniciales.AllowUserToDeleteRows = False
        Me.dgvCajasIniciales.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCajasIniciales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCajasIniciales.Location = New System.Drawing.Point(16, 87)
        Me.dgvCajasIniciales.Name = "dgvCajasIniciales"
        Me.dgvCajasIniciales.ReadOnly = True
        Me.dgvCajasIniciales.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCajasIniciales.Size = New System.Drawing.Size(240, 170)
        Me.dgvCajasIniciales.TabIndex = 36
        '
        'btnEliminaCaja
        '
        Me.btnEliminaCaja.Image = Global.produccionBanano.My.Resources.Resources.cancel
        Me.btnEliminaCaja.Location = New System.Drawing.Point(262, 134)
        Me.btnEliminaCaja.Name = "btnEliminaCaja"
        Me.btnEliminaCaja.Size = New System.Drawing.Size(55, 42)
        Me.btnEliminaCaja.TabIndex = 38
        Me.btnEliminaCaja.UseVisualStyleBackColor = True
        '
        'btnGuardarCajas
        '
        Me.btnGuardarCajas.Image = Global.produccionBanano.My.Resources.Resources.save
        Me.btnGuardarCajas.Location = New System.Drawing.Point(262, 87)
        Me.btnGuardarCajas.Name = "btnGuardarCajas"
        Me.btnGuardarCajas.Size = New System.Drawing.Size(55, 41)
        Me.btnGuardarCajas.TabIndex = 37
        Me.btnGuardarCajas.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(275, 13)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Por favor distribuya las cajas que se procesaran en el dia"
        '
        'frmCajasIniciales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(330, 267)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnEliminaCaja)
        Me.Controls.Add(Me.btnGuardarCajas)
        Me.Controls.Add(Me.dgvCajasIniciales)
        Me.Controls.Add(Me.cboCliente)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.cboMarca)
        Me.Controls.Add(Me.Label22)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmCajasIniciales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BANPROD-CAJAS INICIALES"
        CType(Me.dgvCajasIniciales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboCliente As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents cboMarca As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents dgvCajasIniciales As System.Windows.Forms.DataGridView
    Friend WithEvents btnEliminaCaja As System.Windows.Forms.Button
    Friend WithEvents btnGuardarCajas As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
