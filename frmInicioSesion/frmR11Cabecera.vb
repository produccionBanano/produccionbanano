﻿Imports ConexionBase.conexionBase
Public Class frmR11Cabecera
    Public datos As New ConexionBase.conexionBase
    Public sql As String = ""
    Public validado As Integer
    Private Sub frmR11Cabecera_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cargaCombos()
        cboHacienda.SelectedValue = fincaUsuario
    End Sub
    Sub limpialLbl()
        lblTRecobrado.Text = 0
        lblRRechazado.Text = 0
        lblProcesados.Text = 0
        lblPesoPRacimo.Text = 0
        lblRatioGeneral.Text = 0
        lblRatioPTotal.Text = 0
        lblMerma.Text = 0
        lblPesoPCaja.Text = 0
        lblFRecuperada.Text = 0
        lblCajaProcesadas.Text = 0
        lblCajasEnviadas.Text = 0
        lblCajaPrimera.Text = 0
        lblCajaSegunda.Text = 0
    End Sub

    Sub cargarInfoAdicional()
        Dim resultado As New DataTable
        Dim recobrado As Decimal = 0
        Dim rechazado As Decimal = 0
        Dim chapeado As Decimal = 0
        limpialLbl()

        If lblNumero.Text <> 0 Then
            'Total Recobrado

            sql = "SELECT idR11,IFNULL(SUM(cantidad),0) recobrado,IFNULL(SUM(rechazado),0) rechazado,IFNULL(SUM(chapeado),0) chapeado FROM tblRecobre WHERE idR11=" & lblNumero.Text
            resultado = datos.ejecutarMySql(sql)

            If resultado.Rows.Count > 0 Then
                recobrado = resultado.Rows(0).Item("recobrado").ToString
                rechazado = resultado.Rows(0).Item("rechazado").ToString
                chapeado = resultado.Rows(0).Item("chapeado").ToString
                Dim procesado As Decimal = recobrado - rechazado
                lblTRecobrado.Text = recobrado
                lblRRechazado.Text = rechazado
                lblProcesados.Text = procesado
                lblTChapeado.Text = chapeado
            Else
                lblTRecobrado.Text = 0
                lblRRechazado.Text = 0
                lblProcesados.Text = 0
                lblTChapeado.Text = 0
            End If

            'Ratio general
            sql = ""
            sql = "SELECT IFNULL(SUM(cpe.cantidadP),0) cajasProcesadas,IFNULL(SUM(cpe.cantidadE),0) cajasEnviadas FROM tblCajasPE cpe "
            sql &= " INNER JOIN tblCaja caj ON caj.id=cpe.idMarca AND caj.calidad=1"
            sql &= " WHERE cpe.idR11=" & lblNumero.Text

            resultado.Clear()

            resultado = datos.ejecutarMySql(sql)

            If resultado.Rows.Count > 0 Then

                Dim cajasProcesadas As Double = resultado.Rows(0).Item("cajasProcesadas")
                Dim cajasEnviadas As Double = resultado.Rows(0).Item("cajasEnviadas")
                Dim ratioGeneral As Double = 0
                If cajasProcesadas <> 0 Then
                    ratioGeneral = Format(cajasProcesadas / (recobrado), "####.##")
                Else
                    ratioGeneral = 0
                End If

                lblRatioGeneral.Text = ratioGeneral
                lblCajaProcesadas.Text = cajasProcesadas
                lblCajasEnviadas.Text = cajasEnviadas
            Else
                lblRatioGeneral.Text = 0
                lblCajaProcesadas.Text = 0
                lblCajasEnviadas.Text = 0
            End If

            'Ratio cajas transformadas
            sql = ""
            sql = "SELECT SUM(DAT.transformado) procesadasTrans FROM ("
            sql &= " SELECT CAJAS.cajasTipo,CAJAS.tamaño,CAJAS.peso,"
            sql &= " SUM(CAJAS.cantidadP) procesadas,"
            sql &= " IF(CAJAS.tamaño='Chicas',(SUM(CAJAS.cantidadP)*(CAJAS.peso*2.2046))/41.5,SUM(CAJAS.cantidadP)) transformado "
            sql &= " FROM ( select cape.idR11,ca.descripcion cajasTipo,ifnull(ca.peso,0) peso,cape.cantidadP,'Chicas' as tamaño "
            sql &= " from tblCajasPE cape left join tblCaja ca on ca.id=cape.idMarca and ca.peso<=(41/2.2) and ca.calidad=1"
            sql &= " where ifnull(ca.peso, 0) <> 0 and cape.idR11 in (" & lblNumero.Text & ") "

            sql &= " UNION ALL"

            sql &= " select cape.idR11,ca.descripcion cajasTipo,ifnull(ca.peso,0) peso,cape.cantidadP,'Grandes' as tamaño "
            sql &= " from tblCajasPE cape left join tblCaja ca on ca.id=cape.idMarca and ca.peso>(41/2.2) and ca.calidad=1"
            sql &= " where ifnull(ca.peso,0) <> 0 and cape.idR11 in (" & lblNumero.Text & ")) AS CAJAS "
            sql &= " GROUP BY CAJAS.cajasTipo,CAJAS.tamaño,CAJAS.peso"
            sql &= " ) AS DAT"

            resultado.Clear()

            resultado = datos.ejecutarMySql(sql)

            If resultado.Rows.Count > 0 Then

                Dim cajasProcesadasTrans As Double = resultado.Rows(0).Item("procesadasTrans")
                Dim ratioTransformado As Double = 0
                If cajasProcesadasTrans <> 0 Then
                    ratioTransformado = Format(cajasProcesadasTrans / (recobrado), "####.##")
                Else
                    ratioTransformado = 0
                End If

                lblRatioCajasTransformadas.Text = ratioTransformado
            Else
                lblRatioCajasTransformadas.Text = 0

            End If


            'Merma
            Dim pesoCajaPromedio As Double = 0
            Dim pesoRacimoPromedio As Double = 0
            sql = ""
            sql = " SELECT TABLA.calidad,IFNULL(AVG(TABLA.peso),0) pesoCajaPromedio FROM ("
            sql &= " SELECT caini.idCaja,caj.peso,caj.calidad FROM tblCajasIniciales  caini"
            sql &= " INNER JOIN tblCaja caj ON caj.id=caini.idCaja AND caj.calidad=1"
            sql &= " WHERE caini.idR11= " & lblNumero.Text & ") AS TABLA;"

            resultado = datos.ejecutarMySql(sql)
            If resultado.Rows.Count > 0 Then
                pesoCajaPromedio = resultado.Rows(0).Item("pesoCajaPromedio")
                lblPesoPCaja.Text = Format(pesoCajaPromedio, "####.##")
            Else
                pesoCajaPromedio = 0
                lblPesoPCaja.Text = 0
            End If

            sql = ""
            sql = " SELECT IFNULL(avg(peso),0) pesoRacimoPromedio FROM tblRegistroLotes"
            sql &= " WHERE idR11=" & lblNumero.Text


            resultado = datos.ejecutarMySql(sql)
            If resultado.Rows.Count > 0 Then
                pesoRacimoPromedio = resultado.Rows(0).Item("pesoRacimoPromedio")
                lblPesoPRacimo.Text = Format(pesoRacimoPromedio, "####.##")
            Else
                pesoRacimoPromedio = 0
                lblPesoPRacimo.Text = 0
            End If

            Dim merma As Double = 0

            merma = Math.Abs((((pesoCajaPromedio * CDbl(lblRatioCajasTransformadas.Text)) / (pesoRacimoPromedio)) - 1) * 100)
            lblMerma.Text = Format(merma, "###0.##")

            sql = ""
            sql = "UPDATE tblCR11 SET merma=" & (merma / 100) & " WHERE id=" & lblNumero.Text
            datos.ejecutarMySql(sql)

            'Fundas recuperadas
            sql = ""
            sql = "SELECT IFNULL((frecuperada/crecobrada) * 100,0) fundasRecuperadas FROM tblFundaRecuperada "
            sql &= " WHERE idR11=" & lblNumero.Text

            resultado = datos.ejecutarMySql(sql)
            If resultado.Rows.Count > 0 Then
                lblFRecuperada.Text = Format(resultado.Rows(0).Item("fundasRecuperadas"), "###.##")
            Else
                lblFRecuperada.Text = 0
            End If

            '% Cajas
            Dim primera As Double = 0
            Dim segunda As Double = 0

            sql = ""
            sql = " SELECT calidadc.descripcion,IFNULL(SUM(IFNULL(dato.cajas,0)),0) cajas FROM tblCalidadCaja calidadc "
            sql &= " LEFT JOIN ( "
            sql &= " SELECT cape.idMarca,cape.cantidadP cajas,caj.calidad FROM tblCajasPE cape "
            sql &= " LEFT JOIN tblCaja caj ON caj.id=cape.idMarca "
            sql &= " WHERE cape.idR11=" & lblNumero.Text & " ) AS dato ON dato.calidad=calidadc.id "
            sql &= " GROUP BY calidadc.descripcion "

            resultado = datos.ejecutarMySql(sql)

            If resultado.Rows.Count > 0 Then
                primera = resultado.Rows(0).Item("cajas") ' PRIMERA
                segunda = resultado.Rows(1).Item("cajas") ' SEGUNDA

                If primera <> 0 Then
                    lblCajaPrimera.Text = Format((primera / (primera + segunda)) * 100, "##.0#")
                Else
                    lblCajaPrimera.Text = 0
                End If

                If segunda <> 0 Then
                    lblCajaSegunda.Text = Format((segunda / (primera + segunda)) * 100, "##.0#")
                Else
                    lblCajaSegunda.Text = 0
                End If
            Else
                lblCajaPrimera.Text = 0
                lblCajaSegunda.Text = 0
            End If

            'Ratio Promedio lote total
            sql = ""
            sql = "SELECT idR11,ifnull(AVG(ratio),0) ratioPromedioLote FROM tblRegistroLotes WHERE idR11=" & lblNumero.Text

            resultado = datos.ejecutarMySql(sql)
            Dim ratioPromedioLote As Double = resultado.Rows(0).Item("ratioPromedioLote")
            If resultado.Rows.Count > 0 Then
                lblRatioPTotal.Text = Format(ratioPromedioLote, "##0.##")
            Else
                lblRatioPTotal.Text = 0
            End If

            'calibracion,nmanos,ldedos

            sql = ""
            sql = "SELECT IFNULL(AVG(calibracion),0) calibracion,IFNULL(AVG(nmanos),0) manos,IFNULL(AVG(ldedos),0) dedos FROM tblRegistroLotes"
            sql &= " WHERE idR11=" & lblNumero.Text

            resultado = datos.ejecutarMySql(sql)
            If resultado.Rows.Count > 0 Then
                lblCalibracion.Text = Format(resultado.Rows(0).Item("calibracion"), "##0.##")
                lblNManos.Text = Format(resultado.Rows(0).Item("manos"), "##0.##")
                lblLargoDedos.Text = Format(resultado.Rows(0).Item("dedos"), "##0.##")
            Else
                lblCalibracion.Text = 0
                lblNManos.Text = 0
                lblLargoDedos.Text = 0
            End If

            Try
                'Actualizar tabla
                sql = ""
                sql = " UPDATE tblCR11 SET "
                sql &= " peso= " & lblPesoPRacimo.Text
                sql &= " ,manos= " & lblNManos.Text
                sql &= " ,calibracion= " & lblCalibracion.Text
                sql &= " ,merma= " & lblMerma.Text
                sql &= " ,ratio= " & lblRatioCajasTransformadas.Text
                sql &= " WHERE id=" & lblNumero.Text

                datos.ejecutarMySql(sql)
            Catch ex As Exception

            End Try




        End If
    End Sub

    Sub validar()
        If txtHAS.TextLength > 0 Then
            validado = 1
        Else
            validado = 0
        End If

        If dtpFecha.Value < CDate(lblF1.Text) Or dtpFecha.Value > CDate(lblF2.Text) Then
            validado = 0
        End If

    End Sub
    Sub cargaCombos()

        Dim dtDatos As New DataTable
        dtpFecha.Value = Now
        lblNumero.Text = 0
        txtHAS.Text = 0
        txtObservaciones.Clear()
        txtMerma.Text = 0
        lblEstado.Text = "..."

        'Hacienda
        sql = "SELECT * FROM tblFinca"
        dtDatos = datos.ejecutarMySql(sql)
        cboHacienda.DataSource = dtDatos
        cboHacienda.ValueMember = "id"
        cboHacienda.DisplayMember = "descripcion"

        'Pais
        sql = "SELECT * FROM tblPais"
        dtDatos = datos.ejecutarMySql(sql)
        cboPais.DataSource = dtDatos
        cboPais.ValueMember = "id"
        cboPais.DisplayMember = "descripcion"


        'Cultivo
        sql = "SELECT * FROM tblCultivo"
        dtDatos = datos.ejecutarMySql(sql)
        cboCultivo.DataSource = dtDatos
        cboCultivo.ValueMember = "id"
        cboCultivo.DisplayMember = "descripcion"


        'Variedad
        sql = "SELECT * FROM tblVariedad"
        dtDatos = datos.ejecutarMySql(sql)
        cboVariedad.DataSource = dtDatos
        cboVariedad.ValueMember = "id"
        cboVariedad.DisplayMember = "descripcion"

        'Semana
        sql = "SELECT * FROM tblCalendario"
        dtDatos = datos.ejecutarMySql(sql)
        cboSemana.DataSource = dtDatos
        cboSemana.ValueMember = "id"
        cboSemana.DisplayMember = "semana"




    End Sub

    Private Sub cboSemana_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboSemana.SelectedValueChanged
        Try
            Dim dtDatos As New DataTable
            Dim idSemana As Integer = cboSemana.GetItemText(cboSemana.SelectedValue)

            sql = "SELECT * FROM tblCalendario WHERE id=" & idSemana
            dtDatos = datos.ejecutarMySql(sql)
            lblF1.Text = dtDatos.Rows(0).Item("fdesde")
            lblF2.Text = dtDatos.Rows(0).Item("fhasta")

            sql = "SELECT * FROM tblColor WHERE id=" & dtDatos.Rows(0).Item("idColor")
            dtDatos = datos.ejecutarMySql(sql)
            txtColor.BackColor = Color.FromArgb(dtDatos.Rows(0).Item("codigo"))
            dtDatos.Clear()
            GC.Collect()
        Catch ex As Exception
            'MessageBox.Show("No hay datos que mostrar", "BANPROD")
        End Try
        
    End Sub
    Private Sub dtpFecha_ValueChanged(sender As Object, e As EventArgs) Handles dtpFecha.ValueChanged
        lblDia.Text = Format(dtpFecha.Value, "dddd")
    End Sub

    Private Sub cboHacienda_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboHacienda.SelectedValueChanged

        Try
            Dim dtDatos As New DataTable
            sql = ""
            sql = "SELECT area FROM tblFinca WHERE id='" & cboHacienda.GetItemText(cboHacienda.SelectedValue) & "'"
            dtDatos = datos.ejecutarMySql(sql)

            txtArea.Text = dtDatos.Rows(0).Item("area").ToString
        Catch ex As Exception
            ' MessageBox.Show("No hay datos que mostrar", "BANPROD")
        End Try
        

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim idCabecera As Integer = 0
        validar()

        If lblEstado.Text <> "ANULADO" Then
            If validado = 1 Then

                If CInt(lblNumero.Text) = 0 Then
                    'Se inserta
                    sql = "INSERT INTO tblCR11 set "
                    sql &= "idFinca=" & cboHacienda.SelectedValue & ","
                    sql &= "idPais=" & cboPais.SelectedValue & ","
                    sql &= "idCultivo=" & cboCultivo.SelectedValue & ","
                    sql &= "idVariedad=" & cboVariedad.SelectedValue & ","
                    sql &= "idSemana=" & cboSemana.SelectedValue & ","
                    sql &= "fecha='" & Format(dtpFecha.Value, "yyyy-MM-dd") & "',"
                    sql &= "hInicia='" & txthInicia.Text & "',"
                    sql &= "hSale='" & txthFinal.Text & "',"
                    sql &= "has='" & txtHAS.Text & "',"
                    'sql &= "merma='" & txtMerma.Text & "',"
                    sql &= "observacion='" & txtObservaciones.Text & "',"
                    sql &= "estado='ACTIVO'"

                    datos.ejecutarMySql(sql)
                    idCabecera = datos.lastInsertId

                    lblNumero.Text = idCabecera
                    MessageBox.Show("Para su transaccion se ha generado el numero: " & idCabecera, "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    'Actualiza

                    sql = "UPDATE tblCR11 set "
                    sql &= "idFinca=" & cboHacienda.SelectedValue & ","
                    sql &= "idPais=" & cboPais.SelectedValue & ","
                    sql &= "idCultivo=" & cboCultivo.SelectedValue & ","
                    sql &= "idVariedad=" & cboVariedad.SelectedValue & ","
                    sql &= "idSemana=" & cboSemana.SelectedValue & ","
                    sql &= "fecha='" & Format(dtpFecha.Value, "yyyy-MM-dd") & "',"
                    sql &= "hInicia='" & txthInicia.Text & "',"
                    sql &= "hSale='" & txthFinal.Text & "',"
                    sql &= "has='" & txtHAS.Text & "',"
                    'sql &= "merma='" & txtMerma.Text & "',"
                    sql &= "observacion='" & txtObservaciones.Text & "'"
                    sql &= " WHERE id='" & CInt(lblNumero.Text) & "'"
                    datos.ejecutarMySql(sql)
                    MessageBox.Show("Su transacción ha sido actualizada", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                MessageBox.Show("Por favor complete la información o verifique el calendario bananero", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            End If
        Else
            MessageBox.Show("Los registros ANULADO no se pueden actualizar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub txtHAS_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtHAS.KeyPress, txthInicia.KeyPress, txthFinal.KeyPress
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf (e.KeyChar = "." And Not sender.Text.IndexOf(".")) Or (e.KeyChar = ":" And Not sender.Text.IndexOf(":")) Then
            e.Handled = True
        ElseIf e.KeyChar = "." Or e.KeyChar = ":" Then
            e.Handled = False
        Else
            e.Handled = True
        End If

    End Sub

    Private Sub txtObservaciones_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtObservaciones.KeyPress
        txtObservaciones.CharacterCasing = CharacterCasing.Upper
    End Sub

    Private Sub txtObservaciones_TextChanged(sender As Object, e As EventArgs) Handles txtObservaciones.TextChanged

    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click

        Try
            Dim dtDatos As New DataTable
            GC.Collect()
            codigoBuscado = 0

            frmBuscar.sql = "SELECT cr11.id,cr11.id codigo,CONCAT(ca.semana,'/',cr11.fecha,'/',fi.descripcion,'/',va.descripcion)semanaFechaHaciendaVariedad,cr11.estado FROM tblCR11 cr11"
            frmBuscar.sql &= " LEFT JOIN tblCalendario ca ON ca.id=cr11.idSemana"
            frmBuscar.sql &= " LEFT JOIN tblFinca fi on fi.id=cr11.idFinca"
            frmBuscar.sql &= " LEFT JOIN tblVariedad va on va.id=cr11.idVariedad"

            frmBuscar.ShowDialog()
            dtDatos.Clear()
            sql = ""
            sql = "SELECT * FROM tblCR11 WHERE id=" & codigoBuscado
            dtDatos = datos.ejecutarMySql(sql)

            If dtDatos.Rows.Count > 0 Then
                lblNumero.Text = dtDatos.Rows(0).Item("id")
                cboHacienda.SelectedValue = dtDatos.Rows(0).Item("idFinca")
                cboPais.SelectedValue = dtDatos.Rows(0).Item("idPais")
                cboCultivo.SelectedValue = dtDatos.Rows(0).Item("idCultivo")
                cboVariedad.SelectedValue = dtDatos.Rows(0).Item("idVariedad")
                cboSemana.SelectedValue = dtDatos.Rows(0).Item("idSemana")
                dtpFecha.Text = dtDatos.Rows(0).Item("fecha")
                txthInicia.Text = dtDatos.Rows(0).Item("hInicia")
                txthFinal.Text = dtDatos.Rows(0).Item("hSale")
                txtHAS.Text = dtDatos.Rows(0).Item("has")
                'txtMerma.Text = dtDatos.Rows(0).Item("merma")
                txtObservaciones.Text = dtDatos.Rows(0).Item("observacion")
                lblEstado.Text = dtDatos.Rows(0).Item("estado")
                cargarInfoAdicional()

            End If
        Catch ex As Exception
            MessageBox.Show("No hay datos que mostrar", "BANPROD")
        End Try
        
    End Sub

    Private Sub btnAnular_Click(sender As Object, e As EventArgs) Handles btnAnular.Click
        Dim flag As Integer
        If CInt(lblNumero.Text) > 0 And lblEstado.Text = "ACTIVO" Then
            flag = MessageBox.Show("Está seguro de anular el registro, no hay reverso en el proceso", "BRANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No

            If flag = 0 Then
                sql = ""
                sql = "UPDATE tblCR11 SET estado='ANULADO' WHERE id=" & lblNumero.Text
                datos.ejecutarMySql(sql)
                MessageBox.Show("Se ha anulado el registro actual", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cargaCombos()
            End If

        Else
            MessageBox.Show("Por favor busque un proceso NO ANULADO y proceda anular", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        cargaCombos()
        limpialLbl()
    End Sub

    

    Private Sub btnDetalle_Click(sender As Object, e As EventArgs) Handles btnDetalle.Click
        If CInt(lblNumero.Text) <> 0 Then
            frmR11Items.idR11 = lblNumero.Text
            frmR11Items.ShowDialog()
        Else
            MessageBox.Show("No hay registros con que asociar, por favor busque un R11 y vuelva a intentar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End If



    End Sub

    Private Sub btnCajas_Click(sender As Object, e As EventArgs) Handles btnCajas.Click
        If CInt(lblNumero.Text) <> 0 Then
            frmCajasIniciales.idR11 = lblNumero.Text
            frmCajasIniciales.ShowDialog()
        Else
            MessageBox.Show("No hay registros con que asociar, por favor busque un R11 y vuelva a intentar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End If
    End Sub
End Class