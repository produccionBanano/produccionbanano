﻿Public Class frmCajas
    Public sql As String = ""
    Public datos As New ConexionBase.conexionBase
    Public codigoEditar As Integer
    Private Sub frmCajas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarCombo()
    End Sub

    Sub cargarCombo()
        Dim dtDatos As New DataTable
        sql = ""

        'Cliente
        sql = "SELECT id,codigo,nombre FROM tblCliente"
        dtDatos = datos.ejecutarMySql(sql)
        cboCliente.DataSource = dtDatos
        cboCliente.ValueMember = "id"
        cboCliente.DisplayMember = "nombre"

        'Calidad
        sql = "SELECT id,descripcion FROM tblCalidadCaja"
        dtDatos = datos.ejecutarMySql(sql)
        cboCalinad.DataSource = dtDatos
        cboCalinad.ValueMember = "id"
        cboCalinad.DisplayMember = "descripcion"

        'Destino
        sql = "SELECT id,destino FROM tblDestinoCaja"
        dtDatos = datos.ejecutarMySql(sql)
        cboDestino.DataSource = dtDatos
        cboDestino.ValueMember = "id"
        cboDestino.DisplayMember = "destino"

        '


    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim dtDatos As New DataTable
        sql = ""

        Try
            'sql = "SELECT id,codigo,area FROM tblLote WHERE hacienda=" & cboHacienda.GetItemText(cboHacienda.SelectedValue)
            frmBuscar.sql = sql
            frmBuscar.ShowDialog()

            sql = "SELECT id,codigo,hacienda,area,responsable FROM tblLote"
            sql &= "  WHERE id=" & codigoBuscado

            dtDatos = datos.ejecutarMySql(sql)

            If dtDatos.Rows.Count > 0 Then
                codigoEditar = dtDatos.Rows(0).Item("id")
                txtCodigo.Text = dtDatos.Rows(0).Item("codigo")
                'cboHacienda.SelectedValue = dtDatos.Rows(0).Item("hacienda")
                'txtArea.Text = dtDatos.Rows(0).Item("area")
                'txtResponsable.Text = dtDatos.Rows(0).Item("responsable").ToString

                codigoBuscado = 0
            Else
                codigoBuscado = 0
                codigoEditar = 0
                Return
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
End Class