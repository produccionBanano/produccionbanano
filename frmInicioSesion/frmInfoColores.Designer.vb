﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInfoColores
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvRecobroLoteColor = New System.Windows.Forms.DataGridView()
        Me.dgvRecobroColor = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.dgvRecobroLoteColor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRecobroColor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvRecobroLoteColor
        '
        Me.dgvRecobroLoteColor.AllowUserToAddRows = False
        Me.dgvRecobroLoteColor.AllowUserToDeleteRows = False
        Me.dgvRecobroLoteColor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRecobroLoteColor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRecobroLoteColor.Location = New System.Drawing.Point(10, 29)
        Me.dgvRecobroLoteColor.Name = "dgvRecobroLoteColor"
        Me.dgvRecobroLoteColor.ReadOnly = True
        Me.dgvRecobroLoteColor.Size = New System.Drawing.Size(609, 113)
        Me.dgvRecobroLoteColor.TabIndex = 0
        '
        'dgvRecobroColor
        '
        Me.dgvRecobroColor.AllowUserToAddRows = False
        Me.dgvRecobroColor.AllowUserToDeleteRows = False
        Me.dgvRecobroColor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRecobroColor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRecobroColor.Location = New System.Drawing.Point(10, 173)
        Me.dgvRecobroColor.Name = "dgvRecobroColor"
        Me.dgvRecobroColor.ReadOnly = True
        Me.dgvRecobroColor.Size = New System.Drawing.Size(609, 133)
        Me.dgvRecobroColor.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(163, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Detalle de recobro lotes por color"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 157)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(138, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Detalle de recobro por color"
        '
        'frmInfoColores
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(631, 312)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgvRecobroColor)
        Me.Controls.Add(Me.dgvRecobroLoteColor)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmInfoColores"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BANPROD-COLORES RECOBRE"
        CType(Me.dgvRecobroLoteColor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRecobroColor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvRecobroLoteColor As System.Windows.Forms.DataGridView
    Friend WithEvents dgvRecobroColor As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
