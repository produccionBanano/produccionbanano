﻿Imports ConexionBase
Public Class frmCliente
    Public datos As New ConexionBase.conexionBase
    Public sql As String = ""
    Public codigoEditar As Integer
    Private Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtCodigo.TextChanged, txtNombre.TextChanged, txtDireccion.TextChanged
        sender.CharacterCasing = CharacterCasing.Upper
    End Sub

    Private Sub frmCliente_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
    Sub limpiarCampos()
        txtNombre.Clear()
        txtCodigo.Clear()
        txtIdentificacion.Clear()
        txtDireccion.Clear()
        txtTelefono.Clear()
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click

        Dim dtDatos As New DataTable
        sql = ""

        Try
            sql = "SELECT id,codigo,nombre FROM tblCliente "
            frmBuscar.sql = sql
            frmBuscar.ShowDialog()

            sql = "SELECT id,ifnull(codigo,0) codigo,nombre,ifnull(identificacion,0) identificacion,"
            sql &= " ifnull(direccion,'') direccion,ifnull(telefono,0) telefono FROM tblCliente WHERE id=" & codigoBuscado

            dtDatos = datos.ejecutarMySql(sql)

            If dtDatos.Rows.Count > 0 Then
                codigoEditar = dtDatos.Rows(0).Item("id")
                txtNombre.Text = dtDatos.Rows(0).Item("nombre")
                txtIdentificacion.Text = dtDatos.Rows(0).Item("identificacion")
                txtDireccion.Text = dtDatos.Rows(0).Item("direccion")
                txtTelefono.Text = dtDatos.Rows(0).Item("telefono")
                txtCodigo.Text = dtDatos.Rows(0).Item("codigo")

                codigoBuscado = 0
            Else
                codigoBuscado = 0
                codigoEditar = 0
                Return
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        limpiarCampos()
        codigoEditar = 0
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim dtDatos As New DataTable
        Try

            If codigoEditar = 0 Then
                sql = ""
                sql = "INSERT INTO tblCliente SET "
                sql &= " codigo='" & txtCodigo.Text & "'"
                sql &= " ,identificacion='" & txtIdentificacion.Text & "'"
                sql &= " ,nombre='" & txtNombre.Text & "'"
                sql &= " ,direccion='" & txtDireccion.Text & "'"
                sql &= " ,telefono=" & txtTelefono.Text

                dtDatos = datos.ejecutarMySql(sql)

                MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Else
                sql = ""
                sql = "UPDATE tblCliente SET "
                sql &= " codigo='" & txtCodigo.Text & "'"
                sql &= " ,identificacion='" & txtIdentificacion.Text & "'"
                sql &= " ,nombre='" & txtNombre.Text & "'"
                sql &= " ,direccion='" & txtDireccion.Text & "'"
                sql &= " ,telefono='" & txtTelefono.Text & "'"
                sql &= " WHERE id=" & codigoEditar

                dtDatos = datos.ejecutarMySql(sql)

                MessageBox.Show("Datos actualizados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        MessageBox.Show("Los clientes no pueden ser eliminados", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Class