﻿Imports ConexionBase
Public Class frmEnfundeBanano
    Public sql As String = ""
    Public datos As New ConexionBase.conexionBase

    Private Sub btnEnfunde_Click(sender As Object, e As EventArgs) Handles btnEnfunde.Click
        Dim info As New DataTable
        sql = ""

        Try
            If txtCantidad.TextLength > 0 Then
                sql = "INSERT INTO tblEnfundeProceso SET "
                sql &= " idCalendario=" & cboSemana.GetItemText(cboSemana.SelectedValue)
                sql &= " ,idLote=" & cboLote.GetItemText(cboLote.SelectedValue)
                sql &= " ,idFinca=" & cboHacienda.GetItemText(cboHacienda.SelectedValue)
                sql &= " ,cantidad=" & txtCantidad.Text

                datos.ejecutarMySql(sql)

                MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                cargarEnfunde()
            Else
                MessageBox.Show("Por favor ingrese datos antes de guardar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub
    Sub cargarEnfunde()
        Dim info As New DataTable
        sql = ""
        sql = "SELECT enfu.id,enfu.idCalendario,enfu.idFinca,enfu.idLote,cal.anio,cal.semana,lo.codigo lote,enfu.cantidad FROM tblEnfundeProceso enfu "
        sql &= " LEFT JOIN tblCalendario cal ON cal.id=enfu.idCalendario AND cal.anio='" & cboAnio.GetItemText(cboAnio.SelectedItem) & "'"
        sql &= " LEFT JOIN tblLote lo ON lo.id=enfu.idLote "
        sql &= " WHERE enfu.idFinca= " & fincaUsuario
        sql &= " AND enfu.idCalendario= '" & cboSemana.GetItemText(cboSemana.SelectedValue) & "'"

        info = datos.ejecutarMySql(sql)

        dgvEnfunde.DataSource = info
        dgvEnfunde.Columns("id").Visible = False
        dgvEnfunde.Columns("idCalendario").Visible = False
        dgvEnfunde.Columns("idLote").Visible = False
        dgvEnfunde.Columns("idFinca").Visible = False

        Dim total As Decimal = 0
        For i = 0 To dgvEnfunde.Rows.Count - 1
            total = total + dgvEnfunde.Item("cantidad", i).Value
        Next
        lblTotal.Text = total


    End Sub

    Sub cargarCombo()
        Dim dtDatos As New DataTable

        'Hacienda
        sql = "SELECT * FROM tblFinca"
        dtDatos = datos.ejecutarMySql(sql)
        cboHacienda.DataSource = dtDatos
        cboHacienda.ValueMember = "id"
        cboHacienda.DisplayMember = "descripcion"

        'Lotes
        sql = "SELECT * FROM tblLote WHERE hacienda=" & fincaUsuario
        dtDatos = datos.ejecutarMySql(sql)
        cboLote.DataSource = dtDatos
        cboLote.ValueMember = "id"
        cboLote.DisplayMember = "codigo"
    End Sub

    Private Sub frmEnfundeBanano_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboAnio.SelectedItem = Year(Date.Now)
        cargarCombo()
        cargarEnfunde()
    End Sub

    Private Sub cboSemana_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSemana.SelectedIndexChanged

    End Sub

    Private Sub cboSemana_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboSemana.SelectedValueChanged
        Dim info As New DataTable

        sql = ""
        sql = "SELECT color.codigo FROM tblCalendario cal "
        sql &= " LEFT JOIN tblColor color ON color.id=cal.idColor"
        sql &= " WHERE cal.id='" & cboSemana.GetItemText(cboSemana.SelectedValue) & "'"

        info = datos.ejecutarMySql(sql)
        If info.Rows.Count > 0 Then
            txtColorSemanaR.BackColor = Color.FromArgb(info.Rows(0).Item("codigo"))
        End If
        cargarEnfunde()
    End Sub

    Private Sub btnEliminarRecobre_Click(sender As Object, e As EventArgs) Handles btnEliminarRecobre.Click
        Dim idSeleccionado As String = Me.dgvEnfunde.CurrentRow.Cells("id").Value
        Dim flag As Integer
        flag = MessageBox.Show("Esta seguro de eliminar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No
        If flag = 0 Then
            sql = ""
            sql = " DELETE FROM tblEnfundeProceso WHERE id=" & idSeleccionado
            datos.ejecutarMySql(sql)
            MessageBox.Show("El registro ha sido eliminado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cargarEnfunde()
        End If
    End Sub

    Private Sub cboAnio_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboAnio.SelectedValueChanged
        Dim dtDatos As New DataTable
        'Semanas
        sql = "SELECT * FROM tblCalendario WHERE anio='" & cboAnio.GetItemText(cboAnio.SelectedItem) & "'"
        dtDatos = datos.ejecutarMySql(sql)
        cboSemana.DataSource = dtDatos
        cboSemana.ValueMember = "id"
        cboSemana.DisplayMember = "semana"
    End Sub
End Class