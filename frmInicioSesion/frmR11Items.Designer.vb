﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmR11Items
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmR11Items))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblR11 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.tcGeneral = New System.Windows.Forms.TabControl()
        Me.recobre = New System.Windows.Forms.TabPage()
        Me.lblProcesado = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.txtChapeado = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.lblPRecobrado = New System.Windows.Forms.Label()
        Me.lblRecobrado = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cboEdadR = New System.Windows.Forms.ComboBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.cboColorR = New System.Windows.Forms.ComboBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.CboSemanaR = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboAnioR = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboSemanaE = New System.Windows.Forms.ComboBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtColorSemanaF = New System.Windows.Forms.TextBox()
        Me.cboAnioE = New System.Windows.Forms.ComboBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.btnEliminarRecobre = New System.Windows.Forms.Button()
        Me.btnGuardarRecobre = New System.Windows.Forms.Button()
        Me.dgvRecobre = New System.Windows.Forms.DataGridView()
        Me.txtRechazadoR = New System.Windows.Forms.TextBox()
        Me.txtCantidadR = New System.Windows.Forms.TextBox()
        Me.cboLoteR = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.rechazado = New System.Windows.Forms.TabPage()
        Me.btnEliminaRechazado = New System.Windows.Forms.Button()
        Me.btnGuardarRechazado = New System.Windows.Forms.Button()
        Me.dgvRacimosRechazados = New System.Windows.Forms.DataGridView()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtCantidadRechazado = New System.Windows.Forms.TextBox()
        Me.cboDefectoRechazado = New System.Windows.Forms.ComboBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.cboLoteRechazado = New System.Windows.Forms.ComboBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.otro = New System.Windows.Forms.TabPage()
        Me.btnRatiosLotes = New System.Windows.Forms.Button()
        Me.dgvRLotes = New System.Windows.Forms.DataGridView()
        Me.txtMellizos = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtNumManos = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtLargoDedo = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtCalibracion = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtPeso = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtRecorrido = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboLoteOtros = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.btnNuevoLotes = New System.Windows.Forms.Button()
        Me.btnEliminaRegistroLote = New System.Windows.Forms.Button()
        Me.btnGuardarOtros = New System.Windows.Forms.Button()
        Me.caja = New System.Windows.Forms.TabPage()
        Me.txtSaldoCajas = New System.Windows.Forms.TextBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.cboTipoTransporte = New System.Windows.Forms.ComboBox()
        Me.lblProcesados = New System.Windows.Forms.Label()
        Me.lblEnviados = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.chkHabilitar = New System.Windows.Forms.CheckBox()
        Me.txtCod = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtVapor = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtTransporte = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtCantidadCajasE = New System.Windows.Forms.TextBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.cboCliente = New System.Windows.Forms.ComboBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.dgvCajasPE = New System.Windows.Forms.DataGridView()
        Me.txtCantidadCajas = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.cboMarca = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.btnNuevoCaja = New System.Windows.Forms.Button()
        Me.btnEliminaCaja = New System.Windows.Forms.Button()
        Me.btnGuardarCajas = New System.Windows.Forms.Button()
        Me.saco = New System.Windows.Forms.TabPage()
        Me.dgvRechazadoSacos = New System.Windows.Forms.DataGridView()
        Me.txtSacos = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtPersonaSacos = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.cboManosSacos = New System.Windows.Forms.ComboBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.btnEliminaSacos = New System.Windows.Forms.Button()
        Me.btnGuardarSacos = New System.Windows.Forms.Button()
        Me.labores = New System.Windows.Forms.TabPage()
        Me.btnEliminaLabores = New System.Windows.Forms.Button()
        Me.btnGuardarLabores = New System.Windows.Forms.Button()
        Me.dgvLabores = New System.Windows.Forms.DataGridView()
        Me.txtPesoLabores = New System.Windows.Forms.TextBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.txtPersonaLabores = New System.Windows.Forms.TextBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.cboLabores = New System.Windows.Forms.ComboBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.empleados = New System.Windows.Forms.TabPage()
        Me.dgvEvolution = New System.Windows.Forms.DataGridView()
        Me.btnConsultarEvolution = New System.Windows.Forms.Button()
        Me.funda = New System.Windows.Forms.TabPage()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.txtRecuperado = New System.Windows.Forms.TextBox()
        Me.txtFundaRecuperadas = New System.Windows.Forms.TextBox()
        Me.txtCantidadRecobrada = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnGuardarFundas = New System.Windows.Forms.Button()
        Me.cMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SaldosEnfundeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tcGeneral.SuspendLayout()
        Me.recobre.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvRecobre, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.rechazado.SuspendLayout()
        CType(Me.dgvRacimosRechazados, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.otro.SuspendLayout()
        CType(Me.dgvRLotes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.caja.SuspendLayout()
        CType(Me.dgvCajasPE, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.saco.SuspendLayout()
        CType(Me.dgvRechazadoSacos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.labores.SuspendLayout()
        CType(Me.dgvLabores, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.empleados.SuspendLayout()
        CType(Me.dgvEvolution, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.funda.SuspendLayout()
        Me.cMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(132, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Codigo R11 seleccionado:"
        '
        'lblR11
        '
        Me.lblR11.AutoSize = True
        Me.lblR11.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblR11.Location = New System.Drawing.Point(11, 34)
        Me.lblR11.Name = "lblR11"
        Me.lblR11.Size = New System.Drawing.Size(33, 25)
        Me.lblR11.TabIndex = 10
        Me.lblR11.Text = "..."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(364, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Por favor proceda a ingresar el detalle de información para el formulario R11"
        '
        'tcGeneral
        '
        Me.tcGeneral.Controls.Add(Me.recobre)
        Me.tcGeneral.Controls.Add(Me.rechazado)
        Me.tcGeneral.Controls.Add(Me.otro)
        Me.tcGeneral.Controls.Add(Me.caja)
        Me.tcGeneral.Controls.Add(Me.saco)
        Me.tcGeneral.Controls.Add(Me.labores)
        Me.tcGeneral.Controls.Add(Me.empleados)
        Me.tcGeneral.Controls.Add(Me.funda)
        Me.tcGeneral.Location = New System.Drawing.Point(14, 93)
        Me.tcGeneral.Name = "tcGeneral"
        Me.tcGeneral.SelectedIndex = 0
        Me.tcGeneral.Size = New System.Drawing.Size(651, 409)
        Me.tcGeneral.TabIndex = 12
        '
        'recobre
        '
        Me.recobre.Controls.Add(Me.lblProcesado)
        Me.recobre.Controls.Add(Me.Label47)
        Me.recobre.Controls.Add(Me.txtChapeado)
        Me.recobre.Controls.Add(Me.Label42)
        Me.recobre.Controls.Add(Me.LinkLabel1)
        Me.recobre.Controls.Add(Me.lblPRecobrado)
        Me.recobre.Controls.Add(Me.lblRecobrado)
        Me.recobre.Controls.Add(Me.Label41)
        Me.recobre.Controls.Add(Me.Label40)
        Me.recobre.Controls.Add(Me.GroupBox2)
        Me.recobre.Controls.Add(Me.GroupBox1)
        Me.recobre.Controls.Add(Me.btnEliminarRecobre)
        Me.recobre.Controls.Add(Me.btnGuardarRecobre)
        Me.recobre.Controls.Add(Me.dgvRecobre)
        Me.recobre.Controls.Add(Me.txtRechazadoR)
        Me.recobre.Controls.Add(Me.txtCantidadR)
        Me.recobre.Controls.Add(Me.cboLoteR)
        Me.recobre.Controls.Add(Me.Label6)
        Me.recobre.Controls.Add(Me.Label5)
        Me.recobre.Controls.Add(Me.Label4)
        Me.recobre.Location = New System.Drawing.Point(4, 22)
        Me.recobre.Name = "recobre"
        Me.recobre.Padding = New System.Windows.Forms.Padding(3)
        Me.recobre.Size = New System.Drawing.Size(643, 383)
        Me.recobre.TabIndex = 0
        Me.recobre.Text = "Recobre"
        Me.recobre.UseVisualStyleBackColor = True
        '
        'lblProcesado
        '
        Me.lblProcesado.AutoSize = True
        Me.lblProcesado.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcesado.Location = New System.Drawing.Point(504, 129)
        Me.lblProcesado.Name = "lblProcesado"
        Me.lblProcesado.Size = New System.Drawing.Size(28, 24)
        Me.lblProcesado.TabIndex = 24
        Me.lblProcesado.Text = "..."
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(241, 137)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(257, 13)
        Me.Label47.TabIndex = 23
        Me.Label47.Text = "( + Recobrado - Rechazado - Chapeado)  Procesado"
        '
        'txtChapeado
        '
        Me.txtChapeado.Location = New System.Drawing.Point(506, 95)
        Me.txtChapeado.MaxLength = 6
        Me.txtChapeado.Name = "txtChapeado"
        Me.txtChapeado.Size = New System.Drawing.Size(61, 20)
        Me.txtChapeado.TabIndex = 22
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(435, 98)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(56, 13)
        Me.Label42.TabIndex = 21
        Me.Label42.Text = "Chapeado"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Location = New System.Drawing.Point(17, 347)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(117, 13)
        Me.LinkLabel1.TabIndex = 20
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Información por colores"
        '
        'lblPRecobrado
        '
        Me.lblPRecobrado.AutoSize = True
        Me.lblPRecobrado.Location = New System.Drawing.Point(588, 363)
        Me.lblPRecobrado.Name = "lblPRecobrado"
        Me.lblPRecobrado.Size = New System.Drawing.Size(16, 13)
        Me.lblPRecobrado.TabIndex = 19
        Me.lblPRecobrado.Text = "..."
        Me.lblPRecobrado.Visible = False
        '
        'lblRecobrado
        '
        Me.lblRecobrado.AutoSize = True
        Me.lblRecobrado.Location = New System.Drawing.Point(588, 347)
        Me.lblRecobrado.Name = "lblRecobrado"
        Me.lblRecobrado.Size = New System.Drawing.Size(16, 13)
        Me.lblRecobrado.TabIndex = 18
        Me.lblRecobrado.Text = "..."
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(527, 363)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(62, 13)
        Me.Label41.TabIndex = 17
        Me.Label41.Text = "% Recobro:"
        Me.Label41.Visible = False
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Location = New System.Drawing.Point(527, 347)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(63, 13)
        Me.Label40.TabIndex = 16
        Me.Label40.Text = "Recobrado:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboEdadR)
        Me.GroupBox2.Controls.Add(Me.Label39)
        Me.GroupBox2.Controls.Add(Me.cboColorR)
        Me.GroupBox2.Controls.Add(Me.Label38)
        Me.GroupBox2.Controls.Add(Me.CboSemanaR)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.cboAnioR)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Location = New System.Drawing.Point(151, 9)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(269, 113)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Recobre"
        '
        'cboEdadR
        '
        Me.cboEdadR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEdadR.FormattingEnabled = True
        Me.cboEdadR.Items.AddRange(New Object() {"9", "10", "11", "12", "13", "14", "15", "16"})
        Me.cboEdadR.Location = New System.Drawing.Point(14, 41)
        Me.cboEdadR.Name = "cboEdadR"
        Me.cboEdadR.Size = New System.Drawing.Size(58, 21)
        Me.cboEdadR.TabIndex = 17
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(11, 24)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(32, 13)
        Me.Label39.TabIndex = 16
        Me.Label39.Text = "Edad"
        '
        'cboColorR
        '
        Me.cboColorR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboColorR.Enabled = False
        Me.cboColorR.FormattingEnabled = True
        Me.cboColorR.Location = New System.Drawing.Point(167, 41)
        Me.cboColorR.Name = "cboColorR"
        Me.cboColorR.Size = New System.Drawing.Size(85, 21)
        Me.cboColorR.TabIndex = 15
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(166, 23)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(74, 13)
        Me.Label38.TabIndex = 14
        Me.Label38.Text = "Color Enfunde"
        '
        'CboSemanaR
        '
        Me.CboSemanaR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CboSemanaR.Enabled = False
        Me.CboSemanaR.FormattingEnabled = True
        Me.CboSemanaR.Location = New System.Drawing.Point(167, 82)
        Me.CboSemanaR.Name = "CboSemanaR"
        Me.CboSemanaR.Size = New System.Drawing.Size(58, 21)
        Me.CboSemanaR.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(164, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Semana Recobro"
        '
        'cboAnioR
        '
        Me.cboAnioR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnioR.Enabled = False
        Me.cboAnioR.FormattingEnabled = True
        Me.cboAnioR.Items.AddRange(New Object() {"2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"})
        Me.cboAnioR.Location = New System.Drawing.Point(14, 83)
        Me.cboAnioR.Name = "cboAnioR"
        Me.cboAnioR.Size = New System.Drawing.Size(58, 21)
        Me.cboAnioR.TabIndex = 13
        Me.cboAnioR.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(12, 68)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(69, 13)
        Me.Label16.TabIndex = 12
        Me.Label16.Text = "Año Enfunde"
        Me.Label16.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboSemanaE)
        Me.GroupBox1.Controls.Add(Me.Label36)
        Me.GroupBox1.Controls.Add(Me.txtColorSemanaF)
        Me.GroupBox1.Controls.Add(Me.cboAnioE)
        Me.GroupBox1.Controls.Add(Me.Label37)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(15, 9)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(130, 90)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Enfunde"
        '
        'cboSemanaE
        '
        Me.cboSemanaE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSemanaE.FormattingEnabled = True
        Me.cboSemanaE.Location = New System.Drawing.Point(61, 49)
        Me.cboSemanaE.Name = "cboSemanaE"
        Me.cboSemanaE.Size = New System.Drawing.Size(58, 21)
        Me.cboSemanaE.TabIndex = 15
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(9, 57)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(46, 13)
        Me.Label36.TabIndex = 14
        Me.Label36.Text = "Semana"
        '
        'txtColorSemanaF
        '
        Me.txtColorSemanaF.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtColorSemanaF.Location = New System.Drawing.Point(61, 74)
        Me.txtColorSemanaF.MaxLength = 4
        Me.txtColorSemanaF.Multiline = True
        Me.txtColorSemanaF.Name = "txtColorSemanaF"
        Me.txtColorSemanaF.Size = New System.Drawing.Size(58, 5)
        Me.txtColorSemanaF.TabIndex = 11
        '
        'cboAnioE
        '
        Me.cboAnioE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnioE.FormattingEnabled = True
        Me.cboAnioE.Location = New System.Drawing.Point(61, 21)
        Me.cboAnioE.Name = "cboAnioE"
        Me.cboAnioE.Size = New System.Drawing.Size(58, 21)
        Me.cboAnioE.TabIndex = 17
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Location = New System.Drawing.Point(9, 27)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(26, 13)
        Me.Label37.TabIndex = 16
        Me.Label37.Text = "Año"
        '
        'btnEliminarRecobre
        '
        Me.btnEliminarRecobre.Image = CType(resources.GetObject("btnEliminarRecobre.Image"), System.Drawing.Image)
        Me.btnEliminarRecobre.Location = New System.Drawing.Point(585, 46)
        Me.btnEliminarRecobre.Name = "btnEliminarRecobre"
        Me.btnEliminarRecobre.Size = New System.Drawing.Size(55, 42)
        Me.btnEliminarRecobre.TabIndex = 10
        Me.btnEliminarRecobre.UseVisualStyleBackColor = True
        '
        'btnGuardarRecobre
        '
        Me.btnGuardarRecobre.Image = CType(resources.GetObject("btnGuardarRecobre.Image"), System.Drawing.Image)
        Me.btnGuardarRecobre.Location = New System.Drawing.Point(585, 2)
        Me.btnGuardarRecobre.Name = "btnGuardarRecobre"
        Me.btnGuardarRecobre.Size = New System.Drawing.Size(55, 41)
        Me.btnGuardarRecobre.TabIndex = 9
        Me.btnGuardarRecobre.UseVisualStyleBackColor = True
        '
        'dgvRecobre
        '
        Me.dgvRecobre.AllowUserToAddRows = False
        Me.dgvRecobre.AllowUserToDeleteRows = False
        Me.dgvRecobre.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRecobre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRecobre.Location = New System.Drawing.Point(18, 167)
        Me.dgvRecobre.Name = "dgvRecobre"
        Me.dgvRecobre.ReadOnly = True
        Me.dgvRecobre.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRecobre.Size = New System.Drawing.Size(619, 175)
        Me.dgvRecobre.TabIndex = 8
        '
        'txtRechazadoR
        '
        Me.txtRechazadoR.Location = New System.Drawing.Point(506, 69)
        Me.txtRechazadoR.MaxLength = 6
        Me.txtRechazadoR.Name = "txtRechazadoR"
        Me.txtRechazadoR.Size = New System.Drawing.Size(61, 20)
        Me.txtRechazadoR.TabIndex = 7
        '
        'txtCantidadR
        '
        Me.txtCantidadR.Location = New System.Drawing.Point(506, 43)
        Me.txtCantidadR.MaxLength = 6
        Me.txtCantidadR.Name = "txtCantidadR"
        Me.txtCantidadR.Size = New System.Drawing.Size(61, 20)
        Me.txtCantidadR.TabIndex = 6
        '
        'cboLoteR
        '
        Me.cboLoteR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoteR.FormattingEnabled = True
        Me.cboLoteR.Location = New System.Drawing.Point(506, 16)
        Me.cboLoteR.Name = "cboLoteR"
        Me.cboLoteR.Size = New System.Drawing.Size(61, 21)
        Me.cboLoteR.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(435, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 13)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Rechazados"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(435, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Recobrado:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(435, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(28, 13)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Lote"
        '
        'rechazado
        '
        Me.rechazado.Controls.Add(Me.btnEliminaRechazado)
        Me.rechazado.Controls.Add(Me.btnGuardarRechazado)
        Me.rechazado.Controls.Add(Me.dgvRacimosRechazados)
        Me.rechazado.Controls.Add(Me.Label33)
        Me.rechazado.Controls.Add(Me.txtCantidadRechazado)
        Me.rechazado.Controls.Add(Me.cboDefectoRechazado)
        Me.rechazado.Controls.Add(Me.Label32)
        Me.rechazado.Controls.Add(Me.cboLoteRechazado)
        Me.rechazado.Controls.Add(Me.Label31)
        Me.rechazado.Location = New System.Drawing.Point(4, 22)
        Me.rechazado.Name = "rechazado"
        Me.rechazado.Size = New System.Drawing.Size(643, 383)
        Me.rechazado.TabIndex = 6
        Me.rechazado.Text = "Rechazados"
        Me.rechazado.UseVisualStyleBackColor = True
        '
        'btnEliminaRechazado
        '
        Me.btnEliminaRechazado.Image = CType(resources.GetObject("btnEliminaRechazado.Image"), System.Drawing.Image)
        Me.btnEliminaRechazado.Location = New System.Drawing.Point(580, 55)
        Me.btnEliminaRechazado.Name = "btnEliminaRechazado"
        Me.btnEliminaRechazado.Size = New System.Drawing.Size(55, 42)
        Me.btnEliminaRechazado.TabIndex = 42
        Me.btnEliminaRechazado.UseVisualStyleBackColor = True
        '
        'btnGuardarRechazado
        '
        Me.btnGuardarRechazado.Image = CType(resources.GetObject("btnGuardarRechazado.Image"), System.Drawing.Image)
        Me.btnGuardarRechazado.Location = New System.Drawing.Point(580, 8)
        Me.btnGuardarRechazado.Name = "btnGuardarRechazado"
        Me.btnGuardarRechazado.Size = New System.Drawing.Size(55, 41)
        Me.btnGuardarRechazado.TabIndex = 41
        Me.btnGuardarRechazado.UseVisualStyleBackColor = True
        '
        'dgvRacimosRechazados
        '
        Me.dgvRacimosRechazados.AllowUserToAddRows = False
        Me.dgvRacimosRechazados.AllowUserToDeleteRows = False
        Me.dgvRacimosRechazados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRacimosRechazados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRacimosRechazados.Location = New System.Drawing.Point(23, 62)
        Me.dgvRacimosRechazados.Name = "dgvRacimosRechazados"
        Me.dgvRacimosRechazados.ReadOnly = True
        Me.dgvRacimosRechazados.Size = New System.Drawing.Size(477, 316)
        Me.dgvRacimosRechazados.TabIndex = 40
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(398, 29)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(52, 13)
        Me.Label33.TabIndex = 39
        Me.Label33.Text = "Cantidad:"
        '
        'txtCantidadRechazado
        '
        Me.txtCantidadRechazado.Location = New System.Drawing.Point(456, 26)
        Me.txtCantidadRechazado.MaxLength = 6
        Me.txtCantidadRechazado.Name = "txtCantidadRechazado"
        Me.txtCantidadRechazado.Size = New System.Drawing.Size(44, 20)
        Me.txtCantidadRechazado.TabIndex = 38
        '
        'cboDefectoRechazado
        '
        Me.cboDefectoRechazado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDefectoRechazado.FormattingEnabled = True
        Me.cboDefectoRechazado.Location = New System.Drawing.Point(187, 26)
        Me.cboDefectoRechazado.Name = "cboDefectoRechazado"
        Me.cboDefectoRechazado.Size = New System.Drawing.Size(193, 21)
        Me.cboDefectoRechazado.TabIndex = 37
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(133, 29)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(48, 13)
        Me.Label32.TabIndex = 36
        Me.Label32.Text = "Defecto:"
        '
        'cboLoteRechazado
        '
        Me.cboLoteRechazado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoteRechazado.FormattingEnabled = True
        Me.cboLoteRechazado.Location = New System.Drawing.Point(57, 26)
        Me.cboLoteRechazado.Name = "cboLoteRechazado"
        Me.cboLoteRechazado.Size = New System.Drawing.Size(60, 21)
        Me.cboLoteRechazado.TabIndex = 35
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(20, 29)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(31, 13)
        Me.Label31.TabIndex = 34
        Me.Label31.Text = "Lote:"
        '
        'otro
        '
        Me.otro.Controls.Add(Me.btnRatiosLotes)
        Me.otro.Controls.Add(Me.dgvRLotes)
        Me.otro.Controls.Add(Me.txtMellizos)
        Me.otro.Controls.Add(Me.Label17)
        Me.otro.Controls.Add(Me.txtNumManos)
        Me.otro.Controls.Add(Me.Label15)
        Me.otro.Controls.Add(Me.txtLargoDedo)
        Me.otro.Controls.Add(Me.Label14)
        Me.otro.Controls.Add(Me.txtCalibracion)
        Me.otro.Controls.Add(Me.Label13)
        Me.otro.Controls.Add(Me.txtPeso)
        Me.otro.Controls.Add(Me.Label12)
        Me.otro.Controls.Add(Me.txtRecorrido)
        Me.otro.Controls.Add(Me.Label11)
        Me.otro.Controls.Add(Me.cboLoteOtros)
        Me.otro.Controls.Add(Me.Label10)
        Me.otro.Controls.Add(Me.btnNuevoLotes)
        Me.otro.Controls.Add(Me.btnEliminaRegistroLote)
        Me.otro.Controls.Add(Me.btnGuardarOtros)
        Me.otro.Location = New System.Drawing.Point(4, 22)
        Me.otro.Name = "otro"
        Me.otro.Size = New System.Drawing.Size(643, 383)
        Me.otro.TabIndex = 2
        Me.otro.Text = "Lotes"
        Me.otro.UseVisualStyleBackColor = True
        '
        'btnRatiosLotes
        '
        Me.btnRatiosLotes.Location = New System.Drawing.Point(552, 336)
        Me.btnRatiosLotes.Name = "btnRatiosLotes"
        Me.btnRatiosLotes.Size = New System.Drawing.Size(55, 42)
        Me.btnRatiosLotes.TabIndex = 44
        Me.btnRatiosLotes.Text = "Ratios Lotes"
        Me.btnRatiosLotes.UseVisualStyleBackColor = True
        '
        'dgvRLotes
        '
        Me.dgvRLotes.AllowUserToAddRows = False
        Me.dgvRLotes.AllowUserToDeleteRows = False
        Me.dgvRLotes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRLotes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRLotes.Location = New System.Drawing.Point(16, 100)
        Me.dgvRLotes.Name = "dgvRLotes"
        Me.dgvRLotes.ReadOnly = True
        Me.dgvRLotes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRLotes.Size = New System.Drawing.Size(530, 278)
        Me.dgvRLotes.TabIndex = 24
        '
        'txtMellizos
        '
        Me.txtMellizos.Location = New System.Drawing.Point(329, 64)
        Me.txtMellizos.MaxLength = 6
        Me.txtMellizos.Name = "txtMellizos"
        Me.txtMellizos.Size = New System.Drawing.Size(50, 20)
        Me.txtMellizos.TabIndex = 21
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(276, 67)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(47, 13)
        Me.Label17.TabIndex = 20
        Me.Label17.Text = "Mellizos:"
        '
        'txtNumManos
        '
        Me.txtNumManos.Location = New System.Drawing.Point(207, 61)
        Me.txtNumManos.MaxLength = 6
        Me.txtNumManos.Name = "txtNumManos"
        Me.txtNumManos.Size = New System.Drawing.Size(49, 20)
        Me.txtNumManos.TabIndex = 17
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(144, 64)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(57, 13)
        Me.Label15.TabIndex = 16
        Me.Label15.Text = "Nº Manos:"
        '
        'txtLargoDedo
        '
        Me.txtLargoDedo.Location = New System.Drawing.Point(83, 61)
        Me.txtLargoDedo.MaxLength = 6
        Me.txtLargoDedo.Name = "txtLargoDedo"
        Me.txtLargoDedo.Size = New System.Drawing.Size(50, 20)
        Me.txtLargoDedo.TabIndex = 15
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(13, 64)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 13)
        Me.Label14.TabIndex = 14
        Me.Label14.Text = "Largo dedo:"
        '
        'txtCalibracion
        '
        Me.txtCalibracion.Location = New System.Drawing.Point(427, 20)
        Me.txtCalibracion.MaxLength = 6
        Me.txtCalibracion.Name = "txtCalibracion"
        Me.txtCalibracion.Size = New System.Drawing.Size(50, 20)
        Me.txtCalibracion.TabIndex = 13
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(365, 23)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(62, 13)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Calibración:"
        '
        'txtPeso
        '
        Me.txtPeso.Location = New System.Drawing.Point(293, 17)
        Me.txtPeso.MaxLength = 6
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(50, 20)
        Me.txtPeso.TabIndex = 11
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(253, 20)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(34, 13)
        Me.Label12.TabIndex = 10
        Me.Label12.Text = "Peso:"
        '
        'txtRecorrido
        '
        Me.txtRecorrido.Location = New System.Drawing.Point(179, 17)
        Me.txtRecorrido.MaxLength = 6
        Me.txtRecorrido.Name = "txtRecorrido"
        Me.txtRecorrido.Size = New System.Drawing.Size(50, 20)
        Me.txtRecorrido.TabIndex = 9
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(117, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 13)
        Me.Label11.TabIndex = 8
        Me.Label11.Text = "Recorrido:"
        '
        'cboLoteOtros
        '
        Me.cboLoteOtros.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoteOtros.FormattingEnabled = True
        Me.cboLoteOtros.Location = New System.Drawing.Point(47, 16)
        Me.cboLoteOtros.Name = "cboLoteOtros"
        Me.cboLoteOtros.Size = New System.Drawing.Size(56, 21)
        Me.cboLoteOtros.TabIndex = 7
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(13, 20)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(28, 13)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Lote"
        '
        'btnNuevoLotes
        '
        Me.btnNuevoLotes.Image = CType(resources.GetObject("btnNuevoLotes.Image"), System.Drawing.Image)
        Me.btnNuevoLotes.Location = New System.Drawing.Point(521, 9)
        Me.btnNuevoLotes.Name = "btnNuevoLotes"
        Me.btnNuevoLotes.Size = New System.Drawing.Size(55, 41)
        Me.btnNuevoLotes.TabIndex = 43
        Me.btnNuevoLotes.UseVisualStyleBackColor = True
        '
        'btnEliminaRegistroLote
        '
        Me.btnEliminaRegistroLote.Image = CType(resources.GetObject("btnEliminaRegistroLote.Image"), System.Drawing.Image)
        Me.btnEliminaRegistroLote.Location = New System.Drawing.Point(582, 56)
        Me.btnEliminaRegistroLote.Name = "btnEliminaRegistroLote"
        Me.btnEliminaRegistroLote.Size = New System.Drawing.Size(55, 42)
        Me.btnEliminaRegistroLote.TabIndex = 23
        Me.btnEliminaRegistroLote.UseVisualStyleBackColor = True
        '
        'btnGuardarOtros
        '
        Me.btnGuardarOtros.Image = CType(resources.GetObject("btnGuardarOtros.Image"), System.Drawing.Image)
        Me.btnGuardarOtros.Location = New System.Drawing.Point(582, 9)
        Me.btnGuardarOtros.Name = "btnGuardarOtros"
        Me.btnGuardarOtros.Size = New System.Drawing.Size(55, 41)
        Me.btnGuardarOtros.TabIndex = 22
        Me.btnGuardarOtros.UseVisualStyleBackColor = True
        '
        'caja
        '
        Me.caja.Controls.Add(Me.txtSaldoCajas)
        Me.caja.Controls.Add(Me.Label46)
        Me.caja.Controls.Add(Me.Label43)
        Me.caja.Controls.Add(Me.cboTipoTransporte)
        Me.caja.Controls.Add(Me.lblProcesados)
        Me.caja.Controls.Add(Me.lblEnviados)
        Me.caja.Controls.Add(Me.Label44)
        Me.caja.Controls.Add(Me.Label45)
        Me.caja.Controls.Add(Me.chkHabilitar)
        Me.caja.Controls.Add(Me.txtCod)
        Me.caja.Controls.Add(Me.Label35)
        Me.caja.Controls.Add(Me.txtVapor)
        Me.caja.Controls.Add(Me.Label30)
        Me.caja.Controls.Add(Me.txtTransporte)
        Me.caja.Controls.Add(Me.Label34)
        Me.caja.Controls.Add(Me.txtCantidadCajasE)
        Me.caja.Controls.Add(Me.Label26)
        Me.caja.Controls.Add(Me.Label23)
        Me.caja.Controls.Add(Me.cboCliente)
        Me.caja.Controls.Add(Me.Label25)
        Me.caja.Controls.Add(Me.dgvCajasPE)
        Me.caja.Controls.Add(Me.txtCantidadCajas)
        Me.caja.Controls.Add(Me.Label24)
        Me.caja.Controls.Add(Me.cboMarca)
        Me.caja.Controls.Add(Me.Label22)
        Me.caja.Controls.Add(Me.btnNuevoCaja)
        Me.caja.Controls.Add(Me.btnEliminaCaja)
        Me.caja.Controls.Add(Me.btnGuardarCajas)
        Me.caja.Location = New System.Drawing.Point(4, 22)
        Me.caja.Name = "caja"
        Me.caja.Size = New System.Drawing.Size(643, 383)
        Me.caja.TabIndex = 4
        Me.caja.Text = "Cajas"
        Me.caja.UseVisualStyleBackColor = True
        '
        'txtSaldoCajas
        '
        Me.txtSaldoCajas.Location = New System.Drawing.Point(62, 91)
        Me.txtSaldoCajas.MaxLength = 4
        Me.txtSaldoCajas.Name = "txtSaldoCajas"
        Me.txtSaldoCajas.Size = New System.Drawing.Size(54, 20)
        Me.txtSaldoCajas.TabIndex = 50
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(16, 93)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(44, 13)
        Me.Label46.TabIndex = 49
        Me.Label46.Text = "SaldoP:"
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Location = New System.Drawing.Point(128, 98)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(31, 13)
        Me.Label43.TabIndex = 48
        Me.Label43.Text = "Tipo:"
        '
        'cboTipoTransporte
        '
        Me.cboTipoTransporte.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipoTransporte.FormattingEnabled = True
        Me.cboTipoTransporte.Location = New System.Drawing.Point(165, 92)
        Me.cboTipoTransporte.Name = "cboTipoTransporte"
        Me.cboTipoTransporte.Size = New System.Drawing.Size(96, 21)
        Me.cboTipoTransporte.TabIndex = 47
        '
        'lblProcesados
        '
        Me.lblProcesados.AutoSize = True
        Me.lblProcesados.Location = New System.Drawing.Point(595, 364)
        Me.lblProcesados.Name = "lblProcesados"
        Me.lblProcesados.Size = New System.Drawing.Size(16, 13)
        Me.lblProcesados.TabIndex = 46
        Me.lblProcesados.Text = "..."
        '
        'lblEnviados
        '
        Me.lblEnviados.AutoSize = True
        Me.lblEnviados.Location = New System.Drawing.Point(595, 348)
        Me.lblEnviados.Name = "lblEnviados"
        Me.lblEnviados.Size = New System.Drawing.Size(16, 13)
        Me.lblEnviados.TabIndex = 45
        Me.lblEnviados.Text = "..."
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(531, 364)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(66, 13)
        Me.Label44.TabIndex = 44
        Me.Label44.Text = "Procesados:"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(531, 348)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(54, 13)
        Me.Label45.TabIndex = 43
        Me.Label45.Text = "Enviados:"
        '
        'chkHabilitar
        '
        Me.chkHabilitar.AutoSize = True
        Me.chkHabilitar.Location = New System.Drawing.Point(496, 61)
        Me.chkHabilitar.Name = "chkHabilitar"
        Me.chkHabilitar.Size = New System.Drawing.Size(15, 14)
        Me.chkHabilitar.TabIndex = 41
        Me.chkHabilitar.UseVisualStyleBackColor = True
        '
        'txtCod
        '
        Me.txtCod.Location = New System.Drawing.Point(62, 119)
        Me.txtCod.MaxLength = 8
        Me.txtCod.Name = "txtCod"
        Me.txtCod.Size = New System.Drawing.Size(78, 20)
        Me.txtCod.TabIndex = 40
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(21, 122)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(33, 13)
        Me.Label35.TabIndex = 39
        Me.Label35.Text = "COD:"
        '
        'txtVapor
        '
        Me.txtVapor.Location = New System.Drawing.Point(237, 120)
        Me.txtVapor.MaxLength = 15
        Me.txtVapor.Name = "txtVapor"
        Me.txtVapor.Size = New System.Drawing.Size(160, 20)
        Me.txtVapor.TabIndex = 38
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(152, 123)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(79, 13)
        Me.Label30.TabIndex = 37
        Me.Label30.Text = "Vapor/Destino:"
        '
        'txtTransporte
        '
        Me.txtTransporte.Location = New System.Drawing.Point(351, 93)
        Me.txtTransporte.MaxLength = 20
        Me.txtTransporte.Name = "txtTransporte"
        Me.txtTransporte.Size = New System.Drawing.Size(160, 20)
        Me.txtTransporte.TabIndex = 36
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(284, 97)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(61, 13)
        Me.Label34.TabIndex = 35
        Me.Label34.Text = "Transporte:"
        '
        'txtCantidadCajasE
        '
        Me.txtCantidadCajasE.Enabled = False
        Me.txtCantidadCajasE.Location = New System.Drawing.Point(440, 61)
        Me.txtCantidadCajasE.MaxLength = 4
        Me.txtCantidadCajasE.Name = "txtCantidadCajasE"
        Me.txtCantidadCajasE.Size = New System.Drawing.Size(54, 20)
        Me.txtCantidadCajasE.TabIndex = 34
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(379, 63)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(59, 13)
        Me.Label26.TabIndex = 33
        Me.Label26.Text = "CantidadE:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(16, 9)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(142, 13)
        Me.Label23.TabIndex = 32
        Me.Label23.Text = "Procesadas y enviadas:"
        '
        'cboCliente
        '
        Me.cboCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCliente.FormattingEnabled = True
        Me.cboCliente.Location = New System.Drawing.Point(62, 31)
        Me.cboCliente.Name = "cboCliente"
        Me.cboCliente.Size = New System.Drawing.Size(432, 21)
        Me.cboCliente.TabIndex = 31
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(16, 34)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(42, 13)
        Me.Label25.TabIndex = 30
        Me.Label25.Text = "Cliente:"
        '
        'dgvCajasPE
        '
        Me.dgvCajasPE.AllowUserToAddRows = False
        Me.dgvCajasPE.AllowUserToDeleteRows = False
        Me.dgvCajasPE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCajasPE.Location = New System.Drawing.Point(18, 146)
        Me.dgvCajasPE.Name = "dgvCajasPE"
        Me.dgvCajasPE.ReadOnly = True
        Me.dgvCajasPE.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCajasPE.Size = New System.Drawing.Size(619, 199)
        Me.dgvCajasPE.TabIndex = 27
        '
        'txtCantidadCajas
        '
        Me.txtCantidadCajas.Location = New System.Drawing.Point(319, 61)
        Me.txtCantidadCajas.MaxLength = 4
        Me.txtCantidadCajas.Name = "txtCantidadCajas"
        Me.txtCantidadCajas.Size = New System.Drawing.Size(54, 20)
        Me.txtCantidadCajas.TabIndex = 15
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(261, 64)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(59, 13)
        Me.Label24.TabIndex = 14
        Me.Label24.Text = "CantidadP:"
        '
        'cboMarca
        '
        Me.cboMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMarca.FormattingEnabled = True
        Me.cboMarca.Location = New System.Drawing.Point(62, 60)
        Me.cboMarca.Name = "cboMarca"
        Me.cboMarca.Size = New System.Drawing.Size(192, 21)
        Me.cboMarca.TabIndex = 8
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(16, 64)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(40, 13)
        Me.Label22.TabIndex = 7
        Me.Label22.Text = "Marca:"
        '
        'btnNuevoCaja
        '
        Me.btnNuevoCaja.Image = CType(resources.GetObject("btnNuevoCaja.Image"), System.Drawing.Image)
        Me.btnNuevoCaja.Location = New System.Drawing.Point(521, 11)
        Me.btnNuevoCaja.Name = "btnNuevoCaja"
        Me.btnNuevoCaja.Size = New System.Drawing.Size(55, 41)
        Me.btnNuevoCaja.TabIndex = 42
        Me.btnNuevoCaja.UseVisualStyleBackColor = True
        '
        'btnEliminaCaja
        '
        Me.btnEliminaCaja.Image = CType(resources.GetObject("btnEliminaCaja.Image"), System.Drawing.Image)
        Me.btnEliminaCaja.Location = New System.Drawing.Point(582, 56)
        Me.btnEliminaCaja.Name = "btnEliminaCaja"
        Me.btnEliminaCaja.Size = New System.Drawing.Size(55, 42)
        Me.btnEliminaCaja.TabIndex = 29
        Me.btnEliminaCaja.UseVisualStyleBackColor = True
        '
        'btnGuardarCajas
        '
        Me.btnGuardarCajas.Image = CType(resources.GetObject("btnGuardarCajas.Image"), System.Drawing.Image)
        Me.btnGuardarCajas.Location = New System.Drawing.Point(582, 11)
        Me.btnGuardarCajas.Name = "btnGuardarCajas"
        Me.btnGuardarCajas.Size = New System.Drawing.Size(55, 41)
        Me.btnGuardarCajas.TabIndex = 28
        Me.btnGuardarCajas.UseVisualStyleBackColor = True
        '
        'saco
        '
        Me.saco.Controls.Add(Me.dgvRechazadoSacos)
        Me.saco.Controls.Add(Me.txtSacos)
        Me.saco.Controls.Add(Me.Label21)
        Me.saco.Controls.Add(Me.txtPersonaSacos)
        Me.saco.Controls.Add(Me.Label20)
        Me.saco.Controls.Add(Me.cboManosSacos)
        Me.saco.Controls.Add(Me.Label19)
        Me.saco.Controls.Add(Me.Label18)
        Me.saco.Controls.Add(Me.btnEliminaSacos)
        Me.saco.Controls.Add(Me.btnGuardarSacos)
        Me.saco.Location = New System.Drawing.Point(4, 22)
        Me.saco.Name = "saco"
        Me.saco.Size = New System.Drawing.Size(643, 383)
        Me.saco.TabIndex = 3
        Me.saco.Text = "Sacos"
        Me.saco.UseVisualStyleBackColor = True
        '
        'dgvRechazadoSacos
        '
        Me.dgvRechazadoSacos.AllowUserToAddRows = False
        Me.dgvRechazadoSacos.AllowUserToDeleteRows = False
        Me.dgvRechazadoSacos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvRechazadoSacos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRechazadoSacos.Location = New System.Drawing.Point(17, 80)
        Me.dgvRechazadoSacos.Name = "dgvRechazadoSacos"
        Me.dgvRechazadoSacos.ReadOnly = True
        Me.dgvRechazadoSacos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRechazadoSacos.Size = New System.Drawing.Size(533, 298)
        Me.dgvRechazadoSacos.TabIndex = 26
        '
        'txtSacos
        '
        Me.txtSacos.Location = New System.Drawing.Point(500, 42)
        Me.txtSacos.MaxLength = 6
        Me.txtSacos.Name = "txtSacos"
        Me.txtSacos.Size = New System.Drawing.Size(50, 20)
        Me.txtSacos.TabIndex = 13
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(452, 45)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(40, 13)
        Me.Label21.TabIndex = 12
        Me.Label21.Text = "Sacos:"
        '
        'txtPersonaSacos
        '
        Me.txtPersonaSacos.Location = New System.Drawing.Point(230, 43)
        Me.txtPersonaSacos.MaxLength = 255
        Me.txtPersonaSacos.Name = "txtPersonaSacos"
        Me.txtPersonaSacos.Size = New System.Drawing.Size(215, 20)
        Me.txtPersonaSacos.TabIndex = 11
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(182, 46)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(49, 13)
        Me.Label20.TabIndex = 10
        Me.Label20.Text = "Persona:"
        '
        'cboManosSacos
        '
        Me.cboManosSacos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboManosSacos.FormattingEnabled = True
        Me.cboManosSacos.Items.AddRange(New Object() {"PEQUEÑO", "MEDIANO", "GRANDE"})
        Me.cboManosSacos.Location = New System.Drawing.Point(62, 42)
        Me.cboManosSacos.Name = "cboManosSacos"
        Me.cboManosSacos.Size = New System.Drawing.Size(114, 21)
        Me.cboManosSacos.TabIndex = 6
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(14, 45)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(42, 13)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "Manos:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(14, 15)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(75, 13)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Lo rechazado:"
        '
        'btnEliminaSacos
        '
        Me.btnEliminaSacos.Image = CType(resources.GetObject("btnEliminaSacos.Image"), System.Drawing.Image)
        Me.btnEliminaSacos.Location = New System.Drawing.Point(582, 56)
        Me.btnEliminaSacos.Name = "btnEliminaSacos"
        Me.btnEliminaSacos.Size = New System.Drawing.Size(55, 42)
        Me.btnEliminaSacos.TabIndex = 25
        Me.btnEliminaSacos.UseVisualStyleBackColor = True
        '
        'btnGuardarSacos
        '
        Me.btnGuardarSacos.Image = CType(resources.GetObject("btnGuardarSacos.Image"), System.Drawing.Image)
        Me.btnGuardarSacos.Location = New System.Drawing.Point(582, 9)
        Me.btnGuardarSacos.Name = "btnGuardarSacos"
        Me.btnGuardarSacos.Size = New System.Drawing.Size(55, 41)
        Me.btnGuardarSacos.TabIndex = 24
        Me.btnGuardarSacos.UseVisualStyleBackColor = True
        '
        'labores
        '
        Me.labores.Controls.Add(Me.btnEliminaLabores)
        Me.labores.Controls.Add(Me.btnGuardarLabores)
        Me.labores.Controls.Add(Me.dgvLabores)
        Me.labores.Controls.Add(Me.txtPesoLabores)
        Me.labores.Controls.Add(Me.Label27)
        Me.labores.Controls.Add(Me.txtPersonaLabores)
        Me.labores.Controls.Add(Me.Label28)
        Me.labores.Controls.Add(Me.cboLabores)
        Me.labores.Controls.Add(Me.Label29)
        Me.labores.Location = New System.Drawing.Point(4, 22)
        Me.labores.Name = "labores"
        Me.labores.Size = New System.Drawing.Size(643, 383)
        Me.labores.TabIndex = 5
        Me.labores.Text = "Labores"
        Me.labores.UseVisualStyleBackColor = True
        '
        'btnEliminaLabores
        '
        Me.btnEliminaLabores.Image = CType(resources.GetObject("btnEliminaLabores.Image"), System.Drawing.Image)
        Me.btnEliminaLabores.Location = New System.Drawing.Point(580, 56)
        Me.btnEliminaLabores.Name = "btnEliminaLabores"
        Me.btnEliminaLabores.Size = New System.Drawing.Size(55, 42)
        Me.btnEliminaLabores.TabIndex = 40
        Me.btnEliminaLabores.UseVisualStyleBackColor = True
        '
        'btnGuardarLabores
        '
        Me.btnGuardarLabores.Image = CType(resources.GetObject("btnGuardarLabores.Image"), System.Drawing.Image)
        Me.btnGuardarLabores.Location = New System.Drawing.Point(580, 9)
        Me.btnGuardarLabores.Name = "btnGuardarLabores"
        Me.btnGuardarLabores.Size = New System.Drawing.Size(55, 41)
        Me.btnGuardarLabores.TabIndex = 39
        Me.btnGuardarLabores.UseVisualStyleBackColor = True
        '
        'dgvLabores
        '
        Me.dgvLabores.AllowUserToAddRows = False
        Me.dgvLabores.AllowUserToDeleteRows = False
        Me.dgvLabores.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvLabores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLabores.Location = New System.Drawing.Point(14, 72)
        Me.dgvLabores.Name = "dgvLabores"
        Me.dgvLabores.ReadOnly = True
        Me.dgvLabores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLabores.Size = New System.Drawing.Size(430, 306)
        Me.dgvLabores.TabIndex = 38
        '
        'txtPesoLabores
        '
        Me.txtPesoLabores.Location = New System.Drawing.Point(394, 44)
        Me.txtPesoLabores.MaxLength = 6
        Me.txtPesoLabores.Name = "txtPesoLabores"
        Me.txtPesoLabores.Size = New System.Drawing.Size(50, 20)
        Me.txtPesoLabores.TabIndex = 37
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(348, 49)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(40, 13)
        Me.Label27.TabIndex = 36
        Me.Label27.Text = "Sacos:"
        '
        'txtPersonaLabores
        '
        Me.txtPersonaLabores.Location = New System.Drawing.Point(69, 44)
        Me.txtPersonaLabores.MaxLength = 255
        Me.txtPersonaLabores.Name = "txtPersonaLabores"
        Me.txtPersonaLabores.Size = New System.Drawing.Size(273, 20)
        Me.txtPersonaLabores.TabIndex = 35
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(15, 47)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(47, 13)
        Me.Label28.TabIndex = 34
        Me.Label28.Text = "Nombre:"
        '
        'cboLabores
        '
        Me.cboLabores.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLabores.FormattingEnabled = True
        Me.cboLabores.Location = New System.Drawing.Point(69, 17)
        Me.cboLabores.Name = "cboLabores"
        Me.cboLabores.Size = New System.Drawing.Size(193, 21)
        Me.cboLabores.TabIndex = 33
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(15, 20)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(48, 13)
        Me.Label29.TabIndex = 32
        Me.Label29.Text = "Labores:"
        '
        'empleados
        '
        Me.empleados.Controls.Add(Me.dgvEvolution)
        Me.empleados.Controls.Add(Me.btnConsultarEvolution)
        Me.empleados.Location = New System.Drawing.Point(4, 22)
        Me.empleados.Name = "empleados"
        Me.empleados.Size = New System.Drawing.Size(643, 383)
        Me.empleados.TabIndex = 7
        Me.empleados.Text = "Colaboradores"
        Me.empleados.UseVisualStyleBackColor = True
        '
        'dgvEvolution
        '
        Me.dgvEvolution.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEvolution.Location = New System.Drawing.Point(23, 50)
        Me.dgvEvolution.Name = "dgvEvolution"
        Me.dgvEvolution.Size = New System.Drawing.Size(477, 328)
        Me.dgvEvolution.TabIndex = 43
        '
        'btnConsultarEvolution
        '
        Me.btnConsultarEvolution.Location = New System.Drawing.Point(23, 18)
        Me.btnConsultarEvolution.Name = "btnConsultarEvolution"
        Me.btnConsultarEvolution.Size = New System.Drawing.Size(126, 26)
        Me.btnConsultarEvolution.TabIndex = 42
        Me.btnConsultarEvolution.Text = "Consultar a evolution"
        Me.btnConsultarEvolution.UseVisualStyleBackColor = True
        '
        'funda
        '
        Me.funda.Controls.Add(Me.txtObservaciones)
        Me.funda.Controls.Add(Me.txtRecuperado)
        Me.funda.Controls.Add(Me.txtFundaRecuperadas)
        Me.funda.Controls.Add(Me.txtCantidadRecobrada)
        Me.funda.Controls.Add(Me.Label9)
        Me.funda.Controls.Add(Me.Label8)
        Me.funda.Controls.Add(Me.Label7)
        Me.funda.Controls.Add(Me.btnGuardarFundas)
        Me.funda.Location = New System.Drawing.Point(4, 22)
        Me.funda.Name = "funda"
        Me.funda.Padding = New System.Windows.Forms.Padding(3)
        Me.funda.Size = New System.Drawing.Size(643, 383)
        Me.funda.TabIndex = 1
        Me.funda.Text = "--"
        Me.funda.UseVisualStyleBackColor = True
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Location = New System.Drawing.Point(19, 104)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(486, 64)
        Me.txtObservaciones.TabIndex = 11
        Me.txtObservaciones.Visible = False
        '
        'txtRecuperado
        '
        Me.txtRecuperado.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtRecuperado.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRecuperado.Location = New System.Drawing.Point(216, 21)
        Me.txtRecuperado.Name = "txtRecuperado"
        Me.txtRecuperado.Size = New System.Drawing.Size(99, 40)
        Me.txtRecuperado.TabIndex = 5
        Me.txtRecuperado.Text = "0"
        Me.txtRecuperado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtRecuperado.Visible = False
        '
        'txtFundaRecuperadas
        '
        Me.txtFundaRecuperadas.Location = New System.Drawing.Point(125, 53)
        Me.txtFundaRecuperadas.MaxLength = 6
        Me.txtFundaRecuperadas.Name = "txtFundaRecuperadas"
        Me.txtFundaRecuperadas.Size = New System.Drawing.Size(68, 20)
        Me.txtFundaRecuperadas.TabIndex = 4
        Me.txtFundaRecuperadas.Visible = False
        '
        'txtCantidadRecobrada
        '
        Me.txtCantidadRecobrada.Enabled = False
        Me.txtCantidadRecobrada.Location = New System.Drawing.Point(125, 18)
        Me.txtCantidadRecobrada.MaxLength = 6
        Me.txtCantidadRecobrada.Name = "txtCantidadRecobrada"
        Me.txtCantidadRecobrada.Size = New System.Drawing.Size(68, 20)
        Me.txtCantidadRecobrada.TabIndex = 3
        Me.txtCantidadRecobrada.Text = "0"
        Me.txtCantidadRecobrada.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(229, 64)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 13)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "% Recuperación"
        Me.Label9.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(16, 56)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(107, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Fundas recuperadas:"
        Me.Label8.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(16, 23)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(103, 13)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Cantidad recobrada:"
        Me.Label7.Visible = False
        '
        'btnGuardarFundas
        '
        Me.btnGuardarFundas.Image = CType(resources.GetObject("btnGuardarFundas.Image"), System.Drawing.Image)
        Me.btnGuardarFundas.Location = New System.Drawing.Point(582, 9)
        Me.btnGuardarFundas.Name = "btnGuardarFundas"
        Me.btnGuardarFundas.Size = New System.Drawing.Size(55, 41)
        Me.btnGuardarFundas.TabIndex = 10
        Me.btnGuardarFundas.UseVisualStyleBackColor = True
        Me.btnGuardarFundas.Visible = False
        '
        'cMenu
        '
        Me.cMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SaldosEnfundeToolStripMenuItem})
        Me.cMenu.Name = "cMenu"
        Me.cMenu.Size = New System.Drawing.Size(156, 26)
        '
        'SaldosEnfundeToolStripMenuItem
        '
        Me.SaldosEnfundeToolStripMenuItem.Name = "SaldosEnfundeToolStripMenuItem"
        Me.SaldosEnfundeToolStripMenuItem.Size = New System.Drawing.Size(155, 22)
        Me.SaldosEnfundeToolStripMenuItem.Text = "Saldos enfunde"
        '
        'frmR11Items
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(675, 505)
        Me.Controls.Add(Me.tcGeneral)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblR11)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmR11Items"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BANPROD-R11"
        Me.tcGeneral.ResumeLayout(False)
        Me.recobre.ResumeLayout(False)
        Me.recobre.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvRecobre, System.ComponentModel.ISupportInitialize).EndInit()
        Me.rechazado.ResumeLayout(False)
        Me.rechazado.PerformLayout()
        CType(Me.dgvRacimosRechazados, System.ComponentModel.ISupportInitialize).EndInit()
        Me.otro.ResumeLayout(False)
        Me.otro.PerformLayout()
        CType(Me.dgvRLotes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.caja.ResumeLayout(False)
        Me.caja.PerformLayout()
        CType(Me.dgvCajasPE, System.ComponentModel.ISupportInitialize).EndInit()
        Me.saco.ResumeLayout(False)
        Me.saco.PerformLayout()
        CType(Me.dgvRechazadoSacos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.labores.ResumeLayout(False)
        Me.labores.PerformLayout()
        CType(Me.dgvLabores, System.ComponentModel.ISupportInitialize).EndInit()
        Me.empleados.ResumeLayout(False)
        CType(Me.dgvEvolution, System.ComponentModel.ISupportInitialize).EndInit()
        Me.funda.ResumeLayout(False)
        Me.funda.PerformLayout()
        Me.cMenu.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblR11 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tcGeneral As System.Windows.Forms.TabControl
    Friend WithEvents recobre As System.Windows.Forms.TabPage
    Friend WithEvents funda As System.Windows.Forms.TabPage
    Friend WithEvents otro As System.Windows.Forms.TabPage
    Friend WithEvents saco As System.Windows.Forms.TabPage
    Friend WithEvents caja As System.Windows.Forms.TabPage
    Friend WithEvents labores As System.Windows.Forms.TabPage
    Friend WithEvents rechazado As System.Windows.Forms.TabPage
    Friend WithEvents empleados As System.Windows.Forms.TabPage
    Friend WithEvents txtRechazadoR As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidadR As System.Windows.Forms.TextBox
    Friend WithEvents cboLoteR As System.Windows.Forms.ComboBox
    Friend WithEvents CboSemanaR As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvRecobre As System.Windows.Forms.DataGridView
    Friend WithEvents btnEliminarRecobre As System.Windows.Forms.Button
    Friend WithEvents btnGuardarRecobre As System.Windows.Forms.Button
    Friend WithEvents txtRecuperado As System.Windows.Forms.TextBox
    Friend WithEvents txtFundaRecuperadas As System.Windows.Forms.TextBox
    Friend WithEvents txtCantidadRecobrada As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnGuardarFundas As System.Windows.Forms.Button
    Friend WithEvents txtLargoDedo As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtCalibracion As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtPeso As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtRecorrido As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboLoteOtros As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dgvRLotes As System.Windows.Forms.DataGridView
    Friend WithEvents btnEliminaRegistroLote As System.Windows.Forms.Button
    Friend WithEvents btnGuardarOtros As System.Windows.Forms.Button
    Friend WithEvents txtMellizos As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtNumManos As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cboManosSacos As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents dgvRechazadoSacos As System.Windows.Forms.DataGridView
    Friend WithEvents btnEliminaSacos As System.Windows.Forms.Button
    Friend WithEvents btnGuardarSacos As System.Windows.Forms.Button
    Friend WithEvents txtSacos As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtPersonaSacos As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cboCliente As System.Windows.Forms.ComboBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents btnEliminaCaja As System.Windows.Forms.Button
    Friend WithEvents btnGuardarCajas As System.Windows.Forms.Button
    Friend WithEvents dgvCajasPE As System.Windows.Forms.DataGridView
    Friend WithEvents txtCantidadCajas As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cboMarca As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents btnEliminaLabores As System.Windows.Forms.Button
    Friend WithEvents btnGuardarLabores As System.Windows.Forms.Button
    Friend WithEvents dgvLabores As System.Windows.Forms.DataGridView
    Friend WithEvents txtPesoLabores As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtPersonaLabores As System.Windows.Forms.TextBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cboLabores As System.Windows.Forms.ComboBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents btnEliminaRechazado As System.Windows.Forms.Button
    Friend WithEvents btnGuardarRechazado As System.Windows.Forms.Button
    Friend WithEvents dgvRacimosRechazados As System.Windows.Forms.DataGridView
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadRechazado As System.Windows.Forms.TextBox
    Friend WithEvents cboDefectoRechazado As System.Windows.Forms.ComboBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents cboLoteRechazado As System.Windows.Forms.ComboBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents btnConsultarEvolution As System.Windows.Forms.Button
    Friend WithEvents dgvEvolution As System.Windows.Forms.DataGridView
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents txtVapor As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtTransporte As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents txtCantidadCajasE As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtCod As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents chkHabilitar As System.Windows.Forms.CheckBox
    Friend WithEvents txtColorSemanaF As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cboAnioR As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboSemanaE As System.Windows.Forms.ComboBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents cboAnioE As System.Windows.Forms.ComboBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents cboEdadR As System.Windows.Forms.ComboBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents cboColorR As System.Windows.Forms.ComboBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents btnNuevoCaja As System.Windows.Forms.Button
    Friend WithEvents btnNuevoLotes As System.Windows.Forms.Button
    Friend WithEvents lblPRecobrado As System.Windows.Forms.Label
    Friend WithEvents lblRecobrado As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents lblProcesados As System.Windows.Forms.Label
    Friend WithEvents lblEnviados As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtChapeado As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents btnRatiosLotes As System.Windows.Forms.Button
    Friend WithEvents cMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SaldosEnfundeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents cboTipoTransporte As System.Windows.Forms.ComboBox
    Friend WithEvents txtSaldoCajas As System.Windows.Forms.TextBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents lblProcesado As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
End Class
