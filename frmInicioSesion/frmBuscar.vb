﻿Imports ConexionBase
Public Class frmBuscar
    Public sql As String
    Public datos As New ConexionBase.conexionBase
    Public dtDatos As New DataTable
    Private Sub frmBuscar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dtDatos = datos.ejecutarMySql(sql)

        If dtDatos.Rows.Count > 0 Then
            dgvVista.DataSource = dtDatos
            dgvVista.Columns("id").Visible = False
        Else
            MessageBox.Show("No hay datos que mostrar", "BANDPROD", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            dgvVista.DataSource = Nothing
        End If

    End Sub

    Private Sub txtDescripcion_TextChanged(sender As Object, e As EventArgs) Handles txtDescripcion.TextChanged
        txtDescripcion.CharacterCasing = CharacterCasing.Upper
    End Sub

    Private Sub dgvVista_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvVista.CellContentClick
        codigoBuscado = Convert.ToString(dgvVista.CurrentRow.Cells("id").Value)
        Me.Close()
    End Sub
End Class