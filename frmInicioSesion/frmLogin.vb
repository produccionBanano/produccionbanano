﻿Imports ConexionBase
Public Class frmLogin
    Dim dato As New ConexionBase.conexionBase
    Dim sql As String = ""
    Dim dtDatos As New DataTable
    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Me.Close()
        frmMenu.usuario = " "
    End Sub

    Private Sub txtUsuario_TextChanged(sender As Object, e As EventArgs) Handles txtUsuario.TextChanged
        txtUsuario.CharacterCasing = CharacterCasing.Upper
    End Sub

    Private Sub btnAcceder_Click(sender As Object, e As EventArgs) Handles btnAcceder.Click
        Dim usuario As String
        Dim clave As String
        Try
            usuario = txtUsuario.Text
            clave = txtClave.Text

            sql = "SELECT * FROM tblUsuario WHERE usuario='" & txtUsuario.Text & "' AND clave=MD5('" & txtClave.Text & "')"

            dtDatos = dato.ejecutarMySql(sql)

            If dtDatos.Rows.Count > 0 Then
                'MessageBox.Show("Conexión exitosa", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Me.Close()
                fincaUsuario = dtDatos.Rows(0).Item("hacienda")
                frmMenu.usuario = txtUsuario.Text
                frmMenu.Show()

            Else
                MessageBox.Show("Sus credenciales son incorrectas, por favor verifique", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Hand)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        frmUsuarioNuevo.ShowDialog()
    End Sub

    Private Sub txtClave_KeyDown(sender As Object, e As KeyEventArgs) Handles txtClave.KeyDown
        Select Case e.KeyData
            Case Keys.Enter
                Call btnAcceder_Click(sender, e)
        End Select
    End Sub

   
End Class
