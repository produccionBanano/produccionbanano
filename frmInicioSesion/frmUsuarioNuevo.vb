﻿Imports ConexionBase
Public Class frmUsuarioNuevo

    Public datos As New ConexionBase.conexionBase
    Public sql As String = ""
    Public codigoEditar As Integer
    Private Sub frmUsuarioNuevo_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarCombo()
    End Sub

    Sub cargarCombo()
        Dim dtDatos As New DataTable
        sql = "SELECT * FROM tblFinca"
        dtDatos = datos.ejecutarMySql(sql)
        cboHacienda.DataSource = dtDatos
        cboHacienda.ValueMember = "id"
        cboHacienda.DisplayMember = "descripcion"
    End Sub
    Sub limpiarCampos()
        txtNombre.Clear()
        txtCorreo.Clear()
        txtClave.Clear()
        txtUsuario.Clear()
        cargarCombo()
    End Sub

    Private Sub txtNombre_TextChanged(sender As Object, e As EventArgs) Handles txtNombre.TextChanged, txtUsuario.TextChanged
        sender.CharacterCasing = CharacterCasing.Upper
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim dtDatos As New DataTable
        Try

            If codigoEditar = 0 Then
                sql = ""
                sql = "INSERT INTO tblUsuario SET "
                sql &= " nombre='" & txtNombre.Text & "'"
                sql &= " ,usuario='" & txtUsuario.Text & "'"
                sql &= " ,clave=MD5('" & txtClave.Text & "')"
                sql &= " ,correo='" & txtCorreo.Text & "'"
                sql &= " ,hacienda=" & cboHacienda.GetItemText(cboHacienda.SelectedValue)

                dtDatos = datos.ejecutarMySql(sql)

                MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Else
                sql = ""
                sql = "UPDATE tblUsuario SET "
                sql &= " nombre='" & txtNombre.Text & "'"
                sql &= " ,usuario='" & txtUsuario.Text & "'"
                If chbCambio.Checked = True Then
                    sql &= " ,clave=MD5('" & txtClave.Text & "')"
                End If
                sql &= " ,correo='" & txtCorreo.Text & "'"
                sql &= " ,hacienda=" & cboHacienda.GetItemText(cboHacienda.SelectedValue)
                sql &= " WHERE id=" & codigoEditar

                dtDatos = datos.ejecutarMySql(sql)

                MessageBox.Show("Datos actualizados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)

                txtClave.Enabled = False
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim dtDatos As New DataTable
        sql = ""

        sql = "SELECT us.id,us.nombre,us.usuario,fi.descripcion finca FROM tblUsuario us"
        sql &= " LEFT JOIN tblFinca fi ON fi.id=us.hacienda"
        sql &= " ORDER BY us.nombre"
        frmBuscar.sql = sql
        frmBuscar.ShowDialog()

        sql = "SELECT * FROM tblUsuario WHERE id=" & codigoBuscado
        dtDatos = datos.ejecutarMySql(sql)

        If dtDatos.Rows.Count > 0 Then
            codigoEditar = dtDatos.Rows(0).Item("id")
            txtNombre.Text = dtDatos.Rows(0).Item("nombre")
            txtUsuario.Text = dtDatos.Rows(0).Item("usuario")
            txtClave.Text = dtDatos.Rows(0).Item("nombre")
            txtCorreo.Text = dtDatos.Rows(0).Item("correo")
            cboHacienda.SelectedValue = dtDatos.Rows(0).Item("hacienda")

            codigoBuscado = 0
        Else
            codigoBuscado = 0
            codigoEditar = 0
            Return
        End If
    End Sub

    Private Sub chbCambio_CheckedChanged(sender As Object, e As EventArgs) Handles chbCambio.CheckedChanged
        If chbCambio.Checked = True Then
            txtClave.Clear()
            txtClave.Enabled = True
        Else
            txtClave.Enabled = False
            Return
        End If

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        limpiarCampos()
        codigoBuscado = 0
        codigoEditar = 0
        txtClave.Enabled = True
        chbCambio.Checked = True
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        MessageBox.Show("Los usuario no pueden ser eliminados", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub
End Class