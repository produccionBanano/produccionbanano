﻿Imports ConexionBase
Public Class frmInfoColores
    Dim datos As New ConexionBase.conexionBase
    Public sql As String = ""
    Public sql2 As String = ""
    Private Sub frmInfoColores_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim recobroLotes As New DataTable
        Dim recobroColor As New DataTable

        recobroLotes = datos.ejecutarMySql(sql)
        recobroColor = datos.ejecutarMySql(sql2)

        dgvRecobroLoteColor.DataSource = recobroLotes
        dgvRecobroColor.DataSource = recobroColor

        detectaNegativo()

    End Sub
    Sub detectaNegativo()
        For i = 0 To dgvRecobroLoteColor.Rows.Count - 1
            If dgvRecobroLoteColor.Item("diferenciaEnfundeRecobro", i).Value < 0 Then
                dgvRecobroLoteColor.Rows(i).DefaultCellStyle.BackColor = Color.Red
            End If
        Next
    End Sub

End Class