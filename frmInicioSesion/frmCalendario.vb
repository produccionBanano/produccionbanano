﻿Imports ConexionBase
Public Class frmCalendario
    Public datos As New ConexionBase.conexionBase
    Public idCalendario As Integer
    Public sql As String
    'Public dtDatos As New DataTable

    Private Sub frmCalendario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        carga()
    End Sub
    Sub carga()
        Dim dtDatosColor As New DataTable

        idCalendario = 0
        txtNSemana.Text = 0
        dtpInicia.Value = Now
        dtpTermina.Value = Now

        sql = ""
        sql = "SELECT * FROM tblColor WHERE estado='ACTIVO'"
        dtDatosColor = datos.ejecutarMySql(sql)
        cboColor.DataSource = dtDatosColor
        cboColor.ValueMember = "id"
        cboColor.DisplayMember = "observaciones"

    End Sub

    Private Sub cboColor_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboColor.SelectedValueChanged
        Try
            Dim dtDatos As New DataTable
            sql = ""
            sql = "SELECT codigo FROM tblColor WHERE id='" & cboColor.GetItemText(cboColor.SelectedValue) & "'"

            dtDatos = datos.ejecutarMySql(sql)

            If dtDatos.Rows.Count > 0 Then
                txtColor.BackColor = Color.FromArgb(dtDatos.Rows(0).Item("codigo"))
            Else
                txtColor.BackColor = Color.FromArgb(-1)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "BANPROD")
        End Try
        

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        carga()
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim dtDatos As New DataTable

        carga()
        frmBuscar.sql = "SELECT ca.id,CONCAT(ca.periodo,'/',ca.semana,'/',ca.fdesde,'/',ca.fhasta) descripcion,c.observaciones semanaColor,ca.estado FROM tblCalendario ca"
        frmBuscar.sql &= " LEFT JOIN tblColor c ON c.id=ca.idColor"
        frmBuscar.ShowDialog()

        idCalendario = codigoBuscado
        sql = ""
        sql = "SELECT * FROM tblCalendario WHERE id=" & codigoBuscado
        dtDatos = datos.ejecutarMySql(sql)

        If dtDatos.Rows.Count > 0 Then
            txtNSemana.Text = dtDatos.Rows(0).Item("semana").ToString
            dtpInicia.Value = dtDatos.Rows(0).Item("fdesde").ToString
            dtpTermina.Value = dtDatos.Rows(0).Item("fhasta").ToString
            cboColor.SelectedValue = dtDatos.Rows(0).Item("idColor").ToString
            nudPeriodo.Value = dtDatos.Rows(0).Item("periodo").ToString
            lblEstado.Text = dtDatos.Rows(0).Item("estado").ToString
        End If
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim dtDatos As New DataTable


        If CInt(txtNSemana.Text) > 0 Then
            If idCalendario <= 0 Then
                'Inserta
                sql = "SELECT * FROM tblCalendario WHERE semana='" & txtNSemana.Text & "' AND fdesde='" & dtpInicia.Value & "'"
                dtDatos = datos.ejecutarMySql(sql)

                If dtDatos.Rows.Count > 0 Then
                    MessageBox.Show("Por favor verifique, el calendaro ya lo ha ingresado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Else
                    sql = "INSERT INTO tblCalendario SET "
                    sql &= " idColor=" & cboColor.GetItemText(cboColor.SelectedValue)
                    sql &= " ,semana=" & txtNSemana.Text
                    sql &= " ,fdesde='" & Format(dtpInicia.Value, "yyyy-MM-dd") & "'"
                    sql &= " ,fhasta='" & Format(dtpTermina.Value, "yyyy-MM-dd") & "'"
                    sql &= " ,mes=" & Month(dtpInicia.Value)
                    sql &= " ,anio=" & Year(dtpInicia.Value)
                    sql &= " ,periodo=" & nudPeriodo.Value
                    sql &= " ,estado='ACTIVO'"

                    datos.ejecutarMySql(sql)
                    idCalendario = datos.lastInsertId
                    If idCalendario > 0 Then
                        MessageBox.Show("Información ingresada con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        carga()
                    End If
                End If
            Else
                'Actualiza

                sql = "UPDATE tblCalendario SET "
                sql &= " idColor=" & cboColor.GetItemText(cboColor.SelectedValue)
                sql &= " ,semana=" & txtNSemana.Text
                sql &= " ,fdesde='" & Format(dtpInicia.Value, "yyyy-MM-dd") & "'"
                sql &= " ,fhasta='" & Format(dtpTermina.Value, "yyyy-MM-dd") & "'"
                sql &= " ,mes=" & Month(dtpInicia.Value)
                sql &= " ,anio=" & Year(dtpInicia.Value)
                sql &= " ,periodo=" & nudPeriodo.Value
                sql &= " WHERE id=" & idCalendario

                datos.ejecutarMySql(sql)
                MessageBox.Show("Su registro se encuentra actualizado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Else
            MessageBox.Show("Para guardar datos debe llenar los registros correctamente", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        

    End Sub
    Function convertirFecha(fecha As Date) As String
        Dim fechaConvertida As String = ""

        Dim dd As String = ""
        Dim mm As String = ""
        Dim aaaa As String = ""

        dd = (fecha)


        Return fechaConvertida
    End Function

    Private Sub txtNSemana_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtNSemana.KeyPress
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtNSemana_TextChanged(sender As Object, e As EventArgs) Handles txtNSemana.TextChanged

    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If CInt(txtNSemana.Text) > 0 Then
            sql = ""
            sql = "UPDATE tblCalendario SET "
            sql &= " estado='ANULADO' "
            sql &= " WHERE id=" & idCalendario

            datos.ejecutarMySql(sql)
            MessageBox.Show("El registro ha sido anulado", "BANDPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            carga()
        Else
            MessageBox.Show("Para anular debe buscar un registro", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If
    End Sub
End Class