﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReporteInformeEmbarque
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboHacienda = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboSemana = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboAnio = New System.Windows.Forms.ComboBox()
        Me.btnInforme = New System.Windows.Forms.Button()
        Me.dgvProceso = New System.Windows.Forms.DataGridView()
        Me.dgvCajasProcesadas = New System.Windows.Forms.DataGridView()
        Me.dgvCajasEnviadas = New System.Windows.Forms.DataGridView()
        Me.dgvPromedios = New System.Windows.Forms.DataGridView()
        CType(Me.dgvProceso, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCajasProcesadas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCajasEnviadas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPromedios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "Finca:"
        '
        'cboHacienda
        '
        Me.cboHacienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHacienda.FormattingEnabled = True
        Me.cboHacienda.Location = New System.Drawing.Point(72, 12)
        Me.cboHacienda.Name = "cboHacienda"
        Me.cboHacienda.Size = New System.Drawing.Size(225, 21)
        Me.cboHacienda.TabIndex = 27
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(17, 69)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 13)
        Me.Label10.TabIndex = 46
        Me.Label10.Text = "Semana:"
        '
        'cboSemana
        '
        Me.cboSemana.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSemana.FormattingEnabled = True
        Me.cboSemana.Location = New System.Drawing.Point(72, 66)
        Me.cboSemana.Name = "cboSemana"
        Me.cboSemana.Size = New System.Drawing.Size(55, 21)
        Me.cboSemana.TabIndex = 45
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(17, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 48
        Me.Label2.Text = "Anio:"
        '
        'cboAnio
        '
        Me.cboAnio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnio.FormattingEnabled = True
        Me.cboAnio.Location = New System.Drawing.Point(72, 39)
        Me.cboAnio.Name = "cboAnio"
        Me.cboAnio.Size = New System.Drawing.Size(55, 21)
        Me.cboAnio.TabIndex = 47
        '
        'btnInforme
        '
        Me.btnInforme.Location = New System.Drawing.Point(175, 66)
        Me.btnInforme.Name = "btnInforme"
        Me.btnInforme.Size = New System.Drawing.Size(114, 23)
        Me.btnInforme.TabIndex = 49
        Me.btnInforme.Text = "Generar reporte"
        Me.btnInforme.UseVisualStyleBackColor = True
        '
        'dgvProceso
        '
        Me.dgvProceso.AllowUserToAddRows = False
        Me.dgvProceso.AllowUserToDeleteRows = False
        Me.dgvProceso.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvProceso.Location = New System.Drawing.Point(12, 127)
        Me.dgvProceso.Name = "dgvProceso"
        Me.dgvProceso.ReadOnly = True
        Me.dgvProceso.Size = New System.Drawing.Size(240, 38)
        Me.dgvProceso.TabIndex = 50
        '
        'dgvCajasProcesadas
        '
        Me.dgvCajasProcesadas.AllowUserToAddRows = False
        Me.dgvCajasProcesadas.AllowUserToDeleteRows = False
        Me.dgvCajasProcesadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCajasProcesadas.Location = New System.Drawing.Point(12, 171)
        Me.dgvCajasProcesadas.Name = "dgvCajasProcesadas"
        Me.dgvCajasProcesadas.ReadOnly = True
        Me.dgvCajasProcesadas.Size = New System.Drawing.Size(240, 38)
        Me.dgvCajasProcesadas.TabIndex = 51
        '
        'dgvCajasEnviadas
        '
        Me.dgvCajasEnviadas.AllowUserToAddRows = False
        Me.dgvCajasEnviadas.AllowUserToDeleteRows = False
        Me.dgvCajasEnviadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCajasEnviadas.Location = New System.Drawing.Point(12, 215)
        Me.dgvCajasEnviadas.Name = "dgvCajasEnviadas"
        Me.dgvCajasEnviadas.ReadOnly = True
        Me.dgvCajasEnviadas.Size = New System.Drawing.Size(240, 38)
        Me.dgvCajasEnviadas.TabIndex = 52
        '
        'dgvPromedios
        '
        Me.dgvPromedios.AllowUserToAddRows = False
        Me.dgvPromedios.AllowUserToDeleteRows = False
        Me.dgvPromedios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPromedios.Location = New System.Drawing.Point(12, 259)
        Me.dgvPromedios.Name = "dgvPromedios"
        Me.dgvPromedios.ReadOnly = True
        Me.dgvPromedios.Size = New System.Drawing.Size(240, 38)
        Me.dgvPromedios.TabIndex = 53
        '
        'frmReporteInformeEmbarque
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(317, 99)
        Me.Controls.Add(Me.dgvPromedios)
        Me.Controls.Add(Me.dgvCajasEnviadas)
        Me.Controls.Add(Me.dgvCajasProcesadas)
        Me.Controls.Add(Me.dgvProceso)
        Me.Controls.Add(Me.btnInforme)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboAnio)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cboSemana)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboHacienda)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmReporteInformeEmbarque"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BANPROD - INFORME DE EMBARQUE"
        CType(Me.dgvProceso, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCajasProcesadas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCajasEnviadas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPromedios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboHacienda As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboSemana As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboAnio As System.Windows.Forms.ComboBox
    Friend WithEvents btnInforme As System.Windows.Forms.Button
    Friend WithEvents dgvProceso As System.Windows.Forms.DataGridView
    Friend WithEvents dgvCajasProcesadas As System.Windows.Forms.DataGridView
    Friend WithEvents dgvCajasEnviadas As System.Windows.Forms.DataGridView
    Friend WithEvents dgvPromedios As System.Windows.Forms.DataGridView
End Class
