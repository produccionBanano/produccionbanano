﻿Imports ConexionBase
Imports iTextSharp
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.Image
Imports System.IO
Public Class frmReporteComida
    Public sql As String = ""
    Public datos As New ConexionBase.conexionBase
    Public datosSQL As New conexionSQL
    Public dtEmpleados As New DataTable
    Private Sub frmReporteComida_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarCombo()

    End Sub
    Sub consultaEmpleados()

        sql = "SELECT emp.codigo,emp.COD_ALTERNO cedula,emp.apellido + ' ' + emp.NOMBRE empleado,"
        sql &= " (SELECT DESCRIPCION FROM TM_14 WHERE COD_GRUPO = (SELECT H.COD_GRUPO FROM HR_1 H WHERE H.CODIGO = emp.CODIGO) ) grupo,"
        sql &= " (SELECT DESCRIPCION FROM AD_17 WHERE COD_LOC = (SELECT HR_1.COD_LOC FROM HR_1 WHERE HR_1.CODIGO = emp.CODIGO)) localidad"
        sql &= " FROM HR_5 emp "
        sql &= " INNER JOIN HR_1 cab ON cab.CODIGO=emp.codigo AND cab.COD_LOC=" & cboHacienda.SelectedValue & "  AND cab.COD_LOC <>'MIG' AND cab.STATUS='E' "
        sql &= " ORDER BY empleado"

        dtEmpleados = datosSQL.ejecutarSQL(sql)

    End Sub

    Sub cargarCombo()
        Dim dtDatos As New DataTable
        sql = "SELECT codEvolution,descripcion hacienda FROM tblFinca"
        dtDatos = datos.ejecutarMySql(Sql)
        cboHacienda.DataSource = dtDatos
        cboHacienda.ValueMember = "codEvolution"
        cboHacienda.DisplayMember = "hacienda"
    End Sub
    
    Private Sub GenerarPDF()
        Dim Documento As New Document 'Declaracion del documento
        Dim parrafo As New Paragraph ' Declaracion de un parrafo
        Dim tablademo As New PdfPTable(3) 'Se declara 7 por la cantidad de columnas
        Dim tablaInterna As New PdfPTable(1)

        If File.Exists("Pre-Solicitud_.pdf") Then
            'Cerramos el documento si esta abierto.
            'Y asi desbloqueamos el archivo para su eliminacion.
            If Documento.IsOpen Then
                Documento.Close()
                '... lo eliminamos de disco.
                File.Delete("Pre-Solicitud_.pdf")
            End If
        End If

        consultaEmpleados()
        'Dim imagendemo As ItextSharp.text.Image 'Declaracion de una imagen

        Dim widths(2) As Single 'Es el tamaño ajustado de las columnas que se usara en las tablas
        widths(0) = 180
        widths(1) = 180
        widths(2) = 180


        pdf.PdfWriter.GetInstance(Documento, New FileStream("Pre-Solicitud_.pdf", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite)) 'Crea el archivo "DEMO.PDF
        Documento.Open() 'Abre documento para su escritura
        parrafo.Alignment = Element.ALIGN_CENTER 'Alinea el parrafo para que sea centrado o justificado

        ' Seccion Cabecera del PDF

        parrafo.Font = FontFactory.GetFont("Arial", 16, ALIGN_CENTER)
        parrafo.Add("ORODELTI S.A.")
        parrafo.Add(New Paragraph(" "))
        parrafo.Add(New Paragraph())

        parrafo.Font = FontFactory.GetFont("Arial", 10, ALIGN_CENTER) 'Asigan fuente
        parrafo.Add("BANPROD")
        parrafo.Add(New Paragraph(""))
        parrafo.Add("SISTEMA DE GENERACIÓN DE TICKETS COMIDA FINCA")
        parrafo.Add(New Paragraph())

        parrafo.Add("Fecha y hora: " & Date.Now)
        parrafo.Add(New Paragraph()) 'Salto de linea

        parrafo.Add("Finca que genera los tickets: " & cboHacienda.GetItemText(cboHacienda.SelectedItem))
        parrafo.Add(New Paragraph()) 'Salto de linea
        Documento.Add(New Paragraph(" "))
        parrafo.Add("La información mostrada en este documento viene directamente desde evolution, personal activo de finca; no se considera personal en estado de liquidación o proceso de visto bueno, aquel personal que esta activo y no se encuentra en el listado por favor solicitar asistencia al Dpto. de Sistemas.")
        Documento.Add(parrafo) 'Aqui se presenta el texto 

        Documento.Add(New Paragraph(" ")) 'Salto de linea
        Documento.Add(New Paragraph(" "))


        '---------------------------LISTADO-----------------
        Dim tablaEmpleado As New PdfPTable(3)
        Dim widths2(2) As Single 'Es el tamaño ajustado de las columnas que se usara en las tablas
        widths2(0) = 100
        widths2(1) = 300
        widths2(2) = 200
        tablaEmpleado.SetWidthPercentage(widths2, PageSize.A4)

        tablaEmpleado.AddCell(New Paragraph("CODIGO EMP", FontFactory.GetFont("Times", 10, ALIGN_CENTER)))
        tablaEmpleado.AddCell(New Paragraph("EMPLEADO", FontFactory.GetFont("Times", 10, ALIGN_CENTER)))
        tablaEmpleado.AddCell(New Paragraph("FIRMA", FontFactory.GetFont("Times", 10, ALIGN_CENTER)))

        For i = 0 To dtEmpleados.Rows.Count - 1
            tablaEmpleado.AddCell(New Paragraph(dtEmpleados.Rows(i).Item("codigo"), _
                                            FontFactory.GetFont("Times", 10, ALIGN_LEFT)))
            tablaEmpleado.AddCell(New Paragraph(dtEmpleados.Rows(i).Item("nombre") & "                                                                                                               ", _
                                            FontFactory.GetFont("Times", 10, ALIGN_LEFT)))
            tablaEmpleado.AddCell(New Paragraph(" ", _
                                            FontFactory.GetFont("Times", 10, ALIGN_LEFT)))
        Next

        Documento.Add(tablaEmpleado)
        '----------------------------FIN LISTADO-------------

        '--------------------------TICKETS-----------------
        Documento.NewPage()

        tablademo.SetWidthPercentage(widths, PageSize.A4) 'Ajusta el tamaño de cada columna

        For i = 0 To dtEmpleados.Rows.Count - 1

            tablademo.AddCell(New Paragraph(dtEmpleados.Rows(i).Item("codigo") & " - " & _
                                            dtEmpleados.Rows(i).Item("nombre") & "          **ALMUERZO**                          " & dtpFecha.Value & "                                       " & dtEmpleados.Rows(i).Item("localidad"), _
                                         FontFactory.GetFont("Times", 10, ALIGN_LEFT)))
        Next
        
        Documento.Add(tablademo) 'Agrega la tabla al documento

        parrafo.Clear()


        Documento.Close() 'Cierra el documento
        System.Diagnostics.Process.Start("Pre-Solicitud_.pdf") 'Abre el archivo DEMO.PDF


        If File.Exists("Pre-Solicitud_.pdf") Then
            'Cerramos el documento si esta abierto.
            'Y asi desbloqueamos el archivo para su eliminacion.
            If Documento.IsOpen Then
                Documento.Close()
                '... lo eliminamos de disco.
                File.Delete("Pre-Solicitud_.pdf")
            End If
        End If
    End Sub
    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click
        GenerarPDF()
    End Sub
End Class