﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmR11Cabecera
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.btnAnular = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.cboHacienda = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblDia = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboPais = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboCultivo = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboVariedad = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtArea = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboSemana = New System.Windows.Forms.ComboBox()
        Me.lblF1 = New System.Windows.Forms.Label()
        Me.lblF2 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.txtColor = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblNumero = New System.Windows.Forms.Label()
        Me.txtHAS = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtMerma = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblEstado = New System.Windows.Forms.Label()
        Me.txthInicia = New System.Windows.Forms.TextBox()
        Me.txthFinal = New System.Windows.Forms.TextBox()
        Me.btnDetalle = New System.Windows.Forms.Button()
        Me.btnCajas = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.lblRatioCajasTransformadas = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.lblTChapeado = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.lblNManos = New System.Windows.Forms.Label()
        Me.lblLargoDedos = New System.Windows.Forms.Label()
        Me.lblCalibracion = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.lblProcesados = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.lblCajaSegunda = New System.Windows.Forms.Label()
        Me.lblCajaPrimera = New System.Windows.Forms.Label()
        Me.lblCajasEnviadas = New System.Windows.Forms.Label()
        Me.lblCajaProcesadas = New System.Windows.Forms.Label()
        Me.lblRRechazado = New System.Windows.Forms.Label()
        Me.lblFRecuperada = New System.Windows.Forms.Label()
        Me.lblMerma = New System.Windows.Forms.Label()
        Me.lblRatioPTotal = New System.Windows.Forms.Label()
        Me.lblRatioGeneral = New System.Windows.Forms.Label()
        Me.lblPesoPCaja = New System.Windows.Forms.Label()
        Me.lblPesoPRacimo = New System.Windows.Forms.Label()
        Me.lblTRecobrado = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnBuscar
        '
        Me.btnBuscar.Image = Global.produccionBanano.My.Resources.Resources.find
        Me.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnBuscar.Location = New System.Drawing.Point(452, 7)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 55)
        Me.btnBuscar.TabIndex = 23
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'btnAnular
        '
        Me.btnAnular.Image = Global.produccionBanano.My.Resources.Resources.cancel
        Me.btnAnular.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnAnular.Location = New System.Drawing.Point(567, 7)
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.Size = New System.Drawing.Size(75, 55)
        Me.btnAnular.TabIndex = 22
        Me.btnAnular.Text = "Anular"
        Me.btnAnular.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnAnular.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Image = Global.produccionBanano.My.Resources.Resources.save
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnGuardar.Location = New System.Drawing.Point(338, 7)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(75, 55)
        Me.btnGuardar.TabIndex = 21
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Image = Global.produccionBanano.My.Resources.Resources.add
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnNuevo.Location = New System.Drawing.Point(221, 7)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(75, 55)
        Me.btnNuevo.TabIndex = 20
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'cboHacienda
        '
        Me.cboHacienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHacienda.Enabled = False
        Me.cboHacienda.FormattingEnabled = True
        Me.cboHacienda.Location = New System.Drawing.Point(179, 91)
        Me.cboHacienda.Name = "cboHacienda"
        Me.cboHacienda.Size = New System.Drawing.Size(121, 21)
        Me.cboHacienda.TabIndex = 25
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(137, 95)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(36, 13)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "Finca:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(354, 92)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Fecha:"
        '
        'dtpFecha
        '
        Me.dtpFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFecha.Location = New System.Drawing.Point(405, 90)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(97, 20)
        Me.dtpFecha.TabIndex = 28
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(576, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "H. Inicia:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(576, 113)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 30
        Me.Label4.Text = "H. Sale:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(354, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(26, 13)
        Me.Label5.TabIndex = 33
        Me.Label5.Text = "Dia:"
        '
        'lblDia
        '
        Me.lblDia.AutoSize = True
        Me.lblDia.Location = New System.Drawing.Point(402, 114)
        Me.lblDia.Name = "lblDia"
        Me.lblDia.Size = New System.Drawing.Size(16, 13)
        Me.lblDia.TabIndex = 34
        Me.lblDia.Text = "..."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(745, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(32, 13)
        Me.Label6.TabIndex = 36
        Me.Label6.Text = "País:"
        Me.Label6.Visible = False
        '
        'cboPais
        '
        Me.cboPais.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPais.FormattingEnabled = True
        Me.cboPais.Location = New System.Drawing.Point(787, 16)
        Me.cboPais.Name = "cboPais"
        Me.cboPais.Size = New System.Drawing.Size(121, 21)
        Me.cboPais.TabIndex = 35
        Me.cboPais.Visible = False
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(736, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 13)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = "Cultivo:"
        Me.Label7.Visible = False
        '
        'cboCultivo
        '
        Me.cboCultivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCultivo.FormattingEnabled = True
        Me.cboCultivo.Location = New System.Drawing.Point(787, 40)
        Me.cboCultivo.Name = "cboCultivo"
        Me.cboCultivo.Size = New System.Drawing.Size(111, 21)
        Me.cboCultivo.TabIndex = 37
        Me.cboCultivo.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(731, 69)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 13)
        Me.Label8.TabIndex = 40
        Me.Label8.Text = "Variedad:"
        Me.Label8.Visible = False
        '
        'cboVariedad
        '
        Me.cboVariedad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboVariedad.FormattingEnabled = True
        Me.cboVariedad.Location = New System.Drawing.Point(787, 65)
        Me.cboVariedad.Name = "cboVariedad"
        Me.cboVariedad.Size = New System.Drawing.Size(121, 21)
        Me.cboVariedad.TabIndex = 39
        Me.cboVariedad.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(137, 139)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(32, 13)
        Me.Label9.TabIndex = 41
        Me.Label9.Text = "Area:"
        '
        'txtArea
        '
        Me.txtArea.Location = New System.Drawing.Point(179, 136)
        Me.txtArea.Name = "txtArea"
        Me.txtArea.ReadOnly = True
        Me.txtArea.Size = New System.Drawing.Size(57, 20)
        Me.txtArea.TabIndex = 42
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(350, 143)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(49, 13)
        Me.Label10.TabIndex = 44
        Me.Label10.Text = "Semana:"
        '
        'cboSemana
        '
        Me.cboSemana.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSemana.FormattingEnabled = True
        Me.cboSemana.Location = New System.Drawing.Point(405, 139)
        Me.cboSemana.Name = "cboSemana"
        Me.cboSemana.Size = New System.Drawing.Size(55, 21)
        Me.cboSemana.TabIndex = 43
        '
        'lblF1
        '
        Me.lblF1.AutoSize = True
        Me.lblF1.Location = New System.Drawing.Point(473, 131)
        Me.lblF1.Name = "lblF1"
        Me.lblF1.Size = New System.Drawing.Size(16, 13)
        Me.lblF1.TabIndex = 45
        Me.lblF1.Text = "..."
        '
        'lblF2
        '
        Me.lblF2.AutoSize = True
        Me.lblF2.Location = New System.Drawing.Point(473, 149)
        Me.lblF2.Name = "lblF2"
        Me.lblF2.Size = New System.Drawing.Size(16, 13)
        Me.lblF2.TabIndex = 46
        Me.lblF2.Text = "..."
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(134, 190)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(81, 13)
        Me.Label11.TabIndex = 47
        Me.Label11.Text = "Observaciones:"
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Location = New System.Drawing.Point(221, 187)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.Size = New System.Drawing.Size(507, 49)
        Me.txtObservaciones.TabIndex = 48
        '
        'txtColor
        '
        Me.txtColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtColor.Location = New System.Drawing.Point(787, 93)
        Me.txtColor.Multiline = True
        Me.txtColor.Name = "txtColor"
        Me.txtColor.ReadOnly = True
        Me.txtColor.Size = New System.Drawing.Size(71, 26)
        Me.txtColor.TabIndex = 49
        Me.txtColor.Visible = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(137, 65)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(464, 13)
        Me.Label12.TabIndex = 50
        Me.Label12.Text = "Al llenar los datos del R11, se generará un número que servira para llenar los da" & _
    "tos del formulario."
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(4, 130)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(48, 25)
        Me.Label13.TabIndex = 51
        Me.Label13.Text = "No."
        '
        'lblNumero
        '
        Me.lblNumero.AutoSize = True
        Me.lblNumero.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumero.Location = New System.Drawing.Point(43, 131)
        Me.lblNumero.Name = "lblNumero"
        Me.lblNumero.Size = New System.Drawing.Size(21, 24)
        Me.lblNumero.TabIndex = 52
        Me.lblNumero.Text = "0"
        '
        'txtHAS
        '
        Me.txtHAS.Location = New System.Drawing.Point(631, 139)
        Me.txtHAS.Name = "txtHAS"
        Me.txtHAS.Size = New System.Drawing.Size(75, 20)
        Me.txtHAS.TabIndex = 54
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(561, 142)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(64, 13)
        Me.Label15.TabIndex = 53
        Me.Label15.Text = "A. recorrida:"
        '
        'txtMerma
        '
        Me.txtMerma.Location = New System.Drawing.Point(787, 149)
        Me.txtMerma.Name = "txtMerma"
        Me.txtMerma.ReadOnly = True
        Me.txtMerma.Size = New System.Drawing.Size(75, 20)
        Me.txtMerma.TabIndex = 56
        Me.txtMerma.Visible = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(732, 153)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(42, 13)
        Me.Label16.TabIndex = 55
        Me.Label16.Text = "Merma:"
        Me.Label16.Visible = False
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(792, 122)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(61, 20)
        Me.Label17.TabIndex = 57
        Me.Label17.Text = "CINTA"
        Me.Label17.Visible = False
        '
        'lblEstado
        '
        Me.lblEstado.AutoSize = True
        Me.lblEstado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstado.Location = New System.Drawing.Point(6, 232)
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(20, 16)
        Me.lblEstado.TabIndex = 58
        Me.lblEstado.Text = "..."
        '
        'txthInicia
        '
        Me.txthInicia.Location = New System.Drawing.Point(631, 84)
        Me.txthInicia.MaxLength = 5
        Me.txthInicia.Name = "txthInicia"
        Me.txthInicia.Size = New System.Drawing.Size(75, 20)
        Me.txthInicia.TabIndex = 59
        '
        'txthFinal
        '
        Me.txthFinal.Location = New System.Drawing.Point(632, 109)
        Me.txthFinal.MaxLength = 5
        Me.txthFinal.Name = "txthFinal"
        Me.txthFinal.Size = New System.Drawing.Size(75, 20)
        Me.txthFinal.TabIndex = 60
        '
        'btnDetalle
        '
        Me.btnDetalle.Image = Global.produccionBanano.My.Resources.Resources.file_edit
        Me.btnDetalle.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnDetalle.Location = New System.Drawing.Point(70, 163)
        Me.btnDetalle.Name = "btnDetalle"
        Me.btnDetalle.Size = New System.Drawing.Size(60, 54)
        Me.btnDetalle.TabIndex = 61
        Me.btnDetalle.Text = "Detalle"
        Me.btnDetalle.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnDetalle.UseVisualStyleBackColor = True
        '
        'btnCajas
        '
        Me.btnCajas.Image = Global.produccionBanano.My.Resources.Resources.file_apply
        Me.btnCajas.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnCajas.Location = New System.Drawing.Point(4, 163)
        Me.btnCajas.Name = "btnCajas"
        Me.btnCajas.Size = New System.Drawing.Size(60, 55)
        Me.btnCajas.TabIndex = 62
        Me.btnCajas.Text = "Cajas"
        Me.btnCajas.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCajas.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(190, 63)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(117, 12)
        Me.Label14.TabIndex = 64
        Me.Label14.Text = "Ratio sin transf cajas:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(14, 106)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(125, 12)
        Me.Label19.TabIndex = 66
        Me.Label19.Text = "Peso Promedio Racimo:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(368, 85)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(110, 12)
        Me.Label20.TabIndex = 67
        Me.Label20.Text = "Peso Promedio Caja:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(190, 106)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(56, 12)
        Me.Label21.TabIndex = 68
        Me.Label21.Text = "% Merma:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(190, 85)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(127, 12)
        Me.Label22.TabIndex = 69
        Me.Label22.Text = "% Fundas Recuperadas:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(12, 24)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(91, 12)
        Me.Label23.TabIndex = 70
        Me.Label23.Text = "Total Recobrado:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label36)
        Me.GroupBox1.Controls.Add(Me.lblRatioCajasTransformadas)
        Me.GroupBox1.Controls.Add(Me.Label32)
        Me.GroupBox1.Controls.Add(Me.lblTChapeado)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.lblNManos)
        Me.GroupBox1.Controls.Add(Me.lblLargoDedos)
        Me.GroupBox1.Controls.Add(Me.lblCalibracion)
        Me.GroupBox1.Controls.Add(Me.Label33)
        Me.GroupBox1.Controls.Add(Me.Label34)
        Me.GroupBox1.Controls.Add(Me.Label35)
        Me.GroupBox1.Controls.Add(Me.lblProcesados)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.lblCajaSegunda)
        Me.GroupBox1.Controls.Add(Me.lblCajaPrimera)
        Me.GroupBox1.Controls.Add(Me.lblCajasEnviadas)
        Me.GroupBox1.Controls.Add(Me.lblCajaProcesadas)
        Me.GroupBox1.Controls.Add(Me.lblRRechazado)
        Me.GroupBox1.Controls.Add(Me.lblFRecuperada)
        Me.GroupBox1.Controls.Add(Me.lblMerma)
        Me.GroupBox1.Controls.Add(Me.lblRatioPTotal)
        Me.GroupBox1.Controls.Add(Me.lblRatioGeneral)
        Me.GroupBox1.Controls.Add(Me.lblPesoPCaja)
        Me.GroupBox1.Controls.Add(Me.lblPesoPRacimo)
        Me.GroupBox1.Controls.Add(Me.lblTRecobrado)
        Me.GroupBox1.Controls.Add(Me.Label28)
        Me.GroupBox1.Controls.Add(Me.Label27)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 262)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(721, 126)
        Me.GroupBox1.TabIndex = 71
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Info. Adicional"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(190, 44)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(109, 12)
        Me.Label36.TabIndex = 100
        Me.Label36.Text = "Ratio Lotes General:"
        '
        'lblRatioCajasTransformadas
        '
        Me.lblRatioCajasTransformadas.AutoSize = True
        Me.lblRatioCajasTransformadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRatioCajasTransformadas.Location = New System.Drawing.Point(322, 25)
        Me.lblRatioCajasTransformadas.Name = "lblRatioCajasTransformadas"
        Me.lblRatioCajasTransformadas.Size = New System.Drawing.Size(14, 12)
        Me.lblRatioCajasTransformadas.TabIndex = 99
        Me.lblRatioCajasTransformadas.Text = "..."
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(190, 25)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(111, 12)
        Me.Label32.TabIndex = 98
        Me.Label32.Text = "Ratio General Diario:"
        '
        'lblTChapeado
        '
        Me.lblTChapeado.AutoSize = True
        Me.lblTChapeado.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTChapeado.Location = New System.Drawing.Point(144, 84)
        Me.lblTChapeado.Name = "lblTChapeado"
        Me.lblTChapeado.Size = New System.Drawing.Size(14, 12)
        Me.lblTChapeado.TabIndex = 97
        Me.lblTChapeado.Text = "..."
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(14, 84)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(110, 12)
        Me.Label31.TabIndex = 96
        Me.Label31.Text = "Racimos chapeados:"
        '
        'lblNManos
        '
        Me.lblNManos.AutoSize = True
        Me.lblNManos.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNManos.Location = New System.Drawing.Point(490, 65)
        Me.lblNManos.Name = "lblNManos"
        Me.lblNManos.Size = New System.Drawing.Size(14, 12)
        Me.lblNManos.TabIndex = 95
        Me.lblNManos.Text = "..."
        '
        'lblLargoDedos
        '
        Me.lblLargoDedos.AutoSize = True
        Me.lblLargoDedos.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLargoDedos.Location = New System.Drawing.Point(490, 45)
        Me.lblLargoDedos.Name = "lblLargoDedos"
        Me.lblLargoDedos.Size = New System.Drawing.Size(14, 12)
        Me.lblLargoDedos.TabIndex = 94
        Me.lblLargoDedos.Text = "..."
        '
        'lblCalibracion
        '
        Me.lblCalibracion.AutoSize = True
        Me.lblCalibracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalibracion.Location = New System.Drawing.Point(488, 25)
        Me.lblCalibracion.Name = "lblCalibracion"
        Me.lblCalibracion.Size = New System.Drawing.Size(14, 12)
        Me.lblCalibracion.TabIndex = 93
        Me.lblCalibracion.Text = "..."
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(368, 65)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(78, 12)
        Me.Label33.TabIndex = 92
        Me.Label33.Text = "No. de manos:"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(368, 44)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(85, 12)
        Me.Label34.TabIndex = 91
        Me.Label34.Text = "Largo de dedos:"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(368, 25)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(66, 12)
        Me.Label35.TabIndex = 90
        Me.Label35.Text = "Calibración:"
        '
        'lblProcesados
        '
        Me.lblProcesados.AutoSize = True
        Me.lblProcesados.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcesados.Location = New System.Drawing.Point(144, 44)
        Me.lblProcesados.Name = "lblProcesados"
        Me.lblProcesados.Size = New System.Drawing.Size(14, 12)
        Me.lblProcesados.TabIndex = 89
        Me.lblProcesados.Text = "..."
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(13, 43)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(115, 12)
        Me.Label30.TabIndex = 88
        Me.Label30.Text = "Racimos Procesados:"
        '
        'lblCajaSegunda
        '
        Me.lblCajaSegunda.AutoSize = True
        Me.lblCajaSegunda.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCajaSegunda.Location = New System.Drawing.Point(666, 64)
        Me.lblCajaSegunda.Name = "lblCajaSegunda"
        Me.lblCajaSegunda.Size = New System.Drawing.Size(14, 12)
        Me.lblCajaSegunda.TabIndex = 87
        Me.lblCajaSegunda.Text = "..."
        '
        'lblCajaPrimera
        '
        Me.lblCajaPrimera.AutoSize = True
        Me.lblCajaPrimera.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCajaPrimera.Location = New System.Drawing.Point(666, 44)
        Me.lblCajaPrimera.Name = "lblCajaPrimera"
        Me.lblCajaPrimera.Size = New System.Drawing.Size(14, 12)
        Me.lblCajaPrimera.TabIndex = 86
        Me.lblCajaPrimera.Text = "..."
        '
        'lblCajasEnviadas
        '
        Me.lblCajasEnviadas.AutoSize = True
        Me.lblCajasEnviadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCajasEnviadas.Location = New System.Drawing.Point(666, 24)
        Me.lblCajasEnviadas.Name = "lblCajasEnviadas"
        Me.lblCajasEnviadas.Size = New System.Drawing.Size(14, 12)
        Me.lblCajasEnviadas.TabIndex = 85
        Me.lblCajasEnviadas.Text = "..."
        '
        'lblCajaProcesadas
        '
        Me.lblCajaProcesadas.AutoSize = True
        Me.lblCajaProcesadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCajaProcesadas.Location = New System.Drawing.Point(490, 106)
        Me.lblCajaProcesadas.Name = "lblCajaProcesadas"
        Me.lblCajaProcesadas.Size = New System.Drawing.Size(14, 12)
        Me.lblCajaProcesadas.TabIndex = 84
        Me.lblCajaProcesadas.Text = "..."
        '
        'lblRRechazado
        '
        Me.lblRRechazado.AutoSize = True
        Me.lblRRechazado.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRRechazado.Location = New System.Drawing.Point(143, 65)
        Me.lblRRechazado.Name = "lblRRechazado"
        Me.lblRRechazado.Size = New System.Drawing.Size(14, 12)
        Me.lblRRechazado.TabIndex = 83
        Me.lblRRechazado.Text = "..."
        '
        'lblFRecuperada
        '
        Me.lblFRecuperada.AutoSize = True
        Me.lblFRecuperada.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFRecuperada.Location = New System.Drawing.Point(323, 83)
        Me.lblFRecuperada.Name = "lblFRecuperada"
        Me.lblFRecuperada.Size = New System.Drawing.Size(14, 12)
        Me.lblFRecuperada.TabIndex = 82
        Me.lblFRecuperada.Text = "..."
        '
        'lblMerma
        '
        Me.lblMerma.AutoSize = True
        Me.lblMerma.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMerma.Location = New System.Drawing.Point(322, 106)
        Me.lblMerma.Name = "lblMerma"
        Me.lblMerma.Size = New System.Drawing.Size(14, 12)
        Me.lblMerma.TabIndex = 81
        Me.lblMerma.Text = "..."
        '
        'lblRatioPTotal
        '
        Me.lblRatioPTotal.AutoSize = True
        Me.lblRatioPTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRatioPTotal.Location = New System.Drawing.Point(322, 42)
        Me.lblRatioPTotal.Name = "lblRatioPTotal"
        Me.lblRatioPTotal.Size = New System.Drawing.Size(14, 12)
        Me.lblRatioPTotal.TabIndex = 80
        Me.lblRatioPTotal.Text = "..."
        '
        'lblRatioGeneral
        '
        Me.lblRatioGeneral.AutoSize = True
        Me.lblRatioGeneral.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRatioGeneral.Location = New System.Drawing.Point(322, 63)
        Me.lblRatioGeneral.Name = "lblRatioGeneral"
        Me.lblRatioGeneral.Size = New System.Drawing.Size(14, 12)
        Me.lblRatioGeneral.TabIndex = 79
        Me.lblRatioGeneral.Text = "..."
        '
        'lblPesoPCaja
        '
        Me.lblPesoPCaja.AutoSize = True
        Me.lblPesoPCaja.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPesoPCaja.Location = New System.Drawing.Point(490, 85)
        Me.lblPesoPCaja.Name = "lblPesoPCaja"
        Me.lblPesoPCaja.Size = New System.Drawing.Size(14, 12)
        Me.lblPesoPCaja.TabIndex = 78
        Me.lblPesoPCaja.Text = "..."
        '
        'lblPesoPRacimo
        '
        Me.lblPesoPRacimo.AutoSize = True
        Me.lblPesoPRacimo.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPesoPRacimo.Location = New System.Drawing.Point(145, 106)
        Me.lblPesoPRacimo.Name = "lblPesoPRacimo"
        Me.lblPesoPRacimo.Size = New System.Drawing.Size(14, 12)
        Me.lblPesoPRacimo.TabIndex = 77
        Me.lblPesoPRacimo.Text = "..."
        '
        'lblTRecobrado
        '
        Me.lblTRecobrado.AutoSize = True
        Me.lblTRecobrado.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTRecobrado.Location = New System.Drawing.Point(143, 22)
        Me.lblTRecobrado.Name = "lblTRecobrado"
        Me.lblTRecobrado.Size = New System.Drawing.Size(14, 12)
        Me.lblTRecobrado.TabIndex = 76
        Me.lblTRecobrado.Text = "..."
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(555, 64)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(96, 12)
        Me.Label28.TabIndex = 75
        Me.Label28.Text = "% Cajas Segunda:"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(555, 43)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(92, 12)
        Me.Label27.TabIndex = 74
        Me.Label27.Text = "% Cajas Primera:"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(370, 106)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(100, 12)
        Me.Label26.TabIndex = 73
        Me.Label26.Text = "Cajas Procesadas:"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(555, 24)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(88, 12)
        Me.Label25.TabIndex = 72
        Me.Label25.Text = "Cajas Enviadas:"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(12, 64)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(118, 12)
        Me.Label24.TabIndex = 71
        Me.Label24.Text = "Racimos Rechazados:"
        '
        'frmR11Cabecera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BackgroundImage = Global.produccionBanano.My.Resources.Resources.logoWeb2
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.ClientSize = New System.Drawing.Size(735, 397)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnCajas)
        Me.Controls.Add(Me.btnDetalle)
        Me.Controls.Add(Me.txthFinal)
        Me.Controls.Add(Me.txthInicia)
        Me.Controls.Add(Me.lblEstado)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.txtMerma)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.txtHAS)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.lblNumero)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.txtColor)
        Me.Controls.Add(Me.txtObservaciones)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.lblF2)
        Me.Controls.Add(Me.lblF1)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cboSemana)
        Me.Controls.Add(Me.txtArea)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.cboVariedad)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cboCultivo)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.cboPais)
        Me.Controls.Add(Me.lblDia)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpFecha)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboHacienda)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.btnAnular)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnNuevo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmR11Cabecera"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "BANPROD-R11"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents btnAnular As System.Windows.Forms.Button
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents cboHacienda As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblDia As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboPais As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboCultivo As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboVariedad As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtArea As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboSemana As System.Windows.Forms.ComboBox
    Friend WithEvents lblF1 As System.Windows.Forms.Label
    Friend WithEvents lblF2 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
    Friend WithEvents txtColor As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblNumero As System.Windows.Forms.Label
    Friend WithEvents txtHAS As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtMerma As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblEstado As System.Windows.Forms.Label
    Friend WithEvents txthInicia As System.Windows.Forms.TextBox
    Friend WithEvents txthFinal As System.Windows.Forms.TextBox
    Friend WithEvents btnDetalle As System.Windows.Forms.Button
    Friend WithEvents btnCajas As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents lblCajaProcesadas As System.Windows.Forms.Label
    Friend WithEvents lblRRechazado As System.Windows.Forms.Label
    Friend WithEvents lblFRecuperada As System.Windows.Forms.Label
    Friend WithEvents lblMerma As System.Windows.Forms.Label
    Friend WithEvents lblRatioPTotal As System.Windows.Forms.Label
    Friend WithEvents lblRatioGeneral As System.Windows.Forms.Label
    Friend WithEvents lblPesoPCaja As System.Windows.Forms.Label
    Friend WithEvents lblPesoPRacimo As System.Windows.Forms.Label
    Friend WithEvents lblTRecobrado As System.Windows.Forms.Label
    Friend WithEvents lblCajaSegunda As System.Windows.Forms.Label
    Friend WithEvents lblCajaPrimera As System.Windows.Forms.Label
    Friend WithEvents lblCajasEnviadas As System.Windows.Forms.Label
    Friend WithEvents lblProcesados As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents lblNManos As System.Windows.Forms.Label
    Friend WithEvents lblLargoDedos As System.Windows.Forms.Label
    Friend WithEvents lblCalibracion As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents lblTChapeado As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents lblRatioCajasTransformadas As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
End Class
