﻿Imports ConexionBase
Public Class frmLotes
    Public sql As String = ""
    Public datos As New ConexionBase.conexionBase
    Public codigoEditar As Integer
    Private Sub frmLotes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarCombo()

        cboHacienda.SelectedValue = fincaUsuario
    End Sub
    Sub cargarCombo()
        Dim dtDatos As New DataTable
        sql = "SELECT * FROM tblFinca"
        dtDatos = datos.ejecutarMySql(sql)
        cboHacienda.DataSource = dtDatos
        cboHacienda.ValueMember = "id"
        cboHacienda.DisplayMember = "descripcion"
    End Sub
    Sub limpiar()
        txtCodigo.Clear()
        txtArea.Clear()
        txtResponsable.Clear()
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        limpiar()
        codigoEditar = 0
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim dtDatos As New DataTable
        sql = ""

        Try
            sql = "SELECT id,codigo,area FROM tblLote WHERE hacienda=" & cboHacienda.GetItemText(cboHacienda.SelectedValue)
            frmBuscar.sql = sql
            frmBuscar.ShowDialog()

            sql = "SELECT id,codigo,hacienda,area,responsable FROM tblLote"
            sql &= "  WHERE id=" & codigoBuscado

            dtDatos = datos.ejecutarMySql(sql)

            If dtDatos.Rows.Count > 0 Then
                codigoEditar = dtDatos.Rows(0).Item("id")
                txtCodigo.Text = dtDatos.Rows(0).Item("codigo")
                cboHacienda.SelectedValue = dtDatos.Rows(0).Item("hacienda")
                txtArea.Text = dtDatos.Rows(0).Item("area")
                txtResponsable.Text = dtDatos.Rows(0).Item("responsable").ToString

                codigoBuscado = 0
            Else
                codigoBuscado = 0
                codigoEditar = 0
                Return
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim dtDatos As New DataTable
        Try

            If codigoEditar = 0 Then
                sql = ""
                sql = "INSERT INTO tblLote SET "
                sql &= " codigo='" & txtCodigo.Text & "'"
                sql &= " ,hacienda='" & cboHacienda.SelectedValue & "'"
                sql &= " ,area='" & txtArea.Text & "'"
                sql &= " ,responsable='" & txtResponsable.Text & "'"


                dtDatos = datos.ejecutarMySql(sql)

                MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)

            Else
                sql = ""
                sql = "UPDATE tblLote SET "
                sql &= " codigo='" & txtCodigo.Text & "'"
                sql &= " ,hacienda='" & cboHacienda.SelectedValue & "'"
                sql &= " ,area='" & txtArea.Text & "'"
                sql &= " ,responsable='" & txtResponsable.Text & "'"
                sql &= " WHERE id=" & codigoEditar

                dtDatos = datos.ejecutarMySql(sql)

                MessageBox.Show("Datos actualizados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        MessageBox.Show("Los lotes no pueden ser eliminados", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub
End Class