﻿Imports ConexionBase
Public Class frmColor
    Public datos As New ConexionBase.conexionBase
    Public idColor As Integer
    Public sql As String
    Public dtDatos As New DataTable
    Private Sub txtColor_TextChanged(sender As Object, e As EventArgs) Handles txtColor.TextChanged, txtCodigo.TextChanged, txtObservaciones.TextChanged
        sender.CharacterCasing = CharacterCasing.Upper
    End Sub

    Private Sub btnColor_Click(sender As Object, e As EventArgs) Handles btnColor.Click
        If ColorDialog1.ShowDialog() = DialogResult.OK Then
            txtColor.BackColor = ColorDialog1.Color
            txtCodigo.Text = ColorDialog1.Color.ToArgb
        End If
    End Sub
    Sub carga()
        idColor = 0
        txtColor.BackColor = Color.White
        txtCodigo.Clear()
        txtObservaciones.Clear()
        lblEstado.Text = "..."
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        carga()
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Try

            If txtCodigo.TextLength > 0 Then
                If idColor = 0 Then
                    'Inserta
                    sql = "SELECT * FROM tblColor WHERE codigo='" & txtCodigo.Text & "'"
                    dtDatos = datos.ejecutarMySql(sql)

                    If dtDatos.Rows.Count > 0 Then
                        MessageBox.Show("Por favor verifique, el color " & dtDatos.Rows(0).Item("codigo").ToString & " - " & dtDatos.Rows(0).Item("observaciones").ToString & "  ya ha sido ingresado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Else
                        sql = "INSERT INTO tblColor (codigo,observaciones,estado) VALUES ('" & txtCodigo.Text & "','" & txtObservaciones.Text & "','ACTIVO')"
                        datos.ejecutarMySql(sql)
                        idColor = datos.lastInsertId
                        If idColor > 0 Then
                            MessageBox.Show("Información ingresada con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If
                Else
                    'Actualiza
                    sql = "UPDATE tblColor SET "
                    sql &= "codigo='" & txtCodigo.Text & "',"
                    sql &= "observaciones='" & txtObservaciones.Text & "' "
                    sql &= " WHERE id=" & idColor
                    datos.ejecutarMySql(sql)

                    MessageBox.Show("Su registro se encuentra actualizado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            Else
                MessageBox.Show("Para guardar datos debe seleccionar un color", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
            carga()

            
        Catch ex As Exception
            MessageBox.Show(ex.Message, "BANPROD")
        End Try
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        carga()
        frmBuscar.sql = "SELECT id,codigo,observaciones descripcion FROM tblColor"
        frmBuscar.ShowDialog()

        idColor = codigoBuscado
        sql = ""
        sql = "SELECT * FROM tblColor WHERE id=" & codigoBuscado
        dtDatos = datos.ejecutarMySql(sql)

        If dtDatos.Rows.Count > 0 Then
            txtCodigo.Text = dtDatos.Rows(0).Item("codigo").ToString
            txtColor.BackColor = Color.FromArgb(dtDatos.Rows(0).Item("codigo").ToString)
            lblEstado.Text = dtDatos.Rows(0).Item("estado").ToString
            txtObservaciones.Text = dtDatos.Rows(0).Item("observaciones").ToString
        End If



    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If txtCodigo.TextLength > 0 Then
            sql = ""
            sql = "UPDATE tblColor SET "
            sql &= " estado='ANULADO' "
            sql &= " WHERE id=" & idColor

            datos.ejecutarMySql(sql)
            MessageBox.Show("El registro ha sido anulado", "BANDPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            carga()
        Else
            MessageBox.Show("Para anular debe buscar un registro", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        End If
        

    End Sub

End Class