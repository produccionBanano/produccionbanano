﻿Imports ConexionBase
Public Class frmR11Items
    Public idR11 As Integer = 0
    Public datosCabecera As New DataTable
    Public sql As String = ""
    Public datos As New ConexionBase.conexionBase
    Public editarCajas As Integer
    Public editarLotes As Integer
    Public editarRecobro As Integer
    Public idSeleccionadoEditarCajas As Integer
    Public idSeleccionadoEditarLotes As Integer
    Public idSeleccionadoEditarRecobro As Integer

    Sub cargarCombos()
        Dim dtDatos As New DataTable

        'Lotes
        sql = "SELECT * FROM tblLote WHERE hacienda=" & datosCabecera.Rows(0).Item("idFinca").ToString
        dtDatos = datos.ejecutarMySql(sql)
        cboLoteR.DataSource = dtDatos
        cboLoteR.ValueMember = "id"
        cboLoteR.DisplayMember = "codigo"

        cboLoteOtros.DataSource = dtDatos
        cboLoteOtros.ValueMember = "id"
        cboLoteOtros.DisplayMember = "codigo"

        cboLoteRechazado.DataSource = dtDatos
        cboLoteRechazado.ValueMember = "id"
        cboLoteRechazado.DisplayMember = "codigo"

        'Labores desde tina banano
        sql = "SELECT * FROM tblLabor "
        dtDatos = datos.ejecutarMySql(sql)
        cboLabores.DataSource = dtDatos
        cboLabores.ValueMember = "id"
        cboLabores.DisplayMember = "labor"

        'Defecto banano
        sql = "SELECT * FROM tblDefectoBanano "
        dtDatos = datos.ejecutarMySql(sql)
        cboDefectoRechazado.DataSource = dtDatos
        cboDefectoRechazado.ValueMember = "id"
        cboDefectoRechazado.DisplayMember = "descripcion"

        'Manos Sacos
        sql = "SELECT * FROM tblTipoMano "
        dtDatos = datos.ejecutarMySql(sql)
        cboManosSacos.DataSource = dtDatos
        cboManosSacos.ValueMember = "id"
        cboManosSacos.DisplayMember = "descripcion"

        'Clientes

        Dim clientes As String = ""

        sql = ""
        sql = "SELECT TABLA.idR11,GROUP_CONCAT(TABLA.idCliente) clientes FROM ("
        sql &= " SELECT idR11,idCliente FROM tblCajasIniciales WHERE idR11=" & idR11
        sql &= " GROUP BY idR11,idCliente) AS TABLA"
        sql &= " GROUP BY TABLA.idR11"
        dtDatos = datos.ejecutarMySql(sql)
        If dtDatos.Rows.Count < 0 Then
            clientes = 0
        End If


        clientes = dtDatos.Rows(0).Item("clientes")

        sql = ""
        sql = "SELECT * FROM tblCliente WHERE id in (" & clientes & ")"
        dtDatos = datos.ejecutarMySql(sql)
        cboCliente.DataSource = dtDatos
        cboCliente.ValueMember = "id"
        cboCliente.DisplayMember = "nombre"

        'Colores
        sql = "SELECT id,observaciones FROM tblColor"
        dtDatos = datos.ejecutarMySql(sql)
        cboColorR.DataSource = dtDatos
        cboColorR.ValueMember = "id"
        cboColorR.DisplayMember = "observaciones"

        cboEdadR.SelectedIndex = 1

        'Anio Enfunde
        sql = "SELECT anio FROM tblAnio"
        dtDatos = datos.ejecutarMySql(sql)
        cboAnioE.DataSource = dtDatos
        cboAnioE.ValueMember = "anio"
        cboAnioE.DisplayMember = "anio"

        'Anio Recobre
        sql = "SELECT anio FROM tblAnio"
        dtDatos = datos.ejecutarMySql(sql)
        cboAnioR.DataSource = dtDatos
        cboAnioR.ValueMember = "anio"
        cboAnioR.DisplayMember = "anio"

        'Edad
        sql = "SELECT edad FROM tblEdad"
        dtDatos = datos.ejecutarMySql(sql)
        cboEdadR.DataSource = dtDatos
        cboEdadR.ValueMember = "edad"
        cboEdadR.DisplayMember = "edad"

        'TransporteTipo
        sql = "SELECT * FROM tblTipoTransporte"
        dtDatos = datos.ejecutarMySql(sql)
        cboTipoTransporte.DataSource = dtDatos
        cboTipoTransporte.ValueMember = "id"
        cboTipoTransporte.DisplayMember = "tipoTransporte"

        'Semana recobro
        sql = "SELECT id,semana FROM tblCalendario WHERE id=(SELECT idSemana from tblCR11 WHERE id=" & idR11 & ")"
        dtDatos = datos.ejecutarMySql(sql)
        CboSemanaR.DataSource = dtDatos
        CboSemanaR.ValueMember = "id"
        CboSemanaR.DisplayMember = "semana"


    End Sub


    Private Sub frmR11Items_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            sql = "SELECT * FROM tblCR11 WHERE id=" & idR11
            datosCabecera = datos.ejecutarMySql(sql)

            'cboAnioE.SelectedIndex = 0
            'cboAnioR.SelectedIndex = 0
            cargarCombos()
            cargarDefectosR()
            cargarLaboresR()
            cargarManosR()
            cargarRecobre()
            cargaFundaRecu()
            cargarCajasPE()
            cargarRegistroLotes()
            lblR11.Text = idR11

            Me.dgvRecobre.ContextMenuStrip = Me.cMenu
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
   
    Private Sub txtFundaRecuperadas_TextChanged(sender As Object, e As EventArgs) Handles txtFundaRecuperadas.TextChanged
        Try
            txtRecuperado.Text = Format((CDbl(txtFundaRecuperadas.Text) / CDbl(txtCantidadRecobrada.Text)) * 100, "###.##")
        Catch ex As Exception

        End Try

    End Sub


    Private Sub cboCliente_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboCliente.SelectedValueChanged
        Dim dtDatos As New DataTable

        sql = "SELECT id,descripcion FROM tblCaja WHERE cliente='" & cboCliente.GetItemText(cboCliente.SelectedValue) & "'"

        dtDatos = datos.ejecutarMySql(sql)
        cboMarca.DataSource = dtDatos
        cboMarca.ValueMember = "id"
        cboMarca.DisplayMember = "descripcion"
    End Sub

    Private Sub btnGuardarFundas_Click(sender As Object, e As EventArgs) Handles btnGuardarFundas.Click
        Dim dtDatos As New DataTable

        If txtFundaRecuperadas.TextLength > 0 Then
            sql = "SELECT * FROM tblFundaRecuperada WHERE idR11=" & idR11
            dtDatos = datos.ejecutarMySql(sql)
            If dtDatos.Rows.Count > 0 Then
                'Actualiza
                sql = ""
                sql = "UPDATE tblFundaRecuperada SET"
                sql &= " crecobrada=" & txtCantidadRecobrada.Text
                sql &= " ,frecuperada=" & txtFundaRecuperadas.Text
                sql &= " ,observaciones='" & txtObservaciones.Text & "'"
                sql &= " WHERE idR11=" & idR11
                datos.ejecutarMySql(sql)
                MessageBox.Show("Información actualizada", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                'Inserta
                sql = ""
                sql = "INSERT INTO tblFundaRecuperada SET "
                sql &= " idR11=" & idR11
                sql &= " ,crecobrada=" & txtCantidadRecobrada.Text
                sql &= " ,frecuperada=" & txtFundaRecuperadas.Text
                sql &= " ,observaciones='" & txtObservaciones.Text & "'"
                datos.ejecutarMySql(sql)
                MessageBox.Show("Información registrada", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
            cargaFundaRecu()
        Else
            MessageBox.Show("Por favor ingrese datos antes de guardar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub txtCantidadCajas_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidadCajas.KeyPress, txtCantidadCajasE.KeyPress, txtSacos.KeyPress, txtPesoLabores.KeyPress, txtCantidadRechazado.KeyPress, txtCantidadR.KeyPress, txtRechazadoR.KeyPress, txtCantidadCajas.KeyPress, txtCantidadCajasE.KeyPress, txtRecorrido.KeyPress, txtPeso.KeyPress, txtCalibracion.KeyPress, txtLargoDedo.KeyPress, txtNumManos.KeyPress, txtMellizos.KeyPress, txtFundaRecuperadas.KeyPress, txtChapeado.KeyPress
        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        ElseIf (e.KeyChar = "." And Not sender.Text.IndexOf(".")) Or (e.KeyChar = ":" And Not sender.Text.IndexOf(":")) Then
            e.Handled = True
        ElseIf e.KeyChar = "." Or e.KeyChar = ":" Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub txtCantidadCajas_TextChanged(sender As Object, e As EventArgs) Handles txtCantidadCajas.TextChanged
        txtCantidadCajasE.Text = txtCantidadCajas.Text
    End Sub

    Private Sub chkHabilitar_CheckedChanged(sender As Object, e As EventArgs) Handles chkHabilitar.CheckedChanged
        If chkHabilitar.Checked = True Then
            txtCantidadCajasE.Enabled = True
        Else
            txtCantidadCajasE.Enabled = False

        End If

    End Sub

    Private Sub txtCantidadCajasE_TextChanged(sender As Object, e As EventArgs) Handles txtCantidadCajasE.TextChanged
        'If CInt(txtCantidadCajasE.Text) > CInt(txtCantidadCajas.Text) Then
        '    txtCantidadCajasE.Text = txtCantidadCajas.Text
        'End If

    End Sub

    Private Sub btnGuardarSacos_Click(sender As Object, e As EventArgs) Handles btnGuardarSacos.Click
        Dim info As New DataTable
        sql = ""

        If txtSacos.TextLength > 0 Then
            sql = "INSERT INTO tblManosRechazados SET "
            sql &= " idR11=" & idR11
            sql &= " ,idMano=" & cboManosSacos.GetItemText(cboManosSacos.SelectedValue)
            sql &= " ,persona='" & txtPersonaSacos.Text & "'"
            sql &= " ,sacos=" & txtSacos.Text

            datos.ejecutarMySql(sql)

            MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)

            cargarManosR()
        Else
            MessageBox.Show("Por favor ingrese datos antes de guardar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    Sub cargaFundaRecu()
        Dim info As New DataTable
        sql = ""
        sql = " SELECT id,frecuperada FROM tblFundaRecuperada WHERE idR11=" & idR11

        info = datos.ejecutarMySql(sql)
        If info.Rows.Count > 0 Then
            txtFundaRecuperadas.Text = info.Rows(0).Item("frecuperada").ToString
            txtRecuperado.Text = Format((txtFundaRecuperadas.Text / txtCantidadRecobrada.Text) * 100, "###.##")
        Else
            txtFundaRecuperadas.Text = 0
        End If


    End Sub

    Sub cargarManosR()
        Dim info As New DataTable
        sql = ""
        sql = "SELECT mr.id,mr.idR11,mr.idMano,mr.persona, "
        sql &= " (SELECT descripcion mano FROM tblTipoMano WHERE id=mr.idMano) mano,mr.sacos "
        sql &= " FROM tblManosRechazados mr WHERE idR11=" & idR11
        info = datos.ejecutarMySql(sql)

        dgvRechazadoSacos.DataSource = info
        dgvRechazadoSacos.Columns("id").Visible = False
        dgvRechazadoSacos.Columns("idR11").Visible = False
        dgvRechazadoSacos.Columns("idMano").Visible = False
    End Sub
    Sub cargarLaboresR()
        Dim info As New DataTable
        sql = ""
        sql = "SELECT mr.id,mr.idR11,mr.idLabor,mr.persona, "
        sql &= " (SELECT labor FROM tblLabor WHERE id=mr.idLabor) labor,mr.saco "
        sql &= " FROM tblRegistroLabores mr WHERE idR11=" & idR11
        info = datos.ejecutarMySql(sql)

        dgvLabores.DataSource = info
        dgvLabores.Columns("id").Visible = False
        dgvLabores.Columns("idR11").Visible = False
        dgvLabores.Columns("idLabor").Visible = False
    End Sub
    Sub cargarDefectosR()
        Dim info As New DataTable
        sql = ""
        sql = "SELECT mr.id,mr.idR11,mr.idLote,mr.idDefecto, "
        sql &= " (SELECT codigo FROM tblLote WHERE id=mr.idLote) lote, "
        sql &= " (SELECT descripcion FROM tblDefectoBanano WHERE id=mr.idDefecto) defectoBanano, "
        sql &= " mr.cantidad"
        sql &= " FROM tblRacimoDefecto mr WHERE idR11=" & idR11
        info = datos.ejecutarMySql(sql)

        dgvRacimosRechazados.DataSource = info
        dgvRacimosRechazados.Columns("id").Visible = False
        dgvRacimosRechazados.Columns("idR11").Visible = False
        dgvRacimosRechazados.Columns("idLote").Visible = False
        dgvRacimosRechazados.Columns("idDefecto").Visible = False
    End Sub

    Sub cargarRecobre()
        Dim info As New DataTable
        sql = ""
        sql = " SELECT re.id,re.idR11,re.idCalendario,calen.semana,re.edad,re.idLote,lo.codigo lote,color.observaciones colorCinta,re.cantidad recobrado,re.chapeado,CONCAT(tabla.semana,'/',tabla.codigo,'/',tabla.cantidad) enfunde,re.porcentajeR,re.rechazado,(re.cantidad - re.chapeado - re.rechazado) totalProcesado,"
        sql &= " color.codigo,re.idEnfundeProceso,re.idColor FROM tblRecobre re "
        sql &= " LEFT JOIN tblCalendario calen ON calen.id=re.idCalendario "
        sql &= " LEFT JOIN tblLote lo ON lo.id=re.idLote"
        sql &= " LEFT JOIN tblColor color ON color.id=re.idColor"
        sql &= " LEFT JOIN (SELECT enfunde.id,cal.semana,lo.codigo,enfunde.cantidad FROM tblEnfundeProceso enfunde"
        sql &= " LEFT JOIN tblCalendario cal ON cal.id=enfunde.idCalendario"
        sql &= " LEFT JOIN tblLote lo ON lo.id=enfunde.idLote"
        sql &= " ) AS tabla on tabla.id=re.idEnfundeProceso"
        sql &= " WHERE re.idR11=" & idR11
        info = datos.ejecutarMySql(sql)

        dgvRecobre.DataSource = info
        dgvRecobre.Columns("id").Visible = False
        dgvRecobre.Columns("idR11").Visible = False
        dgvRecobre.Columns("idLote").Visible = False
        dgvRecobre.Columns("idCalendario").Visible = False
        dgvRecobre.Columns("idEnfundeProceso").Visible = False
        dgvRecobre.Columns("idColor").Visible = False
        dgvRecobre.Columns("codigo").Visible = False

        If dgvRecobre.RowCount > 0 Then
            'Aplica color
            Dim sumaRecobrado As Decimal = 0
            Dim sumaPRecobro As Decimal = 0
            Dim promRecobre As Decimal = 0
            Dim sumChapeo As Decimal = 0
            For i = 0 To dgvRecobre.Rows.Count - 1
                Me.dgvRecobre.Rows(i).Cells("colorCinta").Style.BackColor = Color.FromArgb(dgvRecobre.Item("codigo", i).Value)
                sumaRecobrado = sumaRecobrado + dgvRecobre.Item("recobrado", i).Value
                sumaPRecobro = sumaPRecobro + dgvRecobre.Item("porcentajeR", i).Value
                sumChapeo = sumChapeo + dgvRecobre.Item("chapeado", i).Value
            Next
            txtCantidadRecobrada.Text = sumaRecobrado
            lblRecobrado.Text = sumaRecobrado + sumChapeo
            lblPRecobrado.Text = Format((sumaRecobrado + sumChapeo) / dgvRecobre.Rows.Count, "##.0#")
        Else
            txtCantidadRecobrada.Text = 0
            lblRecobrado.Text = 0
            lblPRecobrado.Text = 0
        End If


        

    End Sub

    Private Sub txtPersonaSacos_TextChanged(sender As Object, e As EventArgs) Handles txtPersonaSacos.TextChanged, txtPersonaLabores.TextChanged, txtTransporte.TextChanged, txtVapor.TextChanged, txtCod.TextChanged
        sender.CharacterCasing = CharacterCasing.Upper
    End Sub

    Private Sub btnEliminaSacos_Click(sender As Object, e As EventArgs) Handles btnEliminaSacos.Click
        Dim idSeleccionado As String = Me.dgvRechazadoSacos.CurrentRow.Cells("id").Value
        Dim flag As Integer
        flag = MessageBox.Show("Esta seguro de eliminar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No
        If flag = 0 Then
            sql = ""
            sql = " DELETE FROM tblManosRechazados WHERE id=" & idSeleccionado
            datos.ejecutarMySql(sql)
            MessageBox.Show("El registro ha sido eliminado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cargarManosR()
        End If


    End Sub

    Private Sub btnGuardarLabores_Click(sender As Object, e As EventArgs) Handles btnGuardarLabores.Click
        Dim info As New DataTable
        sql = ""


        If txtPesoLabores.TextLength > 0 Then
            sql = "INSERT INTO tblRegistroLabores SET "
            sql &= " idR11=" & idR11
            sql &= " ,idLabor=" & cboLabores.GetItemText(cboLabores.SelectedValue)
            sql &= " ,persona='" & txtPersonaLabores.Text & "'"
            sql &= " ,saco=" & txtPesoLabores.Text

            datos.ejecutarMySql(sql)

            MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cargarLaboresR()
        Else
            MessageBox.Show("Por favor ingrese datos antes de guardar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If



        
    End Sub

    Private Sub btnEliminaLabores_Click(sender As Object, e As EventArgs) Handles btnEliminaLabores.Click
        Dim idSeleccionado As String = Me.dgvLabores.CurrentRow.Cells("id").Value
        Dim flag As Integer
        flag = MessageBox.Show("Esta seguro de eliminar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No
        If flag = 0 Then
            sql = ""
            sql = " DELETE FROM tblRegistroLabores WHERE id=" & idSeleccionado
            datos.ejecutarMySql(sql)
            MessageBox.Show("El registro ha sido eliminado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cargarLaboresR()
        End If
    End Sub

    Private Sub btnGuardarRechazado_Click(sender As Object, e As EventArgs) Handles btnGuardarRechazado.Click
        Dim info As New DataTable
        sql = ""

        If txtCantidadRechazado.TextLength > 0 Then
            sql = "INSERT INTO tblRacimoDefecto SET "
            sql &= " idR11=" & idR11
            sql &= " ,idLote=" & cboLoteRechazado.GetItemText(cboLoteRechazado.SelectedValue)
            sql &= " ,idDefecto=" & cboDefectoRechazado.GetItemText(cboDefectoRechazado.SelectedValue)
            sql &= " ,cantidad=" & txtCantidadRechazado.Text

            datos.ejecutarMySql(sql)

            MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cargarDefectosR()
        Else

        End If


        
    End Sub

    Private Sub btnEliminaRechazado_Click(sender As Object, e As EventArgs) Handles btnEliminaRechazado.Click
        Dim idSeleccionado As String = Me.dgvRacimosRechazados.CurrentRow.Cells("id").Value
        Dim flag As Integer
        flag = MessageBox.Show("Esta seguro de eliminar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No
        If flag = 0 Then
            sql = ""
            sql = " DELETE FROM tblRacimoDefecto WHERE id=" & idSeleccionado
            datos.ejecutarMySql(sql)
            MessageBox.Show("El registro ha sido eliminado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cargarDefectosR()
        End If
    End Sub

    Private Sub btnGuardarRecobre_Click(sender As Object, e As EventArgs) Handles btnGuardarRecobre.Click
        Dim info2 As New DataTable
        Dim info As New DataTable
        Dim info3 As New DataTable
        Dim cantidadEnfundada As Double = 0
        Dim recobradoAnterior As Decimal = 0
        sql = ""

        If txtCantidadR.TextLength > 0 Then

            If editarRecobro = 0 Then 'Insercion
                sql = "" ' Aqui se extrae el enfunde segun la semana, lote y finca
                sql = "SELECT id,cantidad FROM tblEnfundeProceso WHERE idCalendario='" & cboSemanaE.GetItemText(cboSemanaE.SelectedValue) & "'"
                sql &= " AND idlote=" & cboLoteR.GetItemText(cboLoteR.SelectedValue)
                sql &= " AND idFinca=" & fincaUsuario
                info2 = datos.ejecutarMySql(sql)

                If info2.Rows.Count > 0 Then

                    sql = ""
                    sql = "SELECT re.id,enfu.cantidad,IFNULL(SUM(re.cantidad + re.chapeado),0) recobradoAnterior FROM tblRecobre  re "
                    sql &= " LEFT JOIN tblEnfundeProceso enfu on enfu.id=re.idEnfundeProceso"
                    sql &= " WHERE re.id <= " & idR11 & " AND re.idEnfundeProceso =" & info2.Rows(0).Item("id") ' id de enfunde proceso
                    sql &= " GROUP BY enfu.cantidad"
                    info3 = datos.ejecutarMySql(sql)

                    If info3.Rows.Count > 0 Then
                        recobradoAnterior = info3.Rows(0).Item("recobradoAnterior")
                    Else
                        recobradoAnterior = 0
                    End If


                    cantidadEnfundada = info2.Rows(0).Item("cantidad")
                Else
                    cantidadEnfundada = 0
                    MessageBox.Show("No hay datos en enfunde, por favor ingresarlos", "BANPROD", MessageBoxButtons.OK)
                    Return
                End If

                sql = "INSERT INTO tblRecobre SET "
                sql &= " idR11=" & idR11
                sql &= " ,idCalendario=" & CboSemanaR.GetItemText(CboSemanaR.SelectedValue)
                sql &= " ,idLote=" & cboLoteR.GetItemText(cboLoteR.SelectedValue)
                sql &= " ,cantidad=" & txtCantidadR.Text
                sql &= " ,rechazado=" & txtRechazadoR.Text
                sql &= " ,chapeado=" & txtChapeado.Text
                sql &= " ,idEnfundeProceso=" & info2.Rows(0).Item("id")
                sql &= " ,porcentajeR=" & ((CDbl(txtCantidadR.Text) + CDbl(txtChapeado.Text) + recobradoAnterior) / cantidadEnfundada) * 100
                sql &= " ,idColor=" & cboColorR.GetItemText(cboColorR.SelectedValue)
                sql &= " ,edad=" & cboEdadR.GetItemText(cboEdadR.SelectedItem)

                datos.ejecutarMySql(sql)

                MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                editarRecobro = 0
                limpiarRecobro()

            ElseIf editarRecobro = 1 Then
                sql = ""
                sql = "SELECT id,cantidad FROM tblEnfundeProceso WHERE idCalendario='" & cboSemanaE.GetItemText(cboSemanaE.SelectedValue) & "'"
                sql &= " AND idlote=" & cboLoteR.GetItemText(cboLoteR.SelectedValue)
                sql &= " AND idFinca=" & fincaUsuario
                info2 = datos.ejecutarMySql(sql)

                If info2.Rows.Count > 0 Then
                    sql = ""
                    sql = "SELECT re.id,enfu.cantidad,SUM(re.cantidad + re.chapeado) recobradoAnterior FROM tblRecobre  re "
                    sql &= " LEFT JOIN tblEnfundeProceso enfu on enfu.id=re.idEnfundeProceso"
                    sql &= " WHERE  re.idR11 <= " & idR11 & " AND re.id not in (" & idSeleccionadoEditarRecobro & ") AND re.idEnfundeProceso =" & info2.Rows(0).Item("id") ' id de enfunde proceso
                    sql &= " GROUP BY enfu.cantidad"
                    info3 = datos.ejecutarMySql(sql)

                    If info3.Rows.Count > 0 Then
                        recobradoAnterior = info3.Rows(0).Item("recobradoAnterior")
                    Else
                        recobradoAnterior = 0
                    End If

                    cantidadEnfundada = info2.Rows(0).Item("cantidad")
                Else
                    cantidadEnfundada = 0
                    MessageBox.Show("No hay datos en enfunde, por favor ingresarlos", "BANPROD", MessageBoxButtons.OK)
                    Return
                End If

                sql = "UPDATE tblRecobre SET "
                sql &= " idCalendario=" & CboSemanaR.GetItemText(CboSemanaR.SelectedValue)
                sql &= " ,idLote=" & cboLoteR.GetItemText(cboLoteR.SelectedValue)
                sql &= " ,cantidad=" & txtCantidadR.Text
                sql &= " ,rechazado=" & txtRechazadoR.Text
                sql &= " ,chapeado=" & txtChapeado.Text
                sql &= " ,idEnfundeProceso=" & info2.Rows(0).Item("id")
                sql &= " ,porcentajeR=" & ((CDbl(txtCantidadR.Text) + CDbl(txtChapeado.Text) + recobradoAnterior) / cantidadEnfundada) * 100
                sql &= " ,idColor=" & cboColorR.GetItemText(cboColorR.SelectedValue)
                sql &= " ,edad=" & cboEdadR.GetItemText(cboEdadR.SelectedItem)
                sql &= " WHERE id=" & idSeleccionadoEditarRecobro
                datos.ejecutarMySql(sql)

                MessageBox.Show("Datos actualizados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                editarRecobro = 0
                limpiarRecobro()
            End If
            
            cargarRecobre()
            Call btnGuardarFundas_Click(sender, e)
        Else
            MessageBox.Show("Por favor ingrese datos antes de guardar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    Sub limpiarRecobro()
        txtCantidadR.Clear()
        txtRechazadoR.Clear()
        txtChapeado.Clear()
        lblProcesado.Text = 0
    End Sub

    Private Sub CboSemanaR_SelectedValueChanged(sender As Object, e As EventArgs) Handles CboSemanaR.SelectedValueChanged
        
        'cargarRecobre()

    End Sub

    Private Sub btnEliminarRecobre_Click(sender As Object, e As EventArgs) Handles btnEliminarRecobre.Click
        Dim idSeleccionado As String = Me.dgvRecobre.CurrentRow.Cells("id").Value
        Dim flag As Integer
        flag = MessageBox.Show("Esta seguro de eliminar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No
        If flag = 0 Then
            sql = ""
            sql = " DELETE FROM tblRecobre WHERE id=" & idSeleccionado
            datos.ejecutarMySql(sql)
            MessageBox.Show("El registro ha sido eliminado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cargarRecobre()
        End If
    End Sub

    Sub cargarCajasPE()
        Dim info As New DataTable
        sql = ""
        sql = " SELECT cpe.id,cpe.idR11,cpe.idCliente,cli.nombre cliente,cpe.idMarca,caj.descripcion caja,"
        sql &= "  cpe.cantidadP,cpe.cantidadE,saldo.nuevoSaldo,cpe.saldo,cpe.idTipoTransporte,titra.tipoTransporte,cpe.transporte,"
        sql &= "  cpe.vapor,cpe.codigo FROM tblCajasPE cpe"
        sql &= "  LEFT JOIN tblCliente cli on cli.id=cpe.idCliente"
        sql &= "  LEFT JOIN tblCaja caj on caj.id=cpe.idMarca"
        sql &= "  LEFT JOIN tblTipoTransporte titra ON titra.id=cpe.idTipoTransporte"
        sql &= "  LEFT JOIN  (SELECT pe.idMarca,SUM(pe.cantidadP) procesado,SUM(pe.cantidadE) enviado,(SUM(pe.cantidadP)- SUM(cantidadE)) nuevoSaldo FROM tblCajasPE pe"
        sql &= "    INNER JOIN tblCR11 cr ON cr.idFinca=" & fincaUsuario & " AND pe.idR11=cr.id AND pe.idR11< " & idR11
        sql &= "    GROUP BY pe.idMarca) AS saldo ON saldo.idMarca=cpe.idMarca"
        sql &= "  WHERE cpe.idR11=" & idR11
        info = datos.ejecutarMySql(sql)

        dgvCajasPE.DataSource = info
        dgvCajasPE.Columns("id").Visible = False
        dgvCajasPE.Columns("idR11").Visible = False
        dgvCajasPE.Columns("idCliente").Visible = False
        dgvCajasPE.Columns("idMarca").Visible = False
        dgvCajasPE.Columns("idTipoTransporte").Visible = False
        dgvCajasPE.Columns("nuevoSaldo").Visible = False

        Dim sumaCajasEnviadas As Decimal = 0
        Dim sumaCajasProcesadas As Decimal = 0

        For i = 0 To dgvCajasPE.Rows.Count - 1
            sumaCajasEnviadas = sumaCajasEnviadas + dgvCajasPE.Item("cantidadE", i).Value
            sumaCajasProcesadas = sumaCajasProcesadas + dgvCajasPE.Item("cantidadP", i).Value
        Next
        lblEnviados.Text = sumaCajasEnviadas
        lblProcesados.Text = sumaCajasProcesadas


    End Sub

    Private Sub btnGuardarCajas_Click(sender As Object, e As EventArgs) Handles btnGuardarCajas.Click
        Dim info As New DataTable
        sql = ""

        Dim tipoTransporte As String = cboTipoTransporte.GetItemText(cboTipoTransporte.SelectedItem)

        If tipoTransporte = "CONTENEDOR" And txtTransporte.TextLength < 11 Then
            MessageBox.Show("Por favor llenar los campos correctos para la identificación del CONTENEDOR", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        ElseIf tipoTransporte = "CAMION" And txtTransporte.TextLength < 7 Then
            MessageBox.Show("Por favor llenar los campos correctos para la identificación del CAMION", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return
        End If

        If txtCantidadCajas.TextLength > 0 Then
            If editarCajas = 0 Then 'Insercion
                sql = "INSERT INTO tblCajasPE SET "
                sql &= " idR11=" & idR11
                sql &= " ,idCliente=" & cboCliente.GetItemText(cboCliente.SelectedValue)
                sql &= " ,idMarca=" & cboMarca.GetItemText(cboMarca.SelectedValue)
                sql &= " ,idTipoTransporte=" & cboTipoTransporte.GetItemText(cboTipoTransporte.SelectedValue)
                sql &= " ,cantidadP=" & txtCantidadCajas.Text
                sql &= " ,cantidadE=" & txtCantidadCajasE.Text
                sql &= " ,saldo=" & (CDbl(txtCantidadCajas.Text) + CDbl(txtSaldoCajas.Text) - (CDbl(txtCantidadCajasE.Text)))
                sql &= " ,vapor='" & txtVapor.Text & "'"
                sql &= " ,transporte='" & txtTransporte.Text & "'"
                sql &= " ,codigo='" & txtCod.Text & "'"

                datos.ejecutarMySql(sql)

                MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                limpiarCajas()
            ElseIf editarCajas = 1 Then 'Actualización
                sql = "UPDATE tblCajasPE SET "
                sql &= " idR11=" & idR11
                sql &= " ,idCliente=" & cboCliente.GetItemText(cboCliente.SelectedValue)
                sql &= " ,idMarca=" & cboMarca.GetItemText(cboMarca.SelectedValue)
                sql &= " ,idTipoTransporte=" & cboTipoTransporte.GetItemText(cboTipoTransporte.SelectedValue)
                sql &= " ,cantidadP=" & txtCantidadCajas.Text
                sql &= " ,cantidadE=" & txtCantidadCajasE.Text
                sql &= " ,saldo=" & (CDbl(txtCantidadCajas.Text) + CDbl(txtSaldoCajas.Text) - (CDbl(txtCantidadCajasE.Text)))
                sql &= " ,vapor='" & txtVapor.Text & "'"
                sql &= " ,transporte='" & txtTransporte.Text & "'"
                sql &= " ,codigo='" & txtCod.Text & "'"
                sql &= " WHERE id=" & idSeleccionadoEditarCajas

                datos.ejecutarMySql(sql)

                MessageBox.Show("Datos actualizados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                editarCajas = 0
                limpiarCajas()
            End If
            cargarCajasPE()
        Else
            MessageBox.Show("Por favor ingrese datos antes de guardar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnEliminaCaja_Click(sender As Object, e As EventArgs) Handles btnEliminaCaja.Click
        Dim idSeleccionado As String = Me.dgvCajasPE.CurrentRow.Cells("id").Value
        Dim flag As Integer
        flag = MessageBox.Show("Esta seguro de eliminar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No
        If flag = 0 Then
            sql = ""
            sql = " DELETE FROM tblCajasPE WHERE id=" & idSeleccionado
            datos.ejecutarMySql(sql)
            MessageBox.Show("El registro ha sido eliminado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cargarCajasPE()
        End If
    End Sub

    Private Sub dgvCajasPE_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCajasPE.CellContentDoubleClick
        Dim flag As Integer
        idSeleccionadoEditarCajas = Me.dgvCajasPE.CurrentRow.Cells("id").Value

        flag = MessageBox.Show("Está seguro de editar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No

        If flag = 0 Then
            editarCajas = 1

            cboCliente.SelectedValue = Me.dgvCajasPE.CurrentRow.Cells("idCliente").Value
            cboMarca.SelectedValue = Me.dgvCajasPE.CurrentRow.Cells("idMarca").Value
            txtCantidadCajas.Text = Me.dgvCajasPE.CurrentRow.Cells("cantidadP").Value
            txtCantidadCajasE.Text = Me.dgvCajasPE.CurrentRow.Cells("cantidadE").Value
            txtVapor.Text = Me.dgvCajasPE.CurrentRow.Cells("vapor").Value
            txtTransporte.Text = Me.dgvCajasPE.CurrentRow.Cells("transporte").Value
            txtCod.Text = Me.dgvCajasPE.CurrentRow.Cells("codigo").Value
            cboTipoTransporte.SelectedValue = Me.dgvCajasPE.CurrentRow.Cells("idTipoTransporte").Value
            txtSaldoCajas.Text = Me.dgvCajasPE.CurrentRow.Cells("nuevoSaldo").Value
        Else
            editarCajas = 0
        End If


    End Sub
    Sub cargarRegistroLotes()
        Dim info As New DataTable
        sql = ""
        sql = "SELECT rlot.id,rlot.idR11,rlot.idLote,lot.codigo lote,rlot.recorrido,rlot.peso,datoRatio.ratiosLotes,rlot.calibracion,rlot.ldedos,rlot.nmanos,rlot.mellizos FROM tblRegistroLotes rlot"
        sql &= " LEFT JOIN tblLote lot ON lot.id=rlot.idLote "

        sql &= " LEFT JOIN ("
        sql &= " SELECT tblRegistroLotes.id,((tblRegistroLotes.peso) * ( 1- (IFNULL(tblCR11.merma,0))/100))/TABLA2.pesoCajaPromedio ratiosLotes FROM tblRegistroLotes "
        sql &= " LEFT JOIN tblCR11 ON tblCR11.id=tblRegistroLotes.idR11"
        sql &= " LEFT JOIN("
        sql &= " SELECT TABLA.idR11,TABLA.calidad,AVG(TABLA.peso) pesoCajaPromedio FROM ("
        sql &= " SELECT caini.idR11,caini.idCaja,caj.peso,caj.calidad FROM tblCajasIniciales  caini"
        sql &= " INNER JOIN tblCaja caj ON caj.id=caini.idCaja AND caj.calidad=1"
        sql &= " WHERE caini.idR11=" & idR11 & ") AS TABLA"
        sql &= " ) AS TABLA2 ON TABLA2.idR11=tblRegistroLotes.idR11"
        sql &= " WHERE tblRegistroLotes.idR11 = " & idR11 & ") AS datoRatio on datoRatio.id=rlot.id"

        sql &= " WHERE rlot.idR11=" & idR11
        info = datos.ejecutarMySql(sql)

        dgvRLotes.DataSource = info
        dgvRLotes.Columns("id").Visible = False
        dgvRLotes.Columns("idR11").Visible = False
        dgvRLotes.Columns("idLote").Visible = False

        actualizarRatiosLotes()

    End Sub
    Sub actualizarRatiosLotes()
        'Actualizar los ratiosLotes
        For i = 0 To dgvRLotes.Rows.Count - 1
            sql = ""
            sql = "UPDATE tblRegistroLotes SET ratio=" & dgvRLotes.Item("ratiosLotes", i).Value & " WHERE id=" & dgvRLotes.Item("id", i).Value
            datos.ejecutarMySql(sql)
        Next
    End Sub

    Private Sub btnGuardarOtros_Click(sender As Object, e As EventArgs) Handles btnGuardarOtros.Click
        Dim info As New DataTable
        sql = ""

        If txtPeso.TextLength > 0 And txtCalibracion.TextLength > 0 And txtNumManos.TextLength > 0 Then

            If editarLotes = 0 Then 'Insercion
                sql = "INSERT INTO tblRegistroLotes SET "
                sql &= " idR11=" & idR11
                sql &= " ,idLote=" & cboLoteOtros.GetItemText(cboLoteOtros.SelectedValue)
                sql &= " ,recorrido=" & txtRecorrido.Text
                sql &= " ,peso=" & txtPeso.Text
                sql &= " ,calibracion=" & txtCalibracion.Text
                sql &= " ,ldedos=" & txtLargoDedo.Text
                sql &= " ,nmanos=" & txtNumManos.Text
                sql &= " ,mellizos=" & txtMellizos.Text

                datos.ejecutarMySql(sql)

                MessageBox.Show("Datos ingresados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                limpiarLotes()
            ElseIf editarLotes = 1 Then 'Actualización

                sql = "UPDATE tblRegistroLotes SET "
                sql &= "  idLote=" & cboLoteOtros.GetItemText(cboLoteOtros.SelectedValue)
                sql &= " ,recorrido=" & txtRecorrido.Text
                sql &= " ,peso=" & txtPeso.Text
                sql &= " ,calibracion=" & txtCalibracion.Text
                sql &= " ,ldedos=" & txtLargoDedo.Text
                sql &= " ,nmanos=" & txtNumManos.Text
                sql &= " ,mellizos=" & txtMellizos.Text
                sql &= " WHERE id=" & idSeleccionadoEditarLotes

                datos.ejecutarMySql(sql)

                MessageBox.Show("Datos actualizados con éxito", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
                editarLotes = 0
                limpiarLotes()
            End If

        Else
            MessageBox.Show("Por favor ingrese datos antes de guardar", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
        cargarRegistroLotes()



        
    End Sub

    Private Sub btnEliminaRegistroLote_Click(sender As Object, e As EventArgs) Handles btnEliminaRegistroLote.Click
        Dim idSeleccionado As String = Me.dgvRLotes.CurrentRow.Cells("id").Value
        Dim flag As Integer
        flag = MessageBox.Show("Esta seguro de eliminar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No
        If flag = 0 Then
            sql = ""
            sql = " DELETE FROM tblRegistroLotes WHERE id=" & idSeleccionado
            datos.ejecutarMySql(sql)
            MessageBox.Show("El registro ha sido eliminado", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
            cargarRegistroLotes()
        End If
    End Sub

    Private Sub cboAnioE_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboAnioE.SelectedValueChanged
        Dim dtDatos As New DataTable
        'Semanas
        sql = "SELECT * FROM tblCalendario WHERE anio='" & cboAnioE.GetItemText(cboAnioE.SelectedItem) & "'"
        dtDatos = datos.ejecutarMySql(sql)
        cboSemanaE.DataSource = dtDatos
        cboSemanaE.ValueMember = "id"
        cboSemanaE.DisplayMember = "semana"
    End Sub

    Private Sub cboAnioR_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboAnioR.SelectedValueChanged
        'Dim dtDatos As New DataTable
        ''Semanas
        'sql = "SELECT * FROM tblCalendario WHERE anio='" & cboAnioR.GetItemText(cboAnioR.SelectedItem) & "'"
        'dtDatos = datos.ejecutarMySql(sql)
        'CboSemanaR.DataSource = dtDatos
        'CboSemanaR.ValueMember = "id"
        'CboSemanaR.DisplayMember = "semana"
    End Sub

    Private Sub dgvRLotes_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRLotes.CellContentDoubleClick
        Dim flag As Integer
        idSeleccionadoEditarLotes = Me.dgvRLotes.CurrentRow.Cells("id").Value

        flag = MessageBox.Show("Está seguro de editar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No

        If flag = 0 Then
            editarLotes = 1

            cboLoteOtros.SelectedValue = Me.dgvRLotes.CurrentRow.Cells("idLote").Value
            txtRecorrido.Text = Me.dgvRLotes.CurrentRow.Cells("recorrido").Value
            txtPeso.Text = Me.dgvRLotes.CurrentRow.Cells("peso").Value
            txtCalibracion.Text = Me.dgvRLotes.CurrentRow.Cells("calibracion").Value
            txtLargoDedo.Text = Me.dgvRLotes.CurrentRow.Cells("ldedos").Value
            txtNumManos.Text = Me.dgvRLotes.CurrentRow.Cells("nmanos").Value
            txtMellizos.Text = Me.dgvRLotes.CurrentRow.Cells("mellizos").Value
        Else
            editarLotes = 0
        End If
    End Sub

    Private Sub btnNuevoCaja_Click(sender As Object, e As EventArgs) Handles btnNuevoCaja.Click
        editarCajas = 0
        limpiarCajas()
    End Sub
    Sub limpiarLotes()
        editarLotes = 0
        txtRecorrido.Clear()
        txtPeso.Clear()
        txtCalibracion.Clear()
        txtLargoDedo.Clear()
        txtNumManos.Clear()
        txtMellizos.Clear()
    End Sub
    Sub limpiarCajas()
        editarCajas = 0
        txtCantidadCajasE.Clear()
        txtCantidadCajas.Clear()
        txtTransporte.Clear()
        txtVapor.Clear()
        txtCod.Clear()
    End Sub

    Private Sub btnNuevoLotes_Click(sender As Object, e As EventArgs) Handles btnNuevoLotes.Click
        editarLotes = 0
        limpiarLotes()
    End Sub

    Private Sub cboSemanaE_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboSemanaE.SelectedValueChanged
        Dim info As New DataTable

        sql = ""
        sql = "SELECT color.codigo FROM tblCalendario cal "
        sql &= " LEFT JOIN tblColor color ON color.id=cal.idColor"
        sql &= " WHERE cal.id='" & cboSemanaE.GetItemText(cboSemanaE.SelectedValue) & "'"

        info = datos.ejecutarMySql(sql)
        If info.Rows.Count > 0 Then
            txtColorSemanaF.BackColor = Color.FromArgb(info.Rows(0).Item("codigo"))
        End If
    End Sub

    Private Sub dgvRecobre_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRecobre.CellContentDoubleClick
        Dim flag As Integer
        idSeleccionadoEditarRecobro = Me.dgvRecobre.CurrentRow.Cells("id").Value

        flag = MessageBox.Show("Está seguro de editar el registro seleccionado", "BANPROD", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No

        If flag = 0 Then
            editarRecobro = 1

            cboLoteR.SelectedValue = Me.dgvRecobre.CurrentRow.Cells("idLote").Value
            Dim edad As Integer = Me.dgvRecobre.CurrentRow.Cells("edad").Value
            cboEdadR.SelectedValue = edad
            txtCantidadR.Text = Me.dgvRecobre.CurrentRow.Cells("recobrado").Value
            txtRechazadoR.Text = Me.dgvRecobre.CurrentRow.Cells("rechazado").Value
            txtChapeado.Text = Me.dgvRecobre.CurrentRow.Cells("chapeado").Value
            cboColorR.SelectedValue = Me.dgvRecobre.CurrentRow.Cells("idColor").Value

            'Semana recobro
            Dim info As New DataTable
            sql = ""
            sql = "SELECT * FROM tblCalendario WHERE id=" & Me.dgvRecobre.CurrentRow.Cells("idCalendario").Value

            info = datos.ejecutarMySql(sql)
            cboAnioR.SelectedValue = info.Rows(0).Item("anio")
            CboSemanaR.SelectedValue = Me.dgvRecobre.CurrentRow.Cells("idCalendario").Value

            'Semana enfunde
            Dim info2 As New DataTable
            sql = ""
            sql = "SELECT * FROM tblCalendario WHERE id= (SELECT idCalendario FROM tblEnfundeProceso WHERE id=" & Me.dgvRecobre.CurrentRow.Cells("idEnfundeProceso").Value & ")"

            info2 = datos.ejecutarMySql(sql)
            'cboAnioE.SelectedValue = info2.Rows(0).Item("anio")
            'cboSemanaE.SelectedValue = info2.Rows(0).Item("id")


        Else
            editarRecobro = 0
        End If
    End Sub

    Private Sub dgvRecobre_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvRecobre.CellContentClick

    End Sub

    Private Sub btnRatiosLotes_Click(sender As Object, e As EventArgs) Handles btnRatiosLotes.Click
        actualizarRatiosLotes()
        MessageBox.Show("Registros actualizados en base", "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    
    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        'Recobro por lote color
        Dim sql1 As String = ""
        sql1 = ""
        sql1 = "SELECT re.edad,co.observaciones color,lo.codigo lote,enfunde.enfundado,SUM(re.cantidad) recobrado,SUM(re.chapeado) chapeado,SUM(re.rechazado) rechazado,(SUM(re.cantidad) + SUM(re.chapeado)) totalRecobro,(enfunde.enfundado - ((SUM(re.cantidad) + SUM(re.chapeado)))) diferenciaEnfundeRecobro"
        sql1 &= " FROM tblRecobre re "
        sql1 &= " LEFT JOIN tblColor co ON co.id=re.idColor"
        sql1 &= " LEFT JOIN tblLote lo ON lo.id=re.idLote"
        sql1 &= " LEFT JOIN ("
        sql1 &= " SELECT  enfu.id,lot.codigo lote,enfu.cantidad enfundado FROM tblEnfundeProceso enfu"
        sql1 &= " LEFT JOIN tblLote lot ON lot.id=enfu.idLote"
        sql1 &= " ) AS enfunde ON enfunde.id=re.idEnfundeProceso"
        sql1 &= " WHERE re.idR11 =" & idR11
        sql1 &= " GROUP BY re.edad,co.observaciones,lo.codigo"

        frmInfoColores.sql = sql1

        Dim sql2 As String = ""
        sql2 = "SELECT co.observaciones color,SUM(re.cantidad) recobrado,"
        sql2 &= " SUM(re.chapeado) chapeado,SUM(re.rechazado) rechazado,(SUM(re.cantidad) + SUM(re.chapeado)) totalRecobro"
        sql2 &= " FROM tblRecobre re "
        sql2 &= " LEFT JOIN tblColor co ON co.id=re.idColor"
        sql2 &= " LEFT JOIN ("
        sql2 &= " SELECT  enfu.id,enfu.cantidad enfundado FROM tblEnfundeProceso enfu"
        sql2 &= " ) AS enfunde ON enfunde.id=re.idEnfundeProceso"
        sql2 &= " WHERE re.idR11 =" & idR11
        sql2 &= " GROUP BY co.observaciones"

        frmInfoColores.sql2 = sql2

        frmInfoColores.ShowDialog()
    End Sub

    Private Sub dgvCajasPE_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCajasPE.CellContentClick

    End Sub

    Private Sub dgvRecobre_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvRecobre.CellMouseClick
        If (dgvRecobre.SelectedCells.Count > 0) Then
            dgvRecobre.ContextMenuStrip = cMenu
        End If
    End Sub

    Private Sub SaldosEnfundeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SaldosEnfundeToolStripMenuItem.Click
        Dim primero As String = ""
        Dim idEnfunde As Integer = 0
        Try
            cargarRecobre()
            primero = Me.dgvRecobre.CurrentRow.Cells("enfunde").Value.ToString
            idEnfunde = Me.dgvRecobre.CurrentRow.Cells("idEnfundeProceso").Value.ToString

            MessageBox.Show("Se presentara registros de recobro para el enfunde: " & primero, "BANPROD", MessageBoxButtons.OK, MessageBoxIcon.Information)

            sql = ""
            sql = "SELECT re.idR11 NoR11,tblCR11.fecha,cal.semana,(re.cantidad + re.chapeado) recobradoychapeado,"
            sql &= " enfu.cantidad enfunde,"
            sql &= " (SELECT enfu.cantidad - SUM(rec.cantidad + rec.chapeado) FROM tblRecobre rec WHERE rec.idR11<=re.idr11 AND rec.idEnfundeProceso=" & idEnfunde & ") saldo,"
            sql &= " re.porcentajeR FROM tblRecobre re"
            sql &= " LEFT JOIN tblCalendario cal ON cal.id=re.idCalendario"
            sql &= " LEFT JOIN tblEnfundeProceso enfu ON enfu.id=re.idEnfundeProceso"
            sql &= " LEFT JOIN tblCR11 ON re.idR11=tblCR11.id"
            sql &= " WHERE re.idEnfundeProceso =" & idEnfunde
            sql &= " ORDER BY re.idR11"

            frmSaldoEnfunde.sql = sql
            frmSaldoEnfunde.ShowDialog()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
    End Sub

    
    Private Sub cboCliente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCliente.SelectedIndexChanged

    End Sub

    Private Sub txtCantidadR_TextChanged(sender As Object, e As EventArgs) Handles txtCantidadR.TextChanged
        lblProcesado.Text = CDbl(IIf(IsNumeric(txtCantidadR.Text), txtCantidadR.Text, 0)) - CDbl(IIf(IsNumeric(txtRechazadoR.Text), txtRechazadoR.Text, 0)) - CDbl(IIf(IsNumeric(txtChapeado.Text), txtChapeado.Text, 0))
    End Sub

    Private Sub txtRechazadoR_TextChanged(sender As Object, e As EventArgs) Handles txtRechazadoR.TextChanged
        lblProcesado.Text = CDbl(IIf(IsNumeric(txtCantidadR.Text), txtCantidadR.Text, 0)) - CDbl(IIf(IsNumeric(txtRechazadoR.Text), txtRechazadoR.Text, 0)) - CDbl(IIf(IsNumeric(txtChapeado.Text), txtChapeado.Text, 0))
    End Sub
    Private Sub txtChapeado_TextChanged(sender As Object, e As EventArgs) Handles txtChapeado.TextChanged
        lblProcesado.Text = CDbl(IIf(IsNumeric(txtCantidadR.Text), txtCantidadR.Text, 0)) - CDbl(IIf(IsNumeric(txtRechazadoR.Text), txtRechazadoR.Text, 0)) - CDbl(IIf(IsNumeric(txtChapeado.Text), txtChapeado.Text, 0))
    End Sub


    Private Sub cboEdadR_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboEdadR.SelectedValueChanged

    End Sub

    
    Private Sub cboEdadR_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboEdadR.SelectedIndexChanged
        Dim anioEnfunde As Integer = 0
        Dim semanaEnfunde As Integer = 0
        Dim color As Integer = 0
        Dim dtDatos As New DataTable

        sql = "SELECT dato.idColor,dato.id idSemana,dato.anio FROM "
        sql &= "(SELECT * FROM tblCalendario WHERE id<=" & IIf(IsDBNull(CboSemanaR.GetItemText(CboSemanaR.SelectedValue)), 0, CboSemanaR.GetItemText(CboSemanaR.SelectedValue)) & " ORDER BY id DESC LIMIT " & cboEdadR.GetItemText(cboEdadR.SelectedItem) & ") AS dato"
        sql &= " ORDER BY dato.id ASC LIMIT 1"

        dtDatos = datos.ejecutarMySql(sql)

        If dtDatos.Rows.Count > 0 Then
            anioEnfunde = dtDatos.Rows(0).Item("anio")
            semanaEnfunde = dtDatos.Rows(0).Item("idSemana")
            color = dtDatos.Rows(0).Item("idColor")

            cboAnioE.SelectedText = anioEnfunde
            cboSemanaE.SelectedValue = semanaEnfunde
            cboColorR.SelectedValue = color



        End If


    End Sub
End Class