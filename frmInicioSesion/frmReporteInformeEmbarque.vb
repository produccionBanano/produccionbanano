﻿Imports ConexionBase
Public Class frmReporteInformeEmbarque
    Public sql As String
    Public dtDatos As New DataTable
    Public datos As New ConexionBase.conexionBase
    Public idsCR11 As String = ""
    Private Sub frmReporteInformeEmbarque_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarCombo()
        cargarDatos()
    End Sub
    Sub cargarCombo()
        'Hacienda
        sql = "SELECT * FROM tblFinca"
        dtDatos = datos.ejecutarMySql(sql)
        cboHacienda.DataSource = dtDatos
        cboHacienda.ValueMember = "id"
        cboHacienda.DisplayMember = "descripcion"

        sql = "SELECT anio FROM tblAnio"
        dtDatos = datos.ejecutarMySql(sql)
        cboAnio.DataSource = dtDatos
        cboAnio.ValueMember = "anio"
        cboAnio.DisplayMember = "anio"

    End Sub

    Private Sub cboAnio_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboAnio.SelectedValueChanged
        Dim dtDatos As New DataTable
        'Semanas
        sql = "SELECT * FROM tblCalendario WHERE anio='" & cboAnio.GetItemText(cboAnio.SelectedItem) & "'"
        dtDatos = datos.ejecutarMySql(sql)
        cboSemana.DataSource = dtDatos
        cboSemana.ValueMember = "id"
        cboSemana.DisplayMember = "semana"

    End Sub

    Sub cargarDatos()

        Try
            Dim info As New DataTable
            sql = ""
            sql = "SELECT GROUP_CONCAT(IFNULL(id,0)) idsCR11 FROM tblCR11 WHERE idSemana=" & cboSemana.SelectedValue
            info = datos.ejecutarMySql(sql)
            idsCR11 = info.Rows(0).Item("idsCR11").ToString

            sql = ""
            sql = "SELECT edad,co.observaciones color,SUM(cantidad) recobrado,"
            sql &= " sum(rechazado) rechazado,sum(cantidad)-sum(rechazado) totalProceso,sum(chapeado) chapeado,"
            sql &= " sum(cantidad)+sum(chapeado) totalRecobrado FROM tblRecobre"
            sql &= " left join tblColor co on co.id=tblRecobre.idColor"
            sql &= " where idR11 in (" & idsCR11 & ")"
            sql &= " GROUP BY edad,idColor"

            info = datos.ejecutarMySql(sql)

            If info.Rows.Count > 0 Then
                dgvProceso.DataSource = info
            End If

            Dim info2 As New DataTable

            sql = ""
            sql = "SELECT CAJAS.cajasTipo,CAJAS.tamaño,CAJAS.peso,SUM(CAJAS.cantidadP) procesadas,IF(CAJAS.tamaño='Chicas',(SUM(CAJAS.cantidadP)*(CAJAS.peso*2.2046))/41.5,SUM(CAJAS.cantidadP)) transformado FROM ("
            sql &= " select cape.idR11,ca.descripcion cajasTipo,ifnull(ca.peso,0) peso,cape.cantidadP,'Chicas' as tamaño from tblCajasPE cape"
            sql &= " left join tblCaja ca on ca.id=cape.idMarca and ca.peso<=(41/2.20462)"
            sql &= " where ifnull(ca.peso, 0) <> 0 and cape.idR11 in (" & idsCR11 & ")"
            sql &= " UNION ALL"
            sql &= " select cape.idR11,ca.descripcion cajasTipo,ifnull(ca.peso,0) peso,cape.cantidadP,'Grandes' as tamaño from tblCajasPE cape"
            sql &= " left join tblCaja ca on ca.id=cape.idMarca and ca.peso>(41/2.20462)"
            sql &= " where ifnull(ca.peso,0) <> 0 and cape.idR11 in (" & idsCR11 & ")) AS CAJAS"
            sql &= " GROUP BY CAJAS.cajasTipo,CAJAS.tamaño,CAJAS.peso"

            info2 = datos.ejecutarMySql(sql)

            dgvCajasProcesadas.DataSource = info2


            Dim info3 As New DataTable

            sql = ""
            sql = "SELECT cr.fecha,cli.nombre,ca.descripcion tipo,cape.cantidadE,cape.transporte,cape.vapor 'vapor/destino',cape.codigo COD FROM tblCajasPE cape"
            sql &= " LEFT JOIN tblCR11 cr ON cr.id=cape.idR11"
            sql &= " LEFT JOIN tblCaja ca on ca.id=cape.idMarca"
            sql &= " LEFT JOIN tblCliente cli on cli.id=ca.cliente"
            sql &= " WHERE cape.idR11 in (" & idsCR11 & ")"
            info3 = datos.ejecutarMySql(sql)

            dgvCajasEnviadas.DataSource = info3

            Dim info4 As New DataTable

            sql = ""
            sql = "SELECT prom.Tipo,prom.Promedio FROM ("
            sql &= " SELECT 'Peso' AS Tipo,AVG(peso) Promedio,idSemana"
            sql &= " FROM tblCR11"
            sql &= " UNION ALL"
            sql &= " SELECT 'Manos',AVG(manos) Promedio,idSemana"
            sql &= " FROM tblCR11"
            sql &= " UNION ALL"
            sql &= " SELECT 'Calibracion',avg(calibracion) Promedio,idSemana"
            sql &= " FROM tblCR11"
            sql &= " UNION ALL"
            sql &= " SELECT 'Ratio',AVG(ratio) Promedio,idSemana"
            sql &= " FROM tblCR11"
            sql &= " UNION ALL"
            sql &= " SELECT 'Merma',avg(MERMA) Promedio,idSemana"
            sql &= " FROM tblCR11) AS prom WHERE prom.idSemana=" & cboSemana.SelectedValue

            info4 = datos.ejecutarMySql(sql)
            dgvPromedios.DataSource = info4
        Catch ex As Exception

        End Try

       

    End Sub

    Private Sub btnInforme_Click(sender As Object, e As EventArgs) Handles btnInforme.Click
        cargarDatos()

        'Creamos las variables
        Dim exApp As New Microsoft.Office.Interop.Excel.Application
        Dim exLibro As Microsoft.Office.Interop.Excel.Workbook
        Dim exHoja As Microsoft.Office.Interop.Excel.Worksheet
        Dim ElGrid_proceso As New DataGridView
        Dim ElGrid_iebodega As New DataGridView
        Dim ELGrid_cajasenviadas As New DataGridView
        Dim ELGrid_promedios As New DataGridView

        ElGrid_proceso = dgvProceso
        ElGrid_iebodega = dgvCajasProcesadas
        ELGrid_cajasenviadas = dgvCajasEnviadas
        ELGrid_promedios = dgvPromedios

        Me.Cursor = Cursors.WaitCursor
        Try
            'Añadimos el Libro al programa, y la hoja al libro
            exLibro = exApp.Workbooks.Add
            exHoja = exLibro.Worksheets.Add()


            'Agregamos los datos iniciales
            exHoja.Cells.Item(1, 1) = "ORODELTI S.A."
            exHoja.Cells.Item(2, 1) = "INFORME SEMANAL DEL EMBARQUE AÑO:" & cboAnio.GetItemText(cboAnio.SelectedItem)
            exHoja.Cells.Item(3, 1) = "HACIENDA: " & cboHacienda.GetItemText(cboHacienda.SelectedItem)
            exHoja.Cells.Item(4, 1) = "SEMANA: " & cboSemana.GetItemText(cboSemana.SelectedItem)

            exHoja.Cells.Item(6, 1) = "INFORMACION DE RACIMOS"

            exHoja.Rows.Item(1).Font.Size = 14
            exHoja.Rows.Item(2).Font.Size = 12
            exHoja.Rows.Item(3).Font.Size = 12
            exHoja.Rows.Item(4).Font.Size = 12
            exHoja.Rows.Item(6).Font.Size = 12

            exHoja.Rows.Item(1).Font.Bold = 1
            exHoja.Rows.Item(6).Font.Bold = 1

            exHoja.Range("A1:L1").Merge()
            exHoja.Range("A2:L2").Merge()
            exHoja.Range("A3:L3").Merge()
            exHoja.Range("A4:L4").Merge()
            exHoja.Range("A6:L6").Merge()


            ' ¿Cuantas columnas y cuantas filas?
            Dim NCol As Integer = ElGrid_proceso.ColumnCount
            Dim NRow As Integer = ElGrid_proceso.RowCount

            Dim NCol2 As Integer = ElGrid_iebodega.ColumnCount
            Dim NRow2 As Integer = ElGrid_iebodega.RowCount

            Dim NCol3 As Integer = ELGrid_cajasenviadas.ColumnCount
            Dim NRow3 As Integer = ELGrid_cajasenviadas.RowCount

            Dim NCol4 As Integer = ELGrid_promedios.ColumnCount
            Dim NRow4 As Integer = ELGrid_promedios.RowCount


            exHoja.Rows.Item(NRow + 10).Font.Size = 12
            exHoja.Rows.Item(NRow + 10).Font.Bold = 1
            exHoja.Cells.Item(NRow + 10, 1) = "CAJAS PROCESADAS "
            Dim numero As Integer = NRow + 10
            exHoja.Range("A" & numero & ":E" & numero).Merge()


            exHoja.Cells.Item(NRow + 10, 7) = "CAJAS ENVIADAS "
            Dim numero2 As Integer = NRow + 10
            exHoja.Range("G" & numero & ":L" & numero2).Merge()


            exHoja.Cells.Item(NRow2 + 17, 1) = "PROMEDIOS "
            'Dim numero3 As Integer = NRow2 + 10
            ' exHoja.Range("A" & numero2 & ":E" & numero3).Merge()


            'Se lee la primera fila de procesos recobro
            For i As Integer = 1 To NCol
                exHoja.Cells.Item(8, i) = ElGrid_proceso.Columns(i - 1).Name.ToString
                'exHoja.Cells.Item(1, i).HorizontalAlignment = 3
            Next

            For Fila As Integer = 0 To NRow - 1
                For Col As Integer = 0 To NCol - 1
                    exHoja.Cells.Item(Fila + 9, Col + 1) = ElGrid_proceso.Rows(Fila).Cells(Col).Value
                Next
            Next



            'Se lee la primera fila de los cajas procesadas
            For i As Integer = 1 To NCol2
                exHoja.Cells.Item(NRow + 12, i) = ElGrid_iebodega.Columns(i - 1).Name.ToString
                'exHoja.Cells.Item(1, i).HorizontalAlignment = 3
            Next

            For Fila As Integer = 0 To NRow2 - 1
                For Col As Integer = 0 To NCol2 - 1
                    exHoja.Cells.Item(Fila + (NRow + 13), Col + 1) = ElGrid_iebodega.Rows(Fila).Cells(Col).Value
                Next
            Next



            'Se lee la primera fila de los cajas enviadas
            For i As Integer = 1 To NCol3
                exHoja.Cells.Item(NRow + 12, i + 6) = ELGrid_cajasenviadas.Columns(i - 1).Name.ToString
                'exHoja.Cells.Item(1, i).HorizontalAlignment = 3
            Next

            For Fila As Integer = 0 To NRow3 - 1
                For Col As Integer = 0 To NCol3 - 1
                    exHoja.Cells.Item(Fila + (NRow + 13), Col + 1 + 6) = ELGrid_cajasenviadas.Rows(Fila).Cells(Col).Value
                Next
            Next


            'Se lee la primera fila de los PROMEDIOS
            For i As Integer = 1 To NCol4
                exHoja.Cells.Item(NRow2 + 18, i) = ELGrid_promedios.Columns(i - 1).Name.ToString
                'exHoja.Cells.Item(1, i).HorizontalAlignment = 3
            Next

            For Fila As Integer = 0 To NRow4 - 1
                For Col As Integer = 0 To NCol4 - 1
                    exHoja.Cells.Item(Fila + (NRow2 + 18), Col + 1) = ELGrid_promedios.Rows(Fila).Cells(Col).Value
                Next
            Next

            'Titulo en negrita, Alineado al centro y que el tamaño de la columna se ajuste al texto
            exHoja.Rows.Item(8).Font.Bold = 1
            exHoja.Rows.Item(8).HorizontalAlignment = 3

            exHoja.Rows.Item(NRow + 12).Font.Bold = 1
            exHoja.Rows.Item(NRow + 12).HorizontalAlignment = 3



            exHoja.Columns.AutoFit()

            'Aplicación visible
            exApp.Application.Visible = True

            exHoja = Nothing
            exLibro = Nothing
            exApp = Nothing

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al exportar a Excel")
        End Try
        Me.Cursor = Cursors.Default
    End Sub
End Class