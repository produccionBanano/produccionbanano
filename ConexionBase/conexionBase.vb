﻿Imports MySql.Data.MySqlClient
Public Class conexionBase
    Public lastInsertId As Integer

    Public Function ejecutarMySql(ByVal msql As String) As DataTable
        Dim conexionBase As New conexionBase

        Dim conexion As MySqlConnection
        conexion = New MySqlConnection()
        conexion.ConnectionString = "server=" & conexionBase.server & ";user id=" & conexionBase.user & ";password=" & conexionBase.password & ";database=" & conexionBase.database & ""

        conexion.Open()

        Dim comandos As New MySqlCommand()
        Dim Transaccion As MySqlTransaction
        comandos.CommandText = msql
        comandos.CommandType = CommandType.Text
        comandos.Connection = conexion
        Transaccion = conexion.BeginTransaction()

        Dim lectura As MySqlDataReader 'DataReader aloja el ResultSet que lanza la ejecucion del query
        Dim dt As New DataTable

        Try
            lectura = comandos.ExecuteReader()

            dt.Load(lectura)
            lectura.Close()
            Me.lastInsertId = comandos.LastInsertedId ' Es el ultimo id (primary) en caso de que se haya hecho un insert en la base de datos

            Transaccion.Commit()

            comandos.Dispose()
            conexion.Close()
        Catch ex As Exception
            Transaccion.Rollback()
            MsgBox(ex.Message)
        Finally
            comandos.Dispose()
            conexion.Close()
        End Try
        Return dt
    End Function

    Public Class conexionBase
        Public server As String = "192.168.0.10"
        Public user As String = "produccion"
        Public password As String = "Sw28Cw37++"
        Public database As String = "produccionBanano"
    End Class

End Class
